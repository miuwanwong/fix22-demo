# FIX22 demo

## Deployed Version

### Admin dashboard

- URL: https://fix22.surge.sh/admin/login
- Sample account username: available upon request
- Sample account password: available upon request

### Client login

Available on Play Store: https://play.google.com/store/apps/details?id=io.fix22.android

-   For new user: create an account with your own email

### Cifu (technician) login

Available on Play Store: https://play.google.com/store/apps/details?id=io.fix22.android

-   For new user: create an account with your own email

## Local Project Setup

1. build all local dependencies first

```bash
cd shared
npm install
```

2.  go into `react` folder & `server` folder to run `npm install`
3.  set up all env passwords

    ```bash
    gen-env <directory>
    ```

    -   env variables in React must start with REACT_APP_something: https://stackoverflow.com/questions/53237293/react-evironment-variables-env-return-undefined/53237511

4.  create development database
5.  create your own sample accounts
6.  run `npm start` at `react` folder & `server` folder
