FROM node:stretch-slim

# go into server folder
WORKDIR /usr/src/server

# Install package one by one for docker image to cache
# No need to use yarn, because docker auto cache each build step
RUN npm install @types/bcryptjs@^2.4.2
RUN npm install @types/cors@^2.8.12
RUN npm install @types/dotenv@^8.2.0
RUN npm install @types/express-session@^1.17.4
RUN npm install @types/jwt-simple@^0.5.33
RUN npm install @types/multer@^1.4.7
RUN npm install @types/multer-s3@^2.7.10
RUN npm install @types/nodemailer@^6.4.4
RUN npm install @types/permit@^0.2.2
RUN npm install @types/socket.io@^3.0.2
RUN npm install aws-sdk@^2.968.0
RUN npm install bcryptjs@^2.4.3
RUN npm install cors@^2.8.5
RUN npm install dotenv@^10.0.0
RUN npm install express@^4.17.1
RUN npm install express-session@^1.17.2
RUN npm install gen-env@^0.2.0
RUN npm install jwt-simple@^0.5.6
RUN npm install knex@^0.95.9
RUN npm install multer@^1.4.3
RUN npm install multer-s3@^2.9.0
RUN npm install nodemailer@^6.6.3
RUN npm install permit@^0.2.4
RUN npm install pg@^8.7.1
RUN npm install socket.io@^4.1.3
RUN npm install ts-node-dev@^1.1.8
RUN npm install  npm-run-all@^4.1.5


# Copy package.json into Docker server folder
COPY server/package.json .

# Check if it can read your package.json
RUN cat package.json 


# Change to shared folder
WORKDIR /usr/src/shared

# install 'shared' package
COPY shared .
# Clear package-lock.json in case of wrong version
RUN rm package-lock.json
RUN npm install

# Change to server folder
WORKDIR /usr/src/server
# Install 'shared' package & other missing packages
RUN npm install
COPY server .

# For manually deploy with Docker (if using GitLab CI, test cases will be run 3 times)
RUN npm run build

RUN cp package.json build/
WORKDIR /usr/src/server/build

EXPOSE 8080

CMD npm run start:prod
