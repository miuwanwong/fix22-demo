import { config } from 'dotenv';
import Knex from 'knex';

config();

let knexConfig = require('./knexfile');
let mode = process.env.NODE_ENV || 'development';
mode = knexConfig[mode]
export let knex = Knex(mode);