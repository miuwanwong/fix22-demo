export class HttpException {
  constructor(public statusCode: number, public reason: string) {}
}

export function makeRandomPasscode() {
  let code = '';
  for (let i = 0; i < 6; i++) {
    code += Math.floor(Math.random() * 10);
  }
  return code;
}
