import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
  // These tables are not to be deleted if there are existing entries
  // Because the data are foreign keys for other tables

  // assignments
  const assignmentsRow = await knex('assignments').count('assignment_id');

  // orders (Not using in this seed file)
  const ordersRow = await knex('orders').count('order_id');

  // users
  const usersRow = await knex('users').count('user_id');

  // repair_items
  const repairItemsRow = await knex('repair_items').count('repair_item_id');

  // repair_items
  const repairCategoriesRow = await knex('repair_categories').count('repair_category_id');

  // Deletes ALL existing entries

  if (+assignmentsRow[0]!.count === 0) await knex('assignments').del();
  if (+ordersRow[0]!.count === 0) await knex('orders').del();
  if (+ordersRow[0]!.count === 0) await knex('payment_status').del();
  if (+ordersRow[0]!.count === 0) await knex('order_status').del();
  if (+usersRow[0]!.count === 0) await knex('clients').del();
  if (+usersRow[0]!.count === 0) await knex('admin').del();
  if (+usersRow[0]!.count === 0) await knex('cifu').del();
  if (+usersRow[0]!.count === 0) await knex('users').del();
  if (+usersRow[0]!.count === 0) await knex('districts').del();
  if (+usersRow[0]!.count === 0) await knex('user_types').del();
  if (+repairItemsRow[0]!.count === 0) await knex('repair_items').del();
  if (+repairCategoriesRow[0]!.count === 0) await knex('repair_categories').del();

  // Inserts seed entries
  if (+repairCategoriesRow[0]!.count === 0) {
    await knex('repair_categories').insert([
      {
        repair_category_id: 1,
        repair_category_name: '電力點燈',
        repair_category_desc:
          '24小時專業電力服務，檢查、安裝或維修電制、點燈、電線、電箱、電錶等；漏電或跳制處理等商用電力相關服務。',
      },
      {
        repair_category_id: 2,
        repair_category_name: '冷氣空調',
        repair_category_desc:
          '維修冷氣空調、冷庫、冷房、冰機、製冷機，如制冷、更換雪櫃、滴水、清洗隔塵網、加雪種、清洗各式窗口式、分體式、掛窗式、中央式、變頻式冷氣等問題。',
      },
      {
        repair_category_id: 3,
        repair_category_name: '商用凍櫃',
        repair_category_desc:
          '檢查、維修制冷、漏水、供電、溫度及相關服務；檢查、安裝、更換或維修冷房、雪房、製冷機、商用雪櫃、急凍雪櫃及相關服務。',
      },
      {
        repair_category_id: 4,
        repair_category_name: '洗衣乾衣機',
        repair_category_desc: '檢查、安裝、更換乾/洗衣機，維修漏水、排水、噪音、滾筒轉動等問題。',
      },
      {
        repair_category_id: 5,
        repair_category_name: '其他電器',
        repair_category_desc: '維修電熱水器、抽油煙機、抽氣扇、抽熱機、煤氣爐、蒸爐及相關服務。',
      },
      {
        repair_category_id: 6,
        repair_category_name: '電腦維修',
        repair_category_desc:
          '無法開啟電腦、病毒處理、數據修復、運行緩慢、電腦散熱及相關服務；安裝、更換、維修硬盤、電腦系統及相關服務。',
      },
      {
        repair_category_id: 7,
        repair_category_name: 'WIFI網絡',
        repair_category_desc: '路由器、網絡裝備設置、更換或維修，檢測及增強WIFI訊號覆蓋範圍。',
      },
      {
        repair_category_id: 8,
        repair_category_name: '監控鏡頭',
        repair_category_desc:
          '檢查、安裝、維修或更換閉路電視、網絡攝影機、監控鏡頭、防盜鏡頭及相關服務。',
      },
    ]);
  }

  if (+repairItemsRow[0]!.count === 0) {
    await knex('repair_items').insert([
      { repair_category_id: 1, repair_item_name: '檢查', repair_fee: 400 },
      { repair_category_id: 1, repair_item_name: '安裝', repair_fee: 600 },
      { repair_category_id: 1, repair_item_name: '維修電制', repair_fee: 400 },
      { repair_category_id: 1, repair_item_name: '維修點燈', repair_fee: 300 },
      { repair_category_id: 1, repair_item_name: '維修電線', repair_fee: 400 },
      { repair_category_id: 1, repair_item_name: '維修電箱', repair_fee: 400 },
      { repair_category_id: 1, repair_item_name: '維修電錶', repair_fee: 400 },
      {
        repair_category_id: 1,
        repair_item_name: '其他問題（價錢另議）',
        repair_fee: null,
      },
      { repair_category_id: 2, repair_item_name: '檢查', repair_fee: 400 },
      { repair_category_id: 2, repair_item_name: '清洗隔塵網', repair_fee: 300 },
      {
        repair_category_id: 2,
        repair_item_name: '清洗冷氣（價錢另議）',
        repair_fee: null,
      },
      { repair_category_id: 2, repair_item_name: '更換', repair_fee: 400 },
      { repair_category_id: 2, repair_item_name: '滴水', repair_fee: 300 },
      { repair_category_id: 2, repair_item_name: '制冷', repair_fee: 500 },
      { repair_category_id: 2, repair_item_name: '加雪種', repair_fee: 500 },
      {
        repair_category_id: 2,
        repair_item_name: '其他問題（價錢另議）',
        repair_fee: null,
      },
      { repair_category_id: 3, repair_item_name: '檢查', repair_fee: 400 },
      { repair_category_id: 3, repair_item_name: '維修制冷', repair_fee: 400 },
      { repair_category_id: 3, repair_item_name: '維修漏水', repair_fee: 300 },
      { repair_category_id: 3, repair_item_name: '維修供電', repair_fee: 300 },
      { repair_category_id: 3, repair_item_name: '維修溫度', repair_fee: 300 },
      { repair_category_id: 3, repair_item_name: '安裝', repair_fee: 500 },
      { repair_category_id: 3, repair_item_name: '加雪種', repair_fee: 500 },
      { repair_category_id: 3, repair_item_name: '更換', repair_fee: 400 },
      {
        repair_category_id: 3,
        repair_item_name: '其他問題（價錢另議）',
        repair_fee: null,
      },
      { repair_category_id: 4, repair_item_name: '檢查', repair_fee: 400 },
      { repair_category_id: 4, repair_item_name: '安裝', repair_fee: 500 },
      { repair_category_id: 4, repair_item_name: '更換', repair_fee: 500 },
      { repair_category_id: 4, repair_item_name: '維修漏水、排水', repair_fee: 600 },
      { repair_category_id: 4, repair_item_name: '噪音', repair_fee: 300 },
      { repair_category_id: 4, repair_item_name: '滾筒轉動', repair_fee: 400 },
      {
        repair_category_id: 4,
        repair_item_name: '其他問題（價錢另議）',
        repair_fee: null,
      },
      { repair_category_id: 5, repair_item_name: '檢查', repair_fee: 400 },
      { repair_category_id: 5, repair_item_name: '安裝', repair_fee: 500 },
      { repair_category_id: 5, repair_item_name: '更換', repair_fee: 500 },
      { repair_category_id: 5, repair_item_name: '維修', repair_fee: 600 },
      {
        repair_category_id: 5,
        repair_item_name: '其他問題（價錢另議）',
        repair_fee: null,
      },
      { repair_category_id: 6, repair_item_name: '檢查', repair_fee: 400 },
      { repair_category_id: 6, repair_item_name: '無法開啟電腦', repair_fee: 400 },
      { repair_category_id: 6, repair_item_name: '病毒處理', repair_fee: 500 },
      { repair_category_id: 6, repair_item_name: '數據修復', repair_fee: 800 },
      { repair_category_id: 6, repair_item_name: '運行緩慢', repair_fee: 300 },
      { repair_category_id: 6, repair_item_name: '安裝', repair_fee: 300 },
      { repair_category_id: 6, repair_item_name: '更換', repair_fee: 300 },
      {
        repair_category_id: 6,
        repair_item_name: '其他問題（價錢另議）',
        repair_fee: null,
      },
      { repair_category_id: 7, repair_item_name: '檢查', repair_fee: 400 },
      { repair_category_id: 7, repair_item_name: '網絡裝備設置', repair_fee: 600 },
      { repair_category_id: 7, repair_item_name: '更換', repair_fee: 300 },
      { repair_category_id: 7, repair_item_name: '維修', repair_fee: 600 },
      { repair_category_id: 7, repair_item_name: '增強WIFI訊號', repair_fee: 500 },
      {
        repair_category_id: 7,
        repair_item_name: '其他問題（價錢另議）',
        repair_fee: null,
      },
      { repair_category_id: 8, repair_item_name: '檢查', repair_fee: 400 },
      { repair_category_id: 8, repair_item_name: '更換', repair_fee: 400 },
      { repair_category_id: 8, repair_item_name: '安裝', repair_fee: 300 },
      { repair_category_id: 8, repair_item_name: '維修', repair_fee: 600 },
      {
        repair_category_id: 8,
        repair_item_name: '其他問題（價錢另議）',
        repair_fee: null,
      },
    ]);
  }

  if (+usersRow[0]!.count === 0) {
    await knex('districts').insert([
      { district_id: 1, district_name: '中西區' },
      { district_id: 2, district_name: '東區' },
      { district_id: 3, district_name: '南區' },
      { district_id: 4, district_name: '灣仔區' },
      { district_id: 5, district_name: '九龍城區' },
      { district_id: 6, district_name: '觀塘區' },
      { district_id: 7, district_name: '深水埗區' },
      { district_id: 8, district_name: '黃大仙區' },
      { district_id: 9, district_name: '油尖旺區' },
      { district_id: 10, district_name: '葵青區' },
      { district_id: 11, district_name: '西貢區' },
      { district_id: 12, district_name: '沙田區' },
      { district_id: 13, district_name: '大埔區' },
      { district_id: 14, district_name: '荃灣區' },
      { district_id: 15, district_name: '元朗區' },
      { district_id: 16, district_name: '屯門區' },
      { district_id: 17, district_name: '北區' },
      { district_id: 18, district_name: '離島區' },
    ]);
  }

  if (+usersRow[0]!.count === 0) {
    await knex('user_types').insert([
      { user_type_id: 1, user_type: 'client' },
      { user_type_id: 2, user_type: 'cifu' },
      { user_type_id: 3, user_type: 'admin' },
    ]);
  }

  if (+ordersRow[0]!.count === 0) {
    await knex('order_status').insert([
      { order_status_id: 1, order_status: 'pending assignment' },
      { order_status_id: 2, order_status: 'assigned' },
      { order_status_id: 3, order_status: 'cancelled' },
      { order_status_id: 4, order_status: 'completed' },
    ]);
    await knex('payment_status').insert([
      { payment_status_id: 1, payment_status: 'pending quotation' },
      { payment_status_id: 2, payment_status: 'pending payment' },
      { payment_status_id: 3, payment_status: 'payment image submitted' },
      { payment_status_id: 4, payment_status: 'payment confirmed' },
    ]);
  }

  // for CICD

  const row = await knex('stat').count('*').first();
  if (+row!.count === 0) {
    // Inserts seed entries
    await knex('stat').insert({
      visit_count: 0,
    });
  }
}
