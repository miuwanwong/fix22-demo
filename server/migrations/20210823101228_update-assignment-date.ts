import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('assignments', (table) => {
        table.timestamp('repair_date').notNullable().alter();
      });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('assignments', (table) => {
        table.date('repair_date').notNullable().alter();
      });
}

