import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('repair_categories', (table) => {
    table.integer('repair_category_id').notNullable().unique();
    table.string('repair_category_name').notNullable().unique();
    table.string('repair_category_desc');
  });

  await knex.schema.createTable('repair_items', (table) => {
    table.increments('repair_item_id');
    table.integer('repair_category_id').unsigned().notNullable();
    table.foreign('repair_category_id').references('repair_categories.repair_category_id');
    table.string('repair_item_name').notNullable();
    table.integer('repair_fee');
  });

  await knex.schema.createTable('user_types', (table) => {
    table.integer('user_type_id').notNullable().unique();
    table.string('user_type').notNullable().unique();
  });

  await knex.schema.createTable('users', (table) => {
    table.increments('user_id');
    table.integer('user_type_id').unsigned().notNullable();
    table.foreign('user_type_id').references('user_types.user_type_id');
    table.string('email').unique(); // for client & admin to login with
    table.string('passcode', 6).unique(); // for client to login with
    table.boolean('email_verified').defaultTo(false);
    table.timestamp('created_at').defaultTo(knex.fn.now());
  });

  await knex.schema.createTable('districts', (table) => {
    table.integer('district_id').notNullable().unique();
    table.string('district_name').notNullable().unique();
  });

  await knex.schema.createTable('clients', (table) => {
    table.integer('user_id').unsigned().notNullable().primary();
    table.foreign('user_id').references('users.user_id');
    table.string('company_name');
    table.string('company_tel').notNullable();
    table.string('company_address1');
    table.string('company_address2');
    table.integer('district_id').unsigned;
    table.foreign('district_id').references('districts.district_id');
    table.string('person_in_charge');
    table.string('person_in_charge_prefix');
    table.string('person_in_charge_job_title');
    table.timestamps(false, true);
  });

  await knex.schema.createTable('order_status', (table) => {
    table.integer('order_status_id').notNullable().unique();
    table.string('order_status').notNullable().unique();
  });

  await knex.schema.createTable('payment_status', (table) => {
    table.integer('payment_status_id').notNullable().unique();
    table.string('payment_status').notNullable().unique();
  });

  await knex.schema.createTable('orders', (table) => {
    table.increments('order_id');
    table.integer('user_id').unsigned().notNullable();
    table.foreign('user_id').references('users.user_id');
    table.integer('repair_category_id').unsigned().notNullable();
    table.foreign('repair_category_id').references('repair_categories.repair_category_id');
    table.string('problem_desc');
    table.string('appliance_brand');
    table.string('appliance_model');
    table.string('appliance_install_location_desc');
    table.string('repair_address1');
    table.string('repair_address2');
    table.integer('district_id').unsigned;
    table.foreign('district_id').references('districts.district_id');
    table.string('person_in_charge');
    table.string('person_in_charge_prefix');
    table.string('person_in_charge_job_title');
    table.string('contact_tel').notNullable();
    table.string('contact_email').notNullable();
    table.integer('order_status_id').unsigned().notNullable().defaultTo(1);
    table.foreign('order_status_id').references('order_status.order_status_id');
    table.integer('payment_status_id').unsigned().notNullable().defaultTo(1);
    table.foreign('payment_status_id').references('payment_status.payment_status_id');
    table.timestamps(false, true);
  });

  await knex.schema.createTable('selected_repair_items', (table) => {
    table.integer('order_id').unsigned().notNullable();
    table.foreign('order_id').references('orders.order_id');
    table.integer('repair_item_id').unsigned().notNullable();
    table.foreign('repair_item_id').references('repair_items.repair_item_id');
    table.primary(['order_id', 'repair_item_id']);
  });

  await knex.schema.createTable('order_images', (table) => {
    table.increments('order_image_id');
    table.integer('order_id').unsigned().notNullable();
    table.foreign('order_id').references('orders.order_id');
    table.string('image_file_name').notNullable();
    table.timestamp('uploaded_at').defaultTo(knex.fn.now());
  });

  await knex.schema.createTable('payment_images', (table) => {
    table.increments('payment_image_id');
    table.integer('order_id').unsigned().notNullable();
    table.foreign('order_id').references('orders.order_id');
    table.string('image_file_name').notNullable();
    table.timestamp('uploaded_at').defaultTo(knex.fn.now());
  });

  await knex.schema.createTable('cifu', (table) => {
    table.integer('user_id').unsigned().notNullable().primary();
    table.foreign('user_id').references('users.user_id');
    table.string('username').unique().notNullable(); // for cifu to login with, for fear that they do not have email address.
    table.string('name').notNullable();
    table.string('password', 60).notNullable();
    table.string('tel').unique();
    table.timestamps(false, true);
  });

  await knex.schema.createTable('admin', (table) => {
    table.integer('user_id').unsigned().notNullable().primary();
    table.foreign('user_id').references('users.user_id');
    table.string('name').notNullable();
    table.string('password', 60).notNullable();
    table.string('tel').unique();
    table.timestamps(false, true);
  });

  await knex.schema.createTable('assignments', (table) => {
    table.increments('assignment_id');
    table.integer('order_id').unsigned().notNullable();
    table.foreign('order_id').references('orders.order_id');
    table.integer('cifu_id').unsigned().notNullable();
    table.foreign('cifu_id').references('cifu.user_id');
    table.date('repair_date').notNullable();
    table.string('repair_time', 2).notNullable();
    table.string('remarks');
    table.timestamp('mark_complete_by_cifu');
    table.timestamp('mark_complete_by_client');
    table.integer('total_amount_to_cifu');
    table.boolean('cifu_payment_status').defaultTo(false);
    table.boolean('is_active').defaultTo(true)
    table.timestamps(false, true);
  });

  await knex.schema.createTable('completion_images', (table) => {
    table.increments('completion_image_id');
    table.integer('assignment_id').unsigned().notNullable();
    table.foreign('assignment_id').references('assignments.assignment_id');
    table.string('image_file_name').notNullable();
    table.timestamp('uploaded_at').defaultTo(knex.fn.now());
    table.integer('uploaded_by').unsigned().notNullable();
    table.foreign('uploaded_by').references('users.user_id');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('completion_images');
  await knex.schema.dropTable('assignments');
  await knex.schema.dropTable('admin');
  await knex.schema.dropTable('cifu');
  await knex.schema.dropTable('payment_images');
  await knex.schema.dropTable('order_images');
  await knex.schema.dropTable('selected_repair_items');
  await knex.schema.dropTable('orders');
  await knex.schema.dropTable('payment_status');
  await knex.schema.dropTable('order_status');
  await knex.schema.dropTable('clients');
  await knex.schema.dropTable('districts');
  await knex.schema.dropTable('users');
  await knex.schema.dropTable('user_types');
  await knex.schema.dropTable('repair_items');
  await knex.schema.dropTable('repair_categories');
}
