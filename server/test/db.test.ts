import { knex } from '../db';

describe('db TestSuit', () => {
  test('it should has database setup', async function() {
    await knex.migrate.rollback();
    await knex.migrate.latest();
    await knex.seed.run();
    expect(await knex('stat').select('visit_count').first()).toEqual({
      visit_count: 0,
    });
    await knex.destroy();
  }, 100000);
});
