import socketIO from 'socket.io';

export let io: socketIO.Server;

export function setSocketIO(value: socketIO.Server) {
  io = value;
  io.on('connection', (socket) => {
    console.log('client connected');

    socket.on('newRoom', (user_id: string) => {
      socket.join(user_id);
      console.log(`joined room ${user_id}`);
    });
  });
}
