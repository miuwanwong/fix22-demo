import express from 'express';
import dotenv from 'dotenv';
import { knex } from './db';
import cors from 'cors';
import http from 'http';
import socketIO from 'socket.io';
import { setSocketIO } from './socketio';
import multer from 'multer';
import multerS3 from 'multer-s3';
import aws from 'aws-sdk';

import { ApplianceTypeController } from './controllers/ApplianceTypeController';
import { ApplianceTypeService } from './services/ApplianceTypeService';
import { OrderService } from './services/OrderService';
import { OrderController } from './controllers/OrderController';
import { AssignmentService } from './services/AssignmentService';
import { AssignmentController } from './controllers/AssignmentController';
import { UserService } from './services/UserService';
import { UserController } from './controllers/UserController';

dotenv.config();

const app = express();

const server = http.createServer(app);
const io = new socketIO.Server(server, {
  cors: {
    origin: '*',
  },
});

setSocketIO(io);

app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const s3 = new aws.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_S3_REGION,
});

if (!process.env.AWS_S3_BUCKET_NAME) {
  console.error('missing AWS_S3_BUCKET_NAME in env');
  process.exit(1);
}

const storage = multerS3({
  s3: s3,
  bucket: process.env.AWS_S3_BUCKET_NAME,
  metadata: (req, file, cb) => {
    console.log(`at server.ts; ${JSON.stringify(file)}`);
    cb(null, { fieldName: file.fieldname });
  },

  key: (req, file, cb) => {
    let prefix = file.fieldname;
    let ext = file.mimetype.split('/')[1];
    let timestamp = Date.now();
    let filename = `${prefix}-${timestamp}.${ext}`;
    let random = Math.random().toString(36).slice(2);

    filename = `${prefix}-${timestamp}-${random}.${ext}`;

    cb(null, filename);
  },
});

export const uploadOrderImage = multer({
  storage: storage,
  limits: {
    files: 3, // allow up to 5 files per request,
    //fieldSize: 2 * 700 * 700 // 2 MB (max file size)
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype.startsWith('image')) {
      cb(null, true);
    } else {
      return cb(new Error('Please upload only images.'));
    }
  },
}).array('orderImage', 3);

export const uploadPaymentImage = multer({
  storage: storage,
  limits: {
    files: 1, // allow up to 5 files per request,
    //fieldSize: 2 * 700 * 700 // 2 MB (max file size)
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype.startsWith('image')) {
      console.log(`filefilter  reached`);
      cb(null, true);
    } else {
      return cb(new Error('Please upload only images.'));
    }
  },
}).array('paymentImage', 1);

export const uploadCompletionImage = multer({
  storage: storage,
  limits: {
    files: 3, // allow up to 5 files per request,
    //fieldSize: 2 * 700 * 700 // 2 MB (max file size)
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype.startsWith('image')) {
      cb(null, true);
    } else {
      return cb(new Error('Please upload only images.'));
    }
  },
}).array('completionImage', 3);

export const userService = new UserService(knex);
export const userController = new UserController(userService);
app.use(userController.toRouter());

const applianceTypeService = new ApplianceTypeService(knex);
export const applianceTypeController = new ApplianceTypeController(applianceTypeService);
app.use(applianceTypeController.toRouter());

const orderService = new OrderService(knex);
export const orderController = new OrderController(orderService);
app.use(orderController.toRouter());

const assignmentService = new AssignmentService(knex);
export const assignmentController = new AssignmentController(assignmentService);
app.use(assignmentController.toRouter());

app.get('/', (req, res) => {
  knex.transaction(async (knex) => {
    let { visit_count } = await knex('stat').select('visit_count').first();
    visit_count++;
    res.end(`Hello #${visit_count} visitor. \nWelcome to FIX22 Server Version 1.0.2`);
    await knex('stat').update({ visit_count });
  });
});

app.use('/uploads', express.static('./uploads'));

app.use((req, res) => {
  res.status(404).json({ error: 'Invalid request, typo on url or method?' });
});

const PORT = +process.env.HTTP_PORT!;

if (!PORT) {
  console.error('missing HTTP_PORT in env');
  process.exit(1);
}

server.listen(PORT, () => {
  console.log(`listening to port ${PORT}`);
});
