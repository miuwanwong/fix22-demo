import { Request, Response, NextFunction } from 'express';
import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import jwt from './jwt';
import { JWTPayload } from 'shared';
import { userService } from './server';

const permit = new Bearer({
  query: 'access_token',
});

export async function requireLogin(req: Request, res: Response, next: NextFunction) {
  let token: string;

  try {
    token = permit.check(req);
  } catch (error) {
    res.status(401).json({ error: 'Invalid format in Request' });
    return;
  }

  if (!token) {
    res.status(401).json({ error: 'Empty Bearer Token in Request' });
    return;
  }

  let payload: JWTPayload | undefined;

  if (!jwt.jwtSecret) {
    res.status(500).json({ error: 'Missing jwt secret in env' });
    return;
  }

  try {
    payload = jwtSimple.decode(token, jwt.jwtSecret);
   
    
    if (!payload) {
      res.status(500).json({ error: 'empty payload' });
      return;
    }
  } catch (error) {
    res.status(401).json({ error: 'Invalid JWT in Request' });
    return;
  }

  let user: JWTPayload | undefined;

  try {
    user = await userService.checkLogin(payload.user_id, payload.user_type_id); // to check if the user_id is still active account in DB.
  } catch (error) {
    console.error(error)
    res.status(401).json({ error: 'Account not active' });
    return;
  }

  if (!user) return res.status(401).json({ error: 'Account not active' });

  req.user = user; // to return the user's info to request. for using user's info in subsequent request.

  console.log(`checked at Guard: ${JSON.stringify(req.user)}`);
  return next();
}

export function requireAdminLogin(req: Request, res: Response, next: NextFunction) {
  if (!process.env.USERS_ADMIN) {
    return res.status(500).json({ error: 'internal server error' });
  }

  if (req.user?.user_type_id === parseInt(process.env.USERS_ADMIN)) {
    console.log(`checked at adminLogin`);
    return next();
  } else {
    console.log('Permission denied');
    return res.status(401).json({ error: 'Permission denied' });
  }
}

export function requireClientLogin(req: Request, res: Response, next: NextFunction) {
  if (!process.env.USERS_CLIENT) {
    return res.status(500).json({ error: 'internal server error' });
  }

  if (req.user?.user_type_id === parseInt(process.env.USERS_CLIENT)) {
    console.log(`checked at clientLogin`);
    return next();
  } else {
    console.log('Permission denied');
    return res.status(401).json({ error: 'Permission denied' });
  }
}

export function requireCifuLogin(req: Request, res: Response, next: NextFunction) {
  if (!process.env.USERS_CIFU) {
    return res.status(500).json({ error: 'internal server error' });
  }

  if (req.user?.user_type_id === parseInt(process.env.USERS_CIFU)) {
    console.log(`checked at cifuLogin`);
    return next();
  } else {
    console.log('Permission denied');
    return res.status(401).json({ error: 'Permission denied' });
  }
}
