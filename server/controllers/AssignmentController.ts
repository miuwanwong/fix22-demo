import express, { Request, Response } from 'express';
import { requireAdminLogin, requireLogin } from '../guards';
import { AssignmentService } from '../services/AssignmentService';
import { uploadCompletionImage } from '../server';
import multer from 'multer';
import { io } from '../socketio';

export class AssignmentController {
  constructor(private assignmentService: AssignmentService) {}

  toRouter = () => {
    let router = express.Router();

    router.post('/assignment', requireLogin, requireAdminLogin, this.createAssignment);
    router.get('/assignments', requireLogin, this.getAllAssignments); //cifu and admin
    router.put('/assignment', requireLogin, requireAdminLogin, this.editAssignment);
    router.delete('/assignment', requireLogin, requireAdminLogin, this.deleteAssignment);

    router.get('/assignment/get-cifu-list', requireLogin, requireAdminLogin, this.getCifuList);
    router.get('/assignments/search', requireLogin, requireAdminLogin, this.getFilterAssignments);
    router.get('/assignment/:assignmentID', requireLogin, this.getAssignment);
    router.post('/assignment/:assignmentID/completion', requireLogin, this.confirmCompletion); //for uploading completion images and mark complete by both client and cifu

    return router;
  };

  todo = (req: Request, res: Response) => {
    res.status(501).json({ err: 'to do' });
  };

  // TO DO METHODS:
  createAssignment = async (req: Request, res: Response) => {
    try {
      const assignment = await this.assignmentService.createAssignment(req.body);

      const clientUserId = await this.assignmentService.getClientIdByOrderId(assignment.order_id);

      const clientRoom = clientUserId.toString();
      const cifuRoom = assignment.cifu_id.toString();

      io.to(clientRoom).emit('newAssignment', { order_id: assignment.order_id });
      io.to(cifuRoom).emit('newAssignment', {
        assignment_id: assignment.assignment_id,
      });

      res.status(200).json({ assignment });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'cannot create assignment' });
    }
  };

  getCifuList = async (req: Request, res: Response) => {
    try {
      const cifuList = await this.assignmentService.getCifuList();
      res.status(200).json({ cifuList });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'cannot get cifu list' });
    }
  };
  editAssignment = async (req: Request, res: Response) => {
    try {
      const prevCifuUserId = await this.assignmentService.getCifuIdByAssignmentId(
        req.body.assignment_id
      );
      // console.log('body', req.body);
      const assignment = await this.assignmentService.editAssignment(req.body);
      // console.log('assignment', assignment);
      const newCifuUserId = assignment.cifu_id;
      const clientUserId = await this.assignmentService.getClientIdByOrderId(assignment.order_id);
      const clientRoom = clientUserId.toString();

      io.to(clientRoom).emit('editAssignment', { order_id: assignment.order_id });

      // console.log(prevCifuUserId, newCifuUserId);
      if (prevCifuUserId == newCifuUserId) {
        io.to(newCifuUserId.toString()).emit('editAssignment', {
          assignment_id: assignment.assignment_id,
        });
      } else {
        io.to(prevCifuUserId.toString()).emit('deleteAssignment', {
          assignment_id: assignment.assignment_id,
        });
        io.to(newCifuUserId.toString()).emit('newAssignment', {
          assignment_id: assignment.assignment_id,
        });
      }

      res.status(200).json({ assignment });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'cannot update assignment' });
    }
  };
  deleteAssignment = async (req: Request, res: Response) => {
    try {
      const orderStatusUpdated = await this.assignmentService.deleteAssignment(
        req.body.assignment_id,
        req.body.order_id
      );

      const clientUserId = await this.assignmentService.getClientIdByOrderId(req.body.order_id);
      const clientRoom = clientUserId.toString();

      const cifuUserId = await this.assignmentService.getCifuIdByAssignmentId(
        req.body.assignment_id
      );

      io.to(clientRoom).emit('deleteAssignment', { order_id: req.body.order_id });
      io.to(cifuUserId.toString()).emit('deleteAssignment', {
        assignment_id: req.body.assignment_id,
      });

      res.status(200).json(orderStatusUpdated);
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'cannot delete assignment' });
    }
  };
  getAllAssignments = async (req: Request, res: Response) => {
    try {
      let assignmentList;

      //if cifu
      if (req.user?.user_type_id === parseInt(process.env.USERS_CIFU!)) {
        const cifu_id = req.user.user_id;

        assignmentList = await this.assignmentService.getAssignmentListByCifuID(cifu_id);
      }

      //if admin

      if (req.user?.user_type_id === parseInt(process.env.USERS_ADMIN!)) {
        assignmentList = await this.assignmentService.getAssignmentList();
      }

      res.status(200).json({ assignmentList });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'cannot get assignment list' });
    }
  };
  getFilterAssignments = async (req: Request, res: Response) => {};
  getAssignment = async (req: Request, res: Response) => {};

  confirmCompletion = async (req: Request, res: Response) => {
    try {
      if (req.user?.user_type_id === parseInt(process.env.USERS_CIFU!)) {
        uploadCompletionImage(req, res, async (err: any) => {
          if (err instanceof multer.MulterError) {
            //err && err.code === 'LIMIT_UNEXPECTED_FILE'
            res.status(400).json({ message: '只可以加3張圖片' });
            return;
          } else if (err instanceof Error) {
            res.status(400).json({ message: '只可以上載圖片' });
            return;
          }

          if (req.files == undefined || req.files.length === 0) {
            res.status(400).json({ message: '請上載圖片' });
            return;
          }

          const orderId = req.body.orderId;

          const images: string[] = [];

          for (const image of req.files as any) {
            images.push(image.key);
          }

          const assignmentId = parseInt(req.params.assignmentID);

          const uploaded_by = req.user?.user_id;

          const userType = req.user?.user_type_id;

          await this.assignmentService.uploadCompletionImages(
            assignmentId,
            images,
            uploaded_by!,
            userType!
          );

          io.emit('cifuCompletedAssignment', { order_id: orderId });
          res.status(200).json({ message: 'success' });
        });
      }
    } catch (err) {
      res.status(500).json({ message: 'cannot send data to server' });
    }
  };
}
