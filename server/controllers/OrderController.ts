import express, { Request, Response } from 'express';
import { requireClientLogin, requireLogin, requireAdminLogin } from '../guards';
import { OrderService } from '../services/OrderService';

import { uploadOrderImage, uploadPaymentImage } from '../server';
import { io } from '../socketio';

export class OrderController {
  constructor(private orderService: OrderService) {}

  toRouter = () => {
    let router = express.Router();

    router.post('/order', requireLogin, requireClientLogin, this.createOrder);
    router.get('/orders', requireLogin, this.getAllOrders); //admin and client
    router.get('/order/:orderID', requireLogin, this.getOrder); //getOrderByAdmin or Client

    router.post(
      '/order/:orderID/payment-images',
      requireLogin,
      requireClientLogin,
      this.uploadPaymentImages
    );
    router.patch(
      '/order/payment-status',
      requireLogin,
      requireAdminLogin,
      this.updatePaymentStatus
    );
    router.patch('/order/order-status', requireLogin, this.updateOrderStatus);

    return router;
  };

  // TO DO METHODS:
  createOrder = async (req: Request, res: Response) => {
    try {
      uploadOrderImage(req, res, async (err: any) => {
        if (err && err.code === 'LIMIT_UNEXPECTED_FILE') {
          res.status(400).json({ message: '只可以加1張圖片' });
          return;
        } else if (err instanceof Error) {
          res.status(400).json({ message: '只可以上載圖片' });
          return;
        }

        const frontEndData = JSON.parse(req.body.data);

        const {
          repair_category_id,
          repair_item_id_array,
          problem_desc,
          appliance_brand,
          appliance_model,
          appliance_install_location_desc,
          district_id,
          repair_address1,
          repair_address2,
          person_in_charge,
          person_in_charge_prefix,
          contact_tel,
          person_in_charge_job_title,
          contact_email,
        } = frontEndData;

        const images: string[] = [];
        for (const image of req.files as any) {
          images.push(image.key); 
        }

        const user_id = req.user?.user_id!;

        const order_id = await this.orderService.createOrder(
          user_id,
          repair_category_id,
          repair_item_id_array,
          problem_desc,
          appliance_brand,
          appliance_model,
          appliance_install_location_desc,
          district_id,
          repair_address1,
          repair_address2,
          person_in_charge,
          person_in_charge_prefix,
          contact_tel,
          person_in_charge_job_title,
          contact_email,
          images
        );

        res.status(200).json({ message: 'success' });
        io.emit('newOrder', { order_id: order_id });
      });
    } catch (err) {
      res.status(500).json({ message: 'cannot send data to server' });
    }
  };
  getAllOrders = async (req: Request, res: Response) => {
    try {
      let orders;
      //if login by admin
      if (req.user?.user_type_id === parseInt(process.env.USERS_ADMIN!)) {
        orders = await this.orderService.getAllOrders();
      }

      if (req.user?.user_type_id === parseInt(process.env.USERS_CLIENT!)) {
        const client_id = req.user.user_id;

        orders = await this.orderService.getOrdersByUserID(client_id);
      }

      //if login by client
      res.status(200).json({ orders });
    } catch (err) {
      res.status(500).json({ message: 'cannot get orders from database' });
    }
  };
  getFilterOrders = async (req: Request, res: Response) => {};

  getOrder = async (req: Request, res: Response) => {
    try {
      res.status(200).json({ message: 'trying' });
    } catch (err) {
      console.error(err.message);
    }
  };
  uploadPaymentImages = async (req: Request, res: Response) => {
    try {
      uploadPaymentImage(req, res, async (err: any) => {
        if (err && err.code === 'LIMIT_UNEXPECTED_FILE') {
          res.status(400).json({ message: '只可以加1張圖片' });
          return;
        } else if (err instanceof Error) {
          res.status(400).json({ message: '只可以上載圖片' });
          return;
        }

        if (req.files == undefined || req.files.length === 0) {
          res.status(400).json({ message: '請上載圖片' });
          return;
        }

        const images: string[] = [];

        for (const image of req.files as any) {
          images.push(image.key);
        }

        const orderId = parseInt(req.params.orderID);

        await this.orderService.uploadPaymentImages(orderId, images);

        io.emit('paymentProofReceived', { id: req.params.orderID });

        res.status(200).json({ message: 'success' });
      });
    } catch (err) {
      res.status(500).json({ message: 'cannot send data to server' });
    }
  };

  updatePaymentStatus = async (req: Request, res: Response) => {
    try {
      await this.orderService.updatePaymentStatus(req.body.orderId, req.body.paymentStatusId);

      const clientUserId = await this.orderService.getClientIdByOrderId(req.body.orderId);
      const clientRoom = clientUserId.toString();

      if (req.body.paymentStatusId == 2) {
        io.to(clientRoom).emit('quotationSent', { order_id: req.body.orderId });
      }

      res.status(200).json({ message: 'success' });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'cannot update payment status' });
    }
  };

  updateOrderStatus = async (req: Request, res: Response) => {
    try {
      if (req.user?.user_type_id === parseInt(process.env.USERS_CLIENT!)) {
        await this.orderService.completionDateMarkedByClient(req.body.assignmentId);
      }

      await this.orderService.updateOrderStatus(req.body.orderId, req.body.orderStatusId);

      if (req.body.orderStatusId == 4) {
        io.emit('orderCompleted', { id: req.body.orderId });
      }

      if (req.body.orderStatusId == 3) {
        const clientUserId = await this.orderService.getClientIdByOrderId(req.body.orderId);
        const clientRoom = clientUserId.toString();
        io.to(clientRoom).emit('orderCancelled', { order_id: req.body.orderId });
      }

      res.status(200).json({ message: 'success' });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'cannot update order status' });
    }
  };
}
