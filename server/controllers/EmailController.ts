import nodemailer from 'nodemailer';
import { ClientEmailAddress, EmailContent } from 'shared';

const credentials = {
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS,
  },
};

const transporter = nodemailer.createTransport(credentials);

export async function sendEmail(to: ClientEmailAddress, content: EmailContent) {
  const contacts = {
    from: process.env.MAIL_USER,
    to,
  };

  const emailConfig = Object.assign({}, content, contacts);
  try {
    await transporter.sendMail(emailConfig);
  } catch (error) {
    console.error(error.message);
  }
}
