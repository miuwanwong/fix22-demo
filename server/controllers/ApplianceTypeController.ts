import express, { Request, Response } from 'express';
import { requireClientLogin, requireLogin } from '../guards';
import { ApplianceTypeService } from '../services/ApplianceTypeService';

export class ApplianceTypeController {
  constructor(private applianceTypeService: ApplianceTypeService) {}

  toRouter = () => {
    let router = express.Router();

    router.get('/appliance/typesproblems', requireLogin, requireClientLogin, this.getAllApplianceTypesProblems);
    return router;
  };

  todo = (req: Request, res: Response) => {
    res.status(501).json({ err: 'to do' });
  };

  // TO DO METHODS:
  createApplianceType = async (req: Request, res: Response) => {};
  getAllApplianceTypes = async (req: Request, res: Response) => {};
  updateApplianceType = async (req: Request, res: Response) => {};
  getAllApplianceTypesProblems = async (req: Request, res: Response) => {
    try {
      const repairList = await this.applianceTypeService.getAllApplianceTypesProblems();
      res.status(200).json({ repairList });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };
}
