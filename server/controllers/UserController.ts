import express, { Request, Response } from 'express';
import { requireLogin } from '../guards';
import { emailTemplates } from '../models';
import { UserService } from '../services/UserService';
import { sendEmail } from './EmailController';
import dotenv from 'dotenv';
import { LoginInput, RequestCodeInput, JWTPayload } from 'shared';
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';
import { HttpException } from '../utils';

dotenv.config();

export class UserController {
  constructor(private userService: UserService) {}

  toRouter = () => {
    let router = express.Router();

    router.post('/login', this.login);
    router.post('/client/signup', this.requestSignUpCode);
    router.post('/client/code', this.requestLoginCode);
    router.post('/cifu/signup', this.cifuSignUp);
    router.get('/user-status', requireLogin, this.getUserStatus);
    router.post('/logout', this.logout);
    router.patch('/account/:userID', requireLogin, this.updateUserInfo);

    router.get('/accounts/:role', requireLogin, this.getAllAccounts);


    return router;
  };

  requestSignUpCode = async (req: Request, res: Response) => {
    if (!req.body) {
      res.status(401).json({ error: 'no body in req' });
      return;
    }

    const requestCodeInput: RequestCodeInput = req.body;

    if (!requestCodeInput.email) {
      res.status(401).json({ error: 'no email in req' });
      return;
    }

    if (!requestCodeInput.company_name) {
      res.status(401).json({ error: 'no company_name in req' });
      return;
    }

    if (!requestCodeInput.company_tel) {
      res.status(401).json({ error: 'no company_tel in req' });
      return;
    }

    let passcode: string | undefined;

    try {
      passcode = await this.userService.clientRequestCode(requestCodeInput);
    } catch (error) {
      res.status(500).json({ error: 'cannot insert email' });
      return;
    }

    if (!passcode) {
      res.status(401).json({ error: 'email taken / other problems' }); // how does thrown exception get here? how to handle more exceptions?
      return;
    }

    try {
      await sendEmail(requestCodeInput.email, emailTemplates.signup(passcode));
    } catch (error) {
      res.status(500).json({ error: 'cannot send email' });
      return;
    }

    res.status(200).json(passcode);
  };

  login = async (req: Request, res: Response) => {
    console.log('login controller reached');

    if (!req.body) {
      res.status(401).json({ error: 'missing body in req' });
      return;
    }

    const loginInput: LoginInput = req.body;

    let payload: JWTPayload | undefined;

    try {
      payload = await this.userService.login(loginInput);
      if (!payload) {
        res.status(401).json({ error: 'user not found' });
        return;
      }
    } catch (error) {
      res.status(500).json(error.message); 
      return;
    }

    if (!jwt.jwtSecret) {
      throw new HttpException(500, "missing 'JWT_SECRET' in env");
    }

    const token = jwtSimple.encode(payload, jwt.jwtSecret);

    req.user = payload; // to return the user's info to request. for using user's info in subsequent request.
    console.log(`leaving controller with encoded token: ${token}`);

    res.json({ token });
  };

  requestLoginCode = async (req: Request, res: Response) => {
    console.log(`logincode controller reached`);
    const requestCodeInput: RequestCodeInput = req.body;

    if (!requestCodeInput.email) {
      res.status(401).json({ error: 'no email in req' });
      return;
    }

    try {
      const passcode = await this.userService.clientRequestCode(requestCodeInput);
      if (!passcode) {
        res
          .status(401)
          .json({ error: 'You did not have an account with us. Please register a new account!' });
        return;
      }

      await sendEmail(req.body.email, emailTemplates.login(passcode));

      res.status(200).json(passcode);
    } catch (error) {
      res.status(500).json({ error });
    }
  };

  getUserStatus = async (req: Request, res: Response) => {
    console.log(JSON.stringify(req.user));

    res.json(req.user);
  };

  logout = async (req: Request, res: Response) => {
    req.user = null;
    res.json({ message: 'req.user cleared' });
  };

  updateUserInfo = async (req: Request, res: Response) => {
    if (!req.body) {
      return res.status(500).json({ error: 'missing req.body' });
    }

    const info = req.body.info;
    const user_id = req.body.user_id;

    if (!req.user) {
      return res.status(500).json({ error: 'missing req.user' });
    }
    const user_type_id = req.user.user_type_id;

    if (!process.env.USERS_CIFU || !process.env.USERS_CLIENT || !process.env.USERS_ADMIN) {
      return res.status(500).json({ error: 'missing env' });
    }

    let result: number | undefined;
    if (
      user_type_id === parseInt(process.env.USERS_CLIENT) ||
      user_type_id === parseInt(process.env.USERS_ADMIN)
    ) {
      result = await this.userService.updateUserInfo(info, user_id);
    } else {
      return res.status(500).json({ error: 'User has no permission to update' });
    }

    if (!result) return res.status(500).json({ error: 'Cannot update' });

    return res.status(200).json({ message: 'updated' });
  };

  cifuSignUp = async (req: Request, res: Response) => {
    if (!req.body) return res.status(401).json({ message: 'Missing req.body' });

    const info = req.body;

    const result = await this.userService.cifuSignUp(info);

    if (!result) return res.status(401).json({ message: 'cannot create account' });

    return res.status(200).json({ message: 'account created' });
  };

  // TO DO METHODS:

  todo = (req: Request, res: Response) => {
    res.status(501).json({ err: 'to do' });
  };

  getAllAccounts = async (req: Request, res: Response) => {
    try {
      let accounts;

      const user_id = req.user?.user_id
     
     
      if(req.params.role == 'client' &&　req.user?.user_type_id === parseInt(process.env.USERS_CLIENT!)){
        
        accounts = await this.userService.getOneClientAccount(user_id!)
     
      }


      if (req.params.role == 'client' && req.user?.user_type_id === parseInt(process.env.USERS_ADMIN!)) {
        accounts = await this.userService.getAllClientAccounts();
      }

      if(req.params.role == 'cifu' && req.user?.user_type_id === parseInt(process.env.USERS_ADMIN!)){
        accounts = await this.userService.getAllCifuAccounts();
      }

      res.status(200).json({ accounts });
    } catch (err) {
      res.status(500).json({ message: `cannot get ${req.params.role}s' accounts` });
    }
  };

  createAdminAccount = async (req: Request, res: Response) => {};

  getCifuPaymentStatus = async (req: Request, res: Response) => {};
}
