import { Knex } from 'knex';

export class OrderService {
  constructor(private knex: Knex) {}

  async getAllOrders() {
    const order_list = await this.knex('orders')
      .select(
        'orders.*',
        'repair_categories.repair_category_name',
        'clients.company_name',
        'districts.district_name',
        'order_status.order_status',
        'payment_status.payment_status'
      )
      .leftJoin(
        'repair_categories',
        'repair_categories.repair_category_id',
        'orders.repair_category_id'
      )
      .leftJoin('clients', 'clients.user_id', 'orders.user_id')
      .leftJoin('districts', 'districts.district_id', 'orders.district_id')
      .leftJoin('order_status', 'orders.order_status_id', 'order_status.order_status_id')
      .leftJoin('payment_status', 'orders.payment_status_id', 'payment_status.payment_status_id')
      .orderBy('orders.order_id');

    const order_id_list: number[] = order_list.map((row) => row.order_id);

    const assignment_list = await this.knex('assignments')
      .whereIn('order_id', order_id_list)
      .andWhere({ is_active: true })
      .orderBy('created_at', 'desc');
    const order_images_list = await this.knex('order_images').whereIn('order_id', order_id_list);
    const payment_images_list = await this.knex('payment_images').whereIn(
      'order_id',
      order_id_list
    );

    const assignment_id_list: number[] = assignment_list.map((row) => row.assignment_id);
    const completion_images_list = await this.knex('completion_images').whereIn(
      'assignment_id',
      assignment_id_list
    );

    const selected_repair_items_list = await this.knex('selected_repair_items')
      .leftJoin(
        'repair_items',
        'repair_items.repair_item_id',
        'selected_repair_items.repair_item_id'
      )
      .whereIn('order_id', order_id_list);

    const cifu_list = await this.knex('cifu').select('user_id', 'name');

    return {
      order_list: Object.fromEntries(order_list.map((row: any) => [row.order_id, row])),
      assignment_list: Object.fromEntries(
        assignment_list.map((row: any) => [row.assignment_id, row])
      ),
      selected_repair_items_list,
      order_images_list,
      payment_images_list,
      completion_images_list,
      cifu_list,
    };
  }

  async getOrdersByUserID(client_id: number) {
    const order_list = await this.knex('orders')
      .select('orders.*', 'repair_category_name', 'clients.company_name', 'district_name')
      .leftJoin(
        'repair_categories',
        'repair_categories.repair_category_id',
        'orders.repair_category_id'
      )
      .leftJoin('clients', 'clients.user_id', 'orders.user_id')
      .leftJoin('districts', 'districts.district_id', 'orders.district_id')
      .where({ 'orders.user_id': client_id });

    const order_id_list: number[] = order_list.map((row) => row.order_id);

    const assignment_list = await this.knex('assignments')
      .whereIn('order_id', order_id_list)
      .andWhere({ is_active: true })
      .orderBy('created_at', 'desc');

    const selected_repair_items_list = await this.knex('selected_repair_items')
      .leftJoin(
        'repair_items',
        'repair_items.repair_item_id',
        'selected_repair_items.repair_item_id'
      )
      .whereIn('order_id', order_id_list);

    return { order_list, assignment_list, selected_repair_items_list };
  }

  async createOrder(
    user_id: number,
    repair_category_id: number,
    repair_item_id_array: number[],
    problem_desc: string,
    appliance_brand: string,
    appliance_model: string,
    appliance_install_location_desc: string,
    district_id: number,
    repair_address1: string,
    repair_address2: string,
    person_in_charge: string,
    person_in_charge_prefix: string,
    contact_tel: number,
    person_in_charge_job_title: string,
    contact_email: string,
    images: string[]
  ) {
    return await this.knex.transaction(async (trx) => {
      const [orderID] = await trx('orders')
        .insert({
          user_id,
          repair_category_id,
          problem_desc,
          appliance_brand,
          appliance_model,
          appliance_install_location_desc,
          district_id,
          repair_address1,
          repair_address2,
          person_in_charge,
          person_in_charge_prefix,
          contact_tel,
          person_in_charge_job_title,
          contact_email,
        })
        .returning('order_id');

      for (const repairID of repair_item_id_array) {
        await trx('selected_repair_items').insert({
          order_id: orderID,
          repair_item_id: repairID,
        });
      }

      for (const image of images) {
        await trx('order_images').insert({
          order_id: orderID,
          image_file_name: image,
        });
      }
      
      return orderID;
    });
  }

  async uploadPaymentImages(orderId: number, images: string[]) {
    await this.knex.transaction(async (trx) => {
      for (const image of images) {
        await trx('payment_images').insert({
          order_id: orderId,
          image_file_name: image,
        });
      }

      await trx('orders')
        .update({
          payment_status_id: 3,
        })
        .where({ order_id: orderId });
    });
  }

  async updatePaymentStatus(order_id: number, payment_status_id: string) {
    await this.knex('orders').update({ payment_status_id }).where({ order_id });
  }

  async updateOrderStatus(order_id: number, order_status_id: string) {
    await this.knex('orders').update({ order_status_id }).where({ order_id });
  }

  async completionDateMarkedByClient(assignment_id: number) {
    await this.knex('assignments')
      .update({
        mark_complete_by_client: new Date(),
      })
      .where({ assignment_id });
  }

  async getClientIdByOrderId(order_id: number) {
    const { user_id } = await this.knex('orders').select('user_id').where({ order_id }).first();
    return user_id;
  }
}
