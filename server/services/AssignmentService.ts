import { Knex } from 'knex';

interface Assignment {
  assignment_id?: number;
  order_id: number;
  cifu_id: number;
  repair_date: Date;
  repair_time: string;
  remarks: string;
  total_amount_to_cifu: number;
}

export class AssignmentService {
  constructor(private knex: Knex) {}

  async createAssignment(assignment: Assignment) {
    return await this.knex.transaction(async (trx) => {
      await trx('orders').update({ order_status_id: 2 }).where({ order_id: assignment.order_id });
      const [assignmentID] = await trx('assignments').insert(assignment).returning('assignment_id');
      const returnAssignment = await trx('assignments')
        .select('assignments.*')
        .from('assignments')
        .leftJoin('orders', 'orders.order_id', 'assignments.order_id')
        .where('assignment_id', assignmentID)
        .first();
      return returnAssignment;
    });
  }

  async getCifuList() {
    const cifuList = await this.knex.select('user_id', 'name').from('cifu');
    return cifuList;
  }

  async getAssignmentListByCifuID(cifu_id: number) {
    const assignment_list = await this.knex('assignments')
      .where({ cifu_id })
      .andWhere({ is_active: true })
      .orderBy('repair_date', 'desc');
    const order_id_list: number[] = assignment_list.map((row) => row.order_id);

    const selected_repair_items_list = await this.knex('selected_repair_items')
      .leftJoin(
        'repair_items',
        'repair_items.repair_item_id',
        'selected_repair_items.repair_item_id'
      )
      .whereIn('order_id', order_id_list);

    const order_list = await this.knex('orders')
      .select('orders.*', 'repair_category_name', 'company_name', 'district_name')
      .leftJoin(
        'repair_categories',
        'repair_categories.repair_category_id',
        'orders.repair_category_id'
      )
      .leftJoin('clients', 'clients.user_id', 'orders.user_id')
      .leftJoin('districts', 'districts.district_id', 'orders.district_id')
      .whereIn('order_id', order_id_list);

    const order_images_list = await this.knex('order_images').whereIn('order_id', order_id_list);

    return { assignment_list, order_list, order_images_list, selected_repair_items_list };
  }

  async getAssignmentList() {
    const assignmentList = await this.knex
      .select(
        'assignments.*',
        this.knex.ref('cifu.name').as('cifu_name'),
        'districts.district_name'
      )
      .from('assignments')
      .leftJoin('cifu', 'cifu.user_id', 'assignments.cifu_id')
      .leftJoin('orders', 'orders.order_id', 'assignments.order_id')
      .leftJoin('districts', 'districts.district_id', 'orders.district_id')
      .where({ 'assignments.is_active': true })
      .orderBy('repair_date', 'repair_time');
    return assignmentList;
  }

  async uploadCompletionImages(
    assignment_id: number,
    images: string[],
    uploaded_by: number,
    userType: number
  ) {
    await this.knex.transaction(async (trx) => {
      for (const image of images) {
        await trx('completion_images').insert({
          assignment_id,
          image_file_name: image,
          uploaded_by,
        });
      }

      //remark if cifu
      if (userType === parseInt(process.env.USERS_CIFU!)) {
        await trx('assignments')
          .update({
            mark_complete_by_cifu: new Date(),
          })
          .where({ assignment_id });
      }
    });
  }

  async editAssignment(assignment: Assignment) {
    return await this.knex.transaction(async (trx) => {
      await trx('assignments').update(assignment).where('assignment_id', assignment.assignment_id);
      await trx('assignments')
        .update({ cifu_payment_status: false })
        .where('assignment_id', assignment.assignment_id);

      const returnAssignment = await trx('assignments')
        .select(
          'assignments.*',
          this.knex.ref('cifu.name').as('cifu_name'),
          'districts.district_name'
        )
        .from('assignments')
        .leftJoin('cifu', 'cifu.user_id', 'assignments.cifu_id')
        .leftJoin('orders', 'orders.order_id', 'assignments.order_id')
        .leftJoin('districts', 'districts.district_id', 'orders.district_id')
        .where('assignment_id', assignment.assignment_id)
        .andWhere({ is_active: true })
        .first();
      return returnAssignment;
    });
  }

  async deleteAssignment(assignment_id: number, order_id: number) {
    return await this.knex.transaction(async (trx) => {
      await trx('assignments').where('assignment_id', assignment_id).update({ is_active: false });

      const checkAssignmentForSameOrder = await trx('assignments')
        .select('assignment_id')
        .where('order_id', order_id)
        .andWhere('is_active', true);

      let orderStatusUpdated = false;

      if (checkAssignmentForSameOrder.length === 0) {
        await trx('orders').update({ order_status_id: 1 }).where('order_id', order_id);
        orderStatusUpdated = true;
      }

      return orderStatusUpdated;
    });
  }

  async getClientIdByOrderId(order_id: number) {
    const { user_id } = await this.knex('orders').select('user_id').where({ order_id }).first();
    return user_id;
  }

  async getCifuIdByAssignmentId(assignment_id: number) {
    const { cifu_id } = await this.knex('assignments')
      .select('cifu_id')
      .where({ assignment_id })
      .first();
    return cifu_id;
  }
}
