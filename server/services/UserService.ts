import { Knex } from 'knex';
import { HttpException, makeRandomPasscode } from '../utils';
import { checkPassword, hashPassword } from '../hash';
import { LoginInput, RequestCodeInput, User, JWTPayload } from 'shared';
import dotenv from 'dotenv';
dotenv.config();

export class UserService {
  constructor(private knex: Knex) {}

  async clientRequestCode(requestCodeInput: RequestCodeInput, knex = this.knex) {
    const { email, company_name, company_tel } = requestCodeInput;

    const user_type_id = process.env.USERS_CLIENT;

    console.log('service reached');

    let passcode: string;
    let duplicateCode: undefined[] | string[];

    while (true) {
      passcode = makeRandomPasscode();
      duplicateCode = await knex('users')
        .select('passcode')
        .where({ passcode })
        .returning('passcode');

      if (duplicateCode.length === 0) {
        console.log(passcode);
        break;
      }
    }

    // Old user: generate new passcode
    if (!company_name && !company_tel) {
      try {
        [passcode] = await knex('users')
          .update({ passcode })
          .where({ email })
          .returning('passcode');
        return passcode;
      } catch (error) {
        console.error(error.message);
        return;
      }
    }

    // New user: generate passcode, insert details
    try {
      await knex.transaction(async (trx) => {
        const [user_id] = await trx('users')
          .insert({ user_type_id, email, passcode })
          .returning('user_id');

        await trx('clients').insert({
          user_id,
          company_name,
          company_tel,
        });
      });
    } catch (error) {
      console.error(error.message);

      if (error.toString().includes('duplicate key')) {
        throw new HttpException(400, 'requested email is already used'); // where will this throw to?
      }
      return;
    }

    return passcode;
  }

  async login(loginInput: LoginInput, knex = this.knex): Promise<JWTPayload | undefined> {
    console.log('service reached');

    const { username, password, email, passcode } = loginInput;

    let user: User | undefined;

    // CIFU LOGIN
    if (username && password) {
      console.log('cifu service');

      try {
        [user] = await knex('users')
          .join('cifu', 'users.user_id', 'cifu.user_id')
          .select(
            'users.user_id as user_id',
            'user_type_id',
            'cifu.name as name',
            'cifu.password as hash_password'
          )
          .where({ username });
      } catch (error) {
        console.log(error.message);
        throw new Error(error.message);
      }

      if (user && user.hash_password) {
        try {
          const matched = await checkPassword(password, user.hash_password);
          if (matched) {
            const payload: JWTPayload = {
              user_id: user.user_id,
              user_type_id: user.user_type_id,
              name: user.name,
            };
            return payload;
          }
        } catch (error) {
          console.log(error.message);
          throw new Error(error.message.toString());
        }
      }
    }

    // CLIENT LOGIN
    if (passcode) {
      console.log('client service');
      try {
        await knex.transaction(async (trx) => {
          [user] = await trx('users')
            .select('user_id', 'user_type_id', 'email', 'email_verified')
            .where({ passcode });

          if (user && !user.email_verified) {
            [user] = await trx('users')
              .update('email_verified', 'true')
              .where({ passcode })
              .returning(['user_id', 'user_type_id', 'email']);
          }
        });

        if (user) {
          const payload: JWTPayload = {
            user_id: user.user_id,
            user_type_id: user.user_type_id,
            email: user.email,
          };

          return payload;
        }
        return user;
      } catch (error) {
        throw new Error(error.message.toString());
      }
    }

    // ADMIN LOGIN
    if (email && password) {
      console.log('admin service');

      try {
        [user] = await knex('users')
          .join('admin', 'users.user_id', 'admin.user_id')
          .select(
            'users.user_id as user_id',
            'user_type_id',
            'admin.name as name',
            'admin.password as hash_password'
          )
          .where({ email });
      } catch (error) {
        console.log(error.message);

        throw new Error(error.message);
      }

      if (user && user.hash_password) {
        try {
          const matched = await checkPassword(password, user.hash_password);
          if (matched) {
            const payload: JWTPayload = {
              user_id: user.user_id,
              user_type_id: user.user_type_id,
              name: user.name,
            };

            return payload;
          }
        } catch (error) {
          console.log(error.message);

          throw new Error(error.message.toString());
        }
      }
    }

    return;
  }

  async checkLogin(
    user_id: number,
    user_type_id: number,
    knex = this.knex
  ): Promise<JWTPayload | undefined> {
    let user: JWTPayload | undefined;

    console.log('service check login');

    if (!process.env.USERS_CIFU || !process.env.USERS_CLIENT || !process.env.USERS_ADMIN) {
      console.error('missing env');
      throw new Error('missing env');
    }

    // SELECT USER INFO ACCORDING TO USER TYPE
    switch (user_type_id) {
      case parseInt(process.env.USERS_CIFU): {
        // select cifu info
        try {
          [user] = await knex('users')
            .join('cifu', 'users.user_id', 'cifu.user_id')
            .select('users.user_id as user_id', 'user_type_id', 'cifu.name as name')
            .where({ 'users.user_id': user_id });
          return user;
        } catch (error) {
          console.error(error);

          throw new Error(error);
        }
      }

      case parseInt(process.env.USERS_CLIENT): {
        // select client info
        try {
          await knex.transaction(async (trx) => {
            [user] = await trx('users')
              .select('user_id', 'user_type_id', 'email')
              .where({ 'users.user_id': user_id });
          });
          return user;
        } catch (error) {
          console.error(error);

          throw new Error(error.message.toString());
        }
      }

      case parseInt(process.env.USERS_ADMIN): {
        // select admin info
        try {
          [user] = await knex('users')
            .join('admin', 'users.user_id', 'admin.user_id')
            .select('users.user_id as user_id', 'user_type_id', 'admin.name as name')
            .where({ 'users.user_id': user_id });
          return user;
        } catch (error) {
          console.error(error);

          throw new Error(error.message);
        }
      }
    }
    throw new Error('user not active');
  }

  async updateUserInfo(info: JWTPayload, user_id: number, user_type_id?: number, knex = this.knex) {
    if (!process.env.USERS_CIFU || !process.env.USERS_CLIENT || !process.env.USERS_ADMIN) {
      throw new Error('missing env');
    }

    let id: number | undefined;



    console.log(JSON.stringify(info))

    try {
      Object.keys(info).forEach((key) => {
        if (info[key].length === 0 || info[key] == undefined ) delete info[key];

      });

      console.log(`here: ${JSON.stringify(info)}`)

      if (JSON.stringify(info) === JSON.stringify({})) return

      id = await knex('clients').update(info).where({ user_id });

      return id;
    } catch (error) {
      console.log(error.message);
      throw new Error(error.message);
    }
  }

  async getAllClientAccounts() {
    const client_list = await this.knex
      .select('clients.*', 'users.email', 'users.email_verified', 'district_name')
      .from('clients')
      .leftJoin('users', 'clients.user_id', 'users.user_id')
      .leftJoin('districts', 'districts.district_id', 'clients.district_id');

    return { client_list };
  }

  async getOneClientAccount(user_id:number){
    const client_list = await this.knex
    .select('clients.*', 'users.email', 'users.email_verified', 'district_name')
      .from('clients')
      .where({'clients.user_id':user_id})
      .leftJoin('users', 'clients.user_id', 'users.user_id')
      .leftJoin('districts', 'districts.district_id', 'clients.district_id')
      ;
      return {client_list}
  }

  async getAllCifuAccounts(){
    const cifu_list = await this.knex
    .select('cifu.user_id', 'cifu.name','cifu.tel', 'users.email')
    .from('cifu')
    .leftJoin('users', 'users.user_id', 'cifu.user_id')

    return { cifu_list };
  }

  async cifuSignUp(info: User, knex = this.knex) {
    const user_type_id = process.env.USERS_CIFU;

    let id: number | undefined;

    try {
      await knex.transaction(async (trx) => {
        const [user_id] = await trx('users').insert({ user_type_id }).returning('user_id');

        let { password } = info;

        if (password) {
          password = await hashPassword(password);
        }

        const infoWithUserID = { ...info, user_id, password };

        id = await trx('cifu').insert(infoWithUserID);
      });
    } catch (error) {
      if (error.toString().includes('duplicate key')) {
        throw new HttpException(400, 'requested username is already used'); // where will this throw to?
      }

      console.error(error.message);
    }
    return id;
  }


}
