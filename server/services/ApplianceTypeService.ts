import { Knex } from 'knex';

export class ApplianceTypeService {
  constructor(private knex: Knex) { }

  async getAllApplianceTypesProblems() {
    const repairCategories = await this.knex("repair_categories")
    const repairTypes = await this.knex("repair_items")

    for (const category of repairCategories) {
      category["service_list"] = []
      for (const item of repairTypes) {
        if (category.repair_category_id === item.repair_category_id) {
          delete item.repair_category_id
          category["service_list"].push(item)
        }
      }
      
    }
 
    return repairCategories

  }




}
