import dotenv from 'dotenv';
dotenv.config();
import { JWTPayload } from 'shared';

export const emailTemplates = {
  signup: (passcode: string) => ({
    subject: '驗證您的電郵地址',
    html: `
    <h3>您的驗證碼：${passcode}</h3>
    
     <a href='${process.env.REACT_APP_CLIENT_ORIGIN}/client/login?passcode=${passcode}'>
       <div>按此直接登入</div>
     </a>
     <div>此電郵由系統自動發出</div>
     `,
    text: `Copy and paste this link: ${process.env.REACT_APP_CLIENT_ORIGIN}/client/login?passcode=${passcode}`,
  }),
  login: (passcode: string) => ({
    subject: '驗證碼登入',
    html: `
    <h3>您的驗證碼：${passcode}</h3>
    <a href='${process.env.REACT_APP_CLIENT_ORIGIN}/client/login?passcode=${passcode}'>
      <div>按此直接登入</div>
    </a>

    <div>此電郵由系統自動發出</div>
     `,
    text: `Copy and paste this link: ${process.env.REACT_APP_CLIENT_ORIGIN}/client/login?passcode=${passcode}`,
  }),
};

declare global {
  namespace Express {
    interface Request {
      user?: JWTPayload | null;
    }
  }
}
