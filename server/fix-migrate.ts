import { knex } from './db'

async function main() {
  if (await knex.schema.hasTable('knex_migrations')) {
    const rows = await knex('knex_migrations').select('id', 'name')
    for (let row of rows) {
      await knex('knex_migrations')
        .update({ name: row.name.replace('.ts', '.js') })
        .where({ id: row.id })
    }
  }
  await knex.destroy()
}
main()
