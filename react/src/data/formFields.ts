import { FormFields } from 'shared';

export const clientSignUpFields: FormFields = [
  {
    label: `公司電郵 (用作登入帳戶) *`,
    props: {
      name: 'email',
      type: 'email' as const,
      required: true,
      placeholder: '',
      pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$", // Not working yet
    },
  },
  {
    label: '公司名稱 *',
    props: {
      name: 'company_name',
      type: 'text' as const,
      required: true,
      placeholder: '',
    },
  },
  {
    label: '聯絡電話 *',
    props: {
      name: 'company_tel',
      type: 'tel' as const,
      required: true,
      placeholder: '',
      minlength: 8,
      maxlength: 8,
    },
  },
];

export const clientInfoFields: FormFields = [
  {
    label: '公司地址（第一行）*',
    props: {
      name: 'company_address1',
      type: 'text' as const,
      placeholder: '街道/門牌號碼/樓宇名稱',
    },
  },
  {
    label: '公司地址（第二行）',
    props: {
      name: 'company_address2',
      type: 'text' as const,
      placeholder: '座數/樓層/單位號碼',
    },
  },
  {
    label: '地區',
    props: {
      name: 'district_id',
      placeholder: '請選擇',
      value: '',
      selectOptions: [
        { value: '', placeholder: '' },
        { value: 1, placeholder: '中西區' },
        { value: 2, placeholder: '東區' },
        { value: 3, placeholder: '南區' },
        { value: 4, placeholder: '灣仔區' },
        { value: 5, placeholder: '九龍城區' },
        { value: 6, placeholder: '觀塘區' },
        { value: 7, placeholder: '深水埗區' },
        { value: 8, placeholder: '黃大仙區' },
        { value: 9, placeholder: '油尖旺區' },
        { value: 10, placeholder: '葵青區' },
        { value: 11, placeholder: '西貢區' },
        { value: 12, placeholder: '沙田區' },
        { value: 13, placeholder: '大埔區' },
        { value: 14, placeholder: '荃灣區' },
        { value: 15, placeholder: '元朗區' },
        { value: 16, placeholder: '屯門區' },
        { value: 17, placeholder: '北區' },
        { value: 18, placeholder: '離島區' },
      ],
    },
  },
  {
    label: '負責人姓名*',
    props: {
      name: 'person_in_charge',
      type: 'text' as const,
      placeholder: '請填寫',
    },
  },
  {
    label: '稱呼*',
    props: {
      name: 'person_in_charge_prefix',
      placeholder: '請選擇',
      value: '',
      selectOptions: [
        { value: '', placeholder: '' },
        { value: '先生', placeholder: '先生' },
        { value: '女士', placeholder: '女士' },
        { value: '小姐', placeholder: '小姐' },
        { value: '其他', placeholder: '其他' },
      ],
    },
  },
  {
    label: '職銜*',
    props: {
      name: 'person_in_charge_job_title',
      type: 'text' as const,
      placeholder: '請填寫',
    },
  },
];

export const cifuSignUpFields: FormFields = [
  {
    label: '登入名稱',
    props: {
      name: 'username',
      type: 'text' as const,
      required: true,
      placeholder: '請填寫',
    },
  },
  {
    label: '登入密碼',
    props: {
      name: 'password',
      type: 'password' as const,
      required: true,
      placeholder: '請填寫',
    },
  },
  {
    label: '師傅姓名',
    props: {
      name: 'name',
      type: 'text' as const,
      required: true,
      placeholder: '請填寫',
    },
  },
  {
    label: '師傅電話',
    props: {
      name: 'tel',
      type: 'tel' as const,
      required: true,
      placeholder: '請填寫',
      minlength: 8,
      maxlength: 8,
    },
  },
];
