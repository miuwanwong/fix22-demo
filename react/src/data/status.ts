export const statusReference = Object.freeze({
  orderStatusPendingAssignment: 1,
  orderStatusAssigned: 2,
  orderStatusCancelled: 3,
  orderStatusCompleted: 4,
  paymentStatusPendingQuotation: 1,
  paymentStatusPendingPayment: 2,
  paymentStatusPaymentImageSubmitted: 3,
  paymentStatusPaymentConfirmed: 4,
});
