import React, { useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { useDispatch } from 'react-redux';
import { AssignmentState } from '../redux/data/state';
import { editAssignmentThunk } from '../redux/data/thunk';
import Swal from 'sweetalert2';
import { useIonToast } from '@ionic/react';

type CifuType = {
    user_id: number;
    name: string;
};

type Props = {
    assignment: AssignmentState;
    hideAssignmentForm: () => void;
};

function EditAssignmentForm({ assignment, hideAssignmentForm }: Props) {
    const dispatch = useDispatch();
    const [presentToast, dismissToast] = useIonToast();
    
    const onEditAssignmentClick = () => {
        Swal.fire({
            title: `確定更新至下列安排？`,
            text: `${cifuList?.find((cifu) => cifu.user_id == cifu_id)?.name}  ${
                repair_date.getMonth() + 1
            }月${repair_date.getDate()}日${repair_time == 'am' ? '上午' : '下午'}`,
            icon: 'warning',
            heightAuto: false,
            showCancelButton: true,
            confirmButtonColor: '#6baa41',
            cancelButtonColor: '#7d8491ff',
            confirmButtonText: '確定更新',
            cancelButtonText: '取消',
        }).then((result) => {
            if (result.isConfirmed) {
                hideAssignmentForm();

                dispatch(
                    editAssignmentThunk(
                        assignment.assignment_id!,
                        assignment.order_id,
                        cifu_id,

                        repair_date.toISOString(),
                        repair_time,
                        remarks,
                        total_amount_to_cifu
                    )
                );

                presentToast({
                    message: `維修安排已更新!`,
                    duration: 3000,
                    color: 'success',
                });
            }
        });
    };

    //for assigning Cifu
    const [cifuList, setCifuList] = useState<CifuType[] | null>(null);
    const [cifu_id, setCifuID] = useState<number>(assignment.cifu_id);
    const [repair_date, setRepairDate] = useState<Date>(new Date(assignment.repair_date));
    const [repair_time, setRepairTime] = useState<string>(assignment.repair_time);
    const [remarks, setRemarks] = useState<string>(assignment.remarks);
    const [total_amount_to_cifu, setTotalAmountToCifu] = useState<number>(
        assignment.total_amount_to_cifu == undefined ? 0 : assignment.total_amount_to_cifu
    );

    useEffect(() => {
        const token = localStorage.getItem('token');
        const url = `${process.env.REACT_APP_API_SERVER}/assignment/get-cifu-list`;
        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setCifuList(data.cifuList);
                if (data.cifuList.length > 0) {
                    setCifuID(parseInt(data.cifuList[0].user_id));
                }
            });
    }, []);

    return (
        <form>
            <h5>更改安排</h5>
            {cifuList && (
                <div>
                    <label>選擇師傅</label>
                    <select
                        defaultValue={cifu_id}
                        onChange={(e) => {
                            setCifuID(parseInt(e.target.value));
                        }}
                    >
                        {cifuList.map((cifu) => (
                            <option key={'cifu' + cifu.user_id} value={cifu.user_id}>
                                {cifu.name}
                            </option>
                        ))}
                    </select>
                </div>
            )}
            <div>
                <label>選擇日期</label>
                {(() => {
                    return (
                        <DatePicker
                            selected={repair_date}
                            onChange={(date: Date) => {
                                // console.log(date)
                                setRepairDate(date);
                            }}
                        />
                    );
                })()}
            </div>
            <div>
                <label>選擇時間</label>
                <select defaultValue={repair_time} onChange={(e) => setRepairTime(e.target.value)}>
                    <option key="am" value="am">
                        上午
                    </option>
                    <option key="pm" value="pm">
                        下午
                    </option>
                </select>
            </div>
            <div>
                <label>師傅工資</label>
                $
                <input
                    defaultValue={total_amount_to_cifu}
                    type="number"
                    name="totalAmountToCifu"
                    onChange={(e) => {
                        if (!isNaN(parseInt(e.target.value)))
                            setTotalAmountToCifu(parseInt(e.target.value));
                    }}
                />
            </div>
            <div>
                <label>備註</label>
                <input
                    value={remarks}
                    type="text"
                    name="remarks"
                    onChange={(e) => setRemarks(e.target.value)}
                />
            </div>
            <div>
                <button className="small_button" type="button" onClick={onEditAssignmentClick}>
                    更改安排
                </button>
            </div>
        </form>
    );
}

export default EditAssignmentForm;
