import React, { useState } from 'react'
import './Admin.scss';
import { statusReference } from '../data/status';
import { useDispatch } from 'react-redux';
import { getAllOrderListThunk, updateOrderStatusThunk, updatePaymentStatusThunk } from '../redux/data/thunk';
import Swal from 'sweetalert2';
import { useIonToast } from '@ionic/react';


type Props = {
    order_id: number;
    payment_status_id: number;
};

function ChangePaymentStatusForm({
    order_id,
    payment_status_id
}: Props) {

    const dispatch = useDispatch()
    const [paymantStatusId, selectedPaymentStatus] = useState<number>(payment_status_id)
    const [formDisplay, setFormDisplay] = useState<boolean>(false)

    const [presentToast, dismissToast] = useIonToast();

    function changePaymentStatus() {

        Swal.fire({
            title: `確定更改訂單付款狀態?`,
            text: '',
            icon: 'warning',
            heightAuto: false,
            showCancelButton: true,
            confirmButtonColor: '#6baa41',
            cancelButtonColor: '#7d8491ff',
            confirmButtonText: '確定更改',
            cancelButtonText: '取消更改',
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(updatePaymentStatusThunk(order_id, paymantStatusId))
                presentToast({
                    message: `訂單#${order_id}的付款狀態已更新`,
                    duration: 3000,
                    color: 'success',
                });
            }
        });
    };

    return (
        <>
            <button className="admin_button"
                onClick={() => {
                    setFormDisplay(!formDisplay);
                }}>
                更新付款狀態
                {formDisplay ? (
                    <i className="fa fa-caret-up"></i>
                ) : (
                    <i className="fa fa-caret-down"></i>
                )}
            </button>
            {formDisplay &&
                    <div className="dashboard__form">
                        <select
                            onChange={(e) => {
                                selectedPaymentStatus(parseInt(e.target.value));
                            }}
                            defaultValue={payment_status_id}
                        >
                            <option value={statusReference.paymentStatusPendingQuotation}>待報價</option>
                            <option value={statusReference.paymentStatusPendingPayment}>已發報價單</option>
                            <option value={statusReference.paymentStatusPaymentImageSubmitted} disabled={true}>待確認收款</option>
                            <option value={statusReference.paymentStatusPaymentConfirmed}>已收款</option>
                        </select>
                        <button className="small_button"
                            type="button"
                            onClick={() => changePaymentStatus()}
                        >
                            更改
                        </button>
                    </div>
            }

        </>
    );
};

export default ChangePaymentStatusForm