import { IonContent, IonPage } from '@ionic/react';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../redux';
import { getAllOrderListThunk } from '../redux/data/thunk';
import Navbar from './Navbar'
import { AssignmentItemState, OrderItemState } from './OrderItem';
import OrderList from './OrderList';
import Sidebar from './Sidebar';

function AdminOrderPage() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        if (orderList.length !== 0) return;
        dispatch(getAllOrderListThunk());

    }, [dispatch]);

    const openSidebar = () => {
        setSidebarOpen(true);
    };

    const closeSidebar = () => {
        setSidebarOpen(false);
    };

    const orderList: OrderItemState[] = useSelector((state: RootState): OrderItemState[] => {
        const orderList: OrderItemState[] = [];

        Object.values(state.data.order_list).forEach((order) => {

            const assignment_list: AssignmentItemState[] = [];
            Object.values(state.data.assignment_list).forEach((assignment) => {
                if (assignment.order_id == order.order_id) {
                    assignment_list.push({
                        assignment,
                        cifu_name: Object.values(state.data.cifu_list).find(
                            (cifu) => cifu.user_id == assignment.cifu_id
                        )?.name!,
                        completion_images_list: Object.values(
                            state.data.completion_images_list
                        ).filter((image) => image.assignment_id == assignment.assignment_id),
                    });
                }
            });

            orderList.push({
                order,
                assignment_list,
                order_images_list: Object.values(state.data.order_images_list).filter(
                    (image) => image.order_id == order.order_id
                ),
                payment_images_list: Object.values(state.data.payment_images_list).filter(
                    (image) => image.order_id == order.order_id
                ),
                repair_item_list: Object.values(state.data.selected_repair_items_list).filter(
                    (item) => item.order_id == order.order_id
                ),
            });
        });

        return orderList;
    });

    return (
        <IonPage>
        <IonContent>
        <div className="admin">
            <div className="container">
                <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
                <main>
                    <div className="main__container">
                        <div className="dashboard__left">
                            <div className={`dashboard__left__card order__card`}>
                                <div className="dashboard__left__title">
                                    <div>
                                        <h1>過往訂單</h1>
                                    </div>
                                </div>
                                {!orderList && <div>未有訂單</div>}
                                {orderList && (
                                    <>


                                        <OrderList key={`completed-orderList`}
                                            filterBy="completed"
                                            orderList={orderList}
                                        />
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                </main>
                <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar} />
            </div>
        </div>
        </IonContent>
        </IonPage>
    )
}

export default AdminOrderPage