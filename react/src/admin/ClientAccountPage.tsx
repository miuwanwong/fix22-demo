import React, { useEffect, useState } from 'react'
import Navbar from './Navbar'
import Sidebar from './Sidebar';
import './Admin.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getAllAccountsListThunk, getAllOrderListThunk } from '../redux/data/thunk';
import { RootState } from '../redux';
import { Link } from 'react-router-dom';
import { IonContent, IonPage } from '@ionic/react';



function UserAccountPage() {
    const dispatch = useDispatch();
    const [sidebarOpen, setSidebarOpen] = useState(false);

    const openSidebar = () => {
        setSidebarOpen(true);
    };

    const closeSidebar = () => {
        setSidebarOpen(false);
    };

    useEffect(() => {
        if(!clientList) return
        dispatch(getAllAccountsListThunk('client'));
        dispatch(getAllOrderListThunk());
    }, [dispatch])

    const clientList = useSelector((state: RootState) => state.data.client_list)


  

    return (
        <IonPage>
        <IonContent>
        <div className="admin">
            <div className="container">

                <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
                <main>
                    <div className="main__container">
                        <div className={`dashboard__left__card order__card`}>
                            <div className="dashboard__left__title">
                                <div>
                                    <h1>客戶資料</h1>
                                </div>
                            </div>
                            {!clientList && <div>未有客戶資料</div>}
                            {clientList &&
                                <>
                                    <div className="dashboard__client__title">
                                        <span>#</span>
                                        <span>客戶</span><span>負責人</span><span>職位</span>
                                        <span>地區</span>
                                        <span>電話</span>
                                    </div>

                                    {Object.values(clientList).map(client => (
                                        <>

                                            <Link to={`/admin/client-accounts/${client.user_id}`} key={client.user_id} style={{ textDecoration: 'none', color:'#7d8491ff'}}>
                                                <div className="dashboard__client__record">
                                                    <span>{client.user_id}</span>
                                                    <span>{client.company_name}</span>
                                                    <span>{client.person_in_charge}{client.person_in_charge_prefix}</span>
                                                    <span>{client.person_in_charge_job_title}</span>
                                                    <span>{client.district_name}</span>
                                                    <span>{client.company_tel}</span>
                                                </div>
                                            </Link>
                                        </>
                                    ))}
                                </>

                            }

                        </div>
                    </div>
                </main>
                <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar} />
            </div>
        </div>
        </IonContent>
        </IonPage>

    )

}

export default UserAccountPage