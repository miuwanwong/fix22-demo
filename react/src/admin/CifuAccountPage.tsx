import { IonContent, IonPage } from '@ionic/react';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../redux';
import { getAllAccountsListThunk } from '../redux/data/thunk';

import Navbar from './Navbar';
import Sidebar from './Sidebar';

function CifuAccountPage() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const dispatch = useDispatch();

    const openSidebar = () => {
        setSidebarOpen(true);
    };

    const closeSidebar = () => {
        setSidebarOpen(false);
    };
    const cifuList = useSelector((state: RootState) => state.data.cifu_list);

    useEffect(() => {
        dispatch(getAllAccountsListThunk('cifu'));
    }, [dispatch]);

    return (
        <IonPage>
            <IonContent>
                <div className="admin">
                    <div className="container">
                        <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
                        <main>
                            <div className="main__container">
                                <div className={`dashboard__left__card order__card`}>
                                    <div className="dashboard__left__title">
                                        <div>
                                            <h1>師傅資料</h1>
                                        </div>
                                    </div>
                                    {!cifuList && <div>未有師傅資料</div>}
                                    {cifuList && (
                                        <>
                                            <div className="dashboard__cifu__title">
                                                <span>#</span>
                                                <span>師傅名稱</span>
                                                <span>電話</span>
                                                <span>電郵</span>
                                            </div>

                                            {Object.values(cifuList).map((cifu) => (
                                                <div className="dashboard__cifu__record">
                                                    <span>{cifu.user_id}</span>
                                                    <span>{cifu.name}</span>
                                                    <span>{cifu.tel}</span>
                                                    <span>{cifu.email}</span>
                                                </div>
                                            ))}
                                        </>
                                    )}
                                </div>
                            </div>
                        </main>
                        <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar} />
                    </div>
                </div>
            </IonContent>
        </IonPage>
    );
}

export default CifuAccountPage;
