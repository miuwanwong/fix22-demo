import {
    IonBackButton,
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonInput,
    IonItem,
    IonLabel,
    IonPage,
    IonSelect,
    IonSelectOption,
    IonTitle,
    IonToolbar,
} from '@ionic/react';
import { useForm } from 'react-hook-form';
import { JWTPayload } from 'shared';
import dotenv from 'dotenv';
import { useState } from 'react';
import { clientInfoFields } from '../data/formFields';
import { useSelector } from 'react-redux';
import { RootState } from '../redux';

dotenv.config();

function AdminSettingsPage() {
    const { register, handleSubmit, reset } = useForm();
    const [updated, setUpdated] = useState<boolean>(false);
    const [error, setError] = useState<string>('');

    const user_id = 60;
    const token = localStorage.getItem('token');

    const onSubmit = async (info: JWTPayload) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/account/${user_id}`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({ info, user_id }),
            });

            if (res.status !== 200) {
                return setError('cannot update');
            } else {
                reset();
                return setUpdated(true);
            }
        } catch (e) {
            console.error(e.message);
        }
    };

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/" />
                    </IonButtons>
                    <IonTitle>更改帳戶資料</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <form onSubmit={handleSubmit(onSubmit)}>
                    {clientInfoFields.map((field, index) => {
                        const { label, props } = field;
                        return (
                            <IonItem key={index} lines="full">
                                <>
                                    {props.selectOptions && (
                                        <>
                                            <IonLabel position="fixed" slot="start">
                                                {label}
                                            </IonLabel>
                                            <IonSelect {...register(props.name)} {...props}>
                                                {props.selectOptions.map((option, index) => {
                                                    return (
                                                        <IonSelectOption
                                                            key={`form_field_${index}`}
                                                            slot="end"
                                                            value={option.value}
                                                        >
                                                            {option.placeholder}
                                                        </IonSelectOption>
                                                    );
                                                })}
                                            </IonSelect>
                                        </>
                                    )}
                                    {props.type && (
                                        <>
                                            <IonLabel position="stacked">{label}</IonLabel>
                                            <IonInput {...props} {...register(props.name)} />
                                        </>
                                    )}
                                </>
                            </IonItem>
                        );
                    })}
                    <IonButton type="submit" className="ion-margin-top" expand="full">
                        確定
                    </IonButton>
                </form>
            </IonContent>
        </IonPage>
    );
}

export default AdminSettingsPage;
