import './Admin.scss';
import OrderItem, { OrderItemState } from './OrderItem';
import { statusReference } from '../data/status';

type Props = {
    filterBy: string;
    orderList: OrderItemState[];
};

const OrderList = ({ filterBy, orderList }: Props) => {
    let filteredList = orderList;

    switch (filterBy) {
        case 'needAdminAttention':
            filteredList = orderList.filter(
                (order) =>
                    (order.order.order_status_id === statusReference.orderStatusPendingAssignment ||
                        order.order.payment_status_id ===
                            statusReference.paymentStatusPendingQuotation ||
                        order.order.payment_status_id ===
                            statusReference.paymentStatusPaymentImageSubmitted) &&
                    order.order.order_status_id !== statusReference.orderStatusCancelled
            );
            break;
        case 'noNeedAdminAttention':
            filteredList = orderList.filter(
                (order) =>
                    order.order.order_status_id !== statusReference.orderStatusPendingAssignment &&
                    order.order.payment_status_id !==
                        statusReference.paymentStatusPendingQuotation &&
                    order.order.payment_status_id !==
                        statusReference.paymentStatusPaymentImageSubmitted &&
                    (order.order.order_status_id !== statusReference.orderStatusCompleted ||
                        order.order.payment_status_id !==
                            statusReference.paymentStatusPaymentConfirmed) &&
                    order.order.order_status_id !== statusReference.orderStatusCancelled
            );

            break;
        case 'completed':
            filteredList = orderList.filter(
                (order) =>
                    (order.order.order_status_id === statusReference.orderStatusCompleted &&
                        order.order.payment_status_id ===
                            statusReference.paymentStatusPaymentConfirmed) ||
                    order.order.order_status_id === statusReference.orderStatusCancelled
            );
            break;
        default:
            break;
    }

    return (
        <>
            {filteredList.length == 0 && (
                <div>
                    <br />
                    未有訂單
                </div>
            )}
            {filteredList.length > 0 && (
                <div className="dashboard__order__title">
                    <span>#</span>
                    <span>客戶</span>
                    <span>地區</span>
                    <span>維修服務</span>
                    <span>訂單狀態</span>
                    <span>付款狀態</span>
                    <span></span>
                </div>
            )}

            {filteredList.map((order) => (
                <OrderItem
                    adminHomepage={true}
                    item={order}
                    key={'order-item-' + order.order.order_id}
                />
            ))}
        </>
    );
};

export default OrderList;
