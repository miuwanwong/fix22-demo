import { AssignmentState, OrderState } from '../redux/data/state';
import './Admin.scss';
import {
    IonCard,
    IonCardHeader,
    IonCardContent,
    IonCardTitle,
    IonBadge,
    useIonToast,
} from '@ionic/react';
import { useState } from 'react';
import OrderItem, { OrderItemState } from './OrderItem';
import AssignmentItem, { AssignmentDisplayState } from './AssignmentItem';
import { formatDate } from '../components/format';

//Object.keys(state.data.assignment_list)

export type AssignmentIdByDateState = {
    repair_date: string;
    repair_time: string;
    assignment_id: number[];
};

type Props = {
    assignmentIdByDate: AssignmentIdByDateState[];
};

const AssignmentListByDate = ({ assignmentIdByDate }: Props) => {
    return (
        <>
            {assignmentIdByDate.map((group) => (
                <div key={group.repair_date + '-' + group.repair_time}>
                    <div className="dashboard__assignment__title">
                        <span>
                            {formatDate(group.repair_date)}{' '}
                            {group.repair_time == 'am' ? '上午' : '下午'}
                        </span>
                    </div>
                    {group.assignment_id.map((id) => (
                        <AssignmentItem assignment_id={id} key={'assignment-' + id} />
                    ))}
                </div>
            ))}

        </>
    );
};

export default AssignmentListByDate;
