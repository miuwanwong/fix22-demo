import {
    AssignmentState,
    CompletionImageState,
    OrderImageState,
    OrderState,
    PaymentImageState,
    SelectedRepairItemList,
} from '../redux/data/state';
import './Admin.scss';
import { useState } from 'react';
import { formatDate, formatDateTime } from '../components/format';
import { statusReference } from '../data/status';
import ChangePaymentStatusForm from './ChangePaymentStatusForm';
import EditAssignmentForm from './EditAssignmentForm';
import AddAssignmentForm from './AddAssignmentForm';
import { useDispatch } from 'react-redux';
import { deleteAssignmentThunk } from '../redux/data/thunk';
import Swal from 'sweetalert2';
import ChangeOrderStatusForm from './ChangeOrderStatusForm';
import { useIonToast } from '@ionic/react';

export type AssignmentItemState = {
    assignment: AssignmentState;
    cifu_name: string;
    completion_images_list: CompletionImageState[];
};

export type OrderItemState = {
    order: OrderState;
    assignment_list: AssignmentItemState[];
    order_images_list: OrderImageState[];
    payment_images_list: PaymentImageState[];
    repair_item_list: SelectedRepairItemList[];
};

type Props = {
    item: OrderItemState;
    adminHomepage: boolean;
};

const OrderItem = ({ item, adminHomepage }: Props) => {
    const { order, assignment_list, repair_item_list, payment_images_list, order_images_list } =
        item;

    const [orderDetailsDisplay, setOrderDetailsDisplay] = useState<boolean>(false);
    const [editAssignmentFormDisplay, setEditAssignmentFormDisplay] = useState<{
        assignmentId: number;
        isOpen: boolean;
    }>({ assignmentId: -1, isOpen: false });

    const dispatch = useDispatch();
    const [presentToast, dismissToast] = useIonToast();

    const deleteAssignment = (assignment_id: number) => {
        const assignmentDetails = assignment_list.find(
            (item) => assignment_id == item.assignment.assignment_id
        )!;

        Swal.fire({
            title: `確定刪除下列安排?`,
            text: `維修安排：${assignmentDetails.cifu_name}  ${new Date(assignmentDetails.assignment.repair_date).getMonth() + 1
                }月${new Date(assignmentDetails.assignment.repair_date).getDate()}日${assignmentDetails.assignment.repair_time == 'am' ? '上午' : '下午'
                }`,
            icon: 'warning',
            heightAuto: false,
            showCancelButton: true,
            confirmButtonColor: '#6baa41',
            cancelButtonColor: '#7d8491ff',
            confirmButtonText: '確定刪除',
            cancelButtonText: '取消',
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(
                    deleteAssignmentThunk(assignment_id, assignmentDetails.assignment.order_id)
                );
                // Swal.fire({
                //     title: '維修安排已刪除!',
                //     text: '',
                //     icon: 'success',
                //     heightAuto: false,
                //     confirmButtonColor: '#6baa41',
                //     confirmButtonText: '確定',
                //     background: '#fffbf4',
                // });
                presentToast({
                    message: `維修安排已刪除`,
                    duration: 3000,
                    color: 'success',
                });
            }
        });
    };

    const enlargeImage = (image_file_path: string) => {
        Swal.fire({
            imageUrl: image_file_path,
            customClass: {
                image: 'max-height: 700px;',
            },
            padding: '2em',
            confirmButtonText: '返回',
            heightAuto: false,
            confirmButtonColor: '#7d8491ff',
        });
    };

    function hideAssignmentForm() {
        setEditAssignmentFormDisplay({ assignmentId: -1, isOpen: false });
    }

    return (
        <>
            <div className="dashboard__order__record">
                <span>{order.order_id}</span>
                <span>{order.company_name}</span>
                <span>{order.district_name}</span>
                <span>{order.repair_category_name}</span>

                <span>
                    {(() => {
                        switch (order.order_status_id) {
                            case statusReference.orderStatusPendingAssignment:
                                return (
                                    <button className="status_button red_button">待安排師傅</button>
                                );
                            case statusReference.orderStatusAssigned:
                                return (
                                    <button className="status_button yellow_button">
                                        已安排師傅
                                    </button>
                                );
                            case statusReference.orderStatusCancelled:
                                return (
                                    <button className="status_button gray_button">已取消</button>
                                );
                            case statusReference.orderStatusCompleted:
                                return (
                                    <button className="status_button green_button">已完工</button>
                                );
                            default:
                                return (
                                    <button className="status_button gray_button">待處理</button>
                                );
                        }
                    })()}
                </span>
                <span>
                    {(() => {
                        switch (order.payment_status_id) {
                            case statusReference.paymentStatusPendingQuotation:
                                return <button className="status_button red_button">待報價</button>;
                            case statusReference.paymentStatusPendingPayment:
                                return (
                                    <button className="status_button yellow_button">
                                        待客戶付款
                                    </button>
                                );
                            case statusReference.paymentStatusPaymentImageSubmitted:
                                return (
                                    <button className="status_button red_button">待確認收款</button>
                                );
                            case statusReference.paymentStatusPaymentConfirmed:
                                return (
                                    <button className="status_button green_button">已收款</button>
                                );
                            default:
                                return (
                                    <button className="status_button gray_button">待處理</button>
                                );
                        }
                    })()}
                </span>
                <span
                    key={`displayOrderDetail-${order.order_id}`}
                    onClick={() => setOrderDetailsDisplay(!orderDetailsDisplay)}
                    className="dropdown-list-button"
                >
                    {orderDetailsDisplay ? (
                        <i className="fa fa-chevron-circle-up"></i>
                    ) : (
                        <i className="fa fa-chevron-circle-down"></i>
                    )}
                </span>
            </div>

            {orderDetailsDisplay && (
                <div key={`orderDetails-${order.order_id}`} className="dashboard__order__details">
                    <div className="dashboard__admin__order">
                        <div>
                            <h5>客戶資料</h5>
                            <p>
                                負責人: {order.person_in_charge_job_title} {order.person_in_charge}{' '}
                                {order.person_in_charge_prefix}
                                <br />
                                聯絡電話: {order.contact_tel}
                                <br />
                                聯絡電話: {order.contact_email}
                                <br />
                                <div className="image-inline-container">
                                    客戶付款証明:{' '}
                                    {payment_images_list &&
                                        payment_images_list.map((image) => {
                                            return (
                                                <>
                                                    <div
                                                        className="square-image"
                                                        key={
                                                            'payment-image-' +
                                                            image.order_id +
                                                            image.image_file_name
                                                        }
                                                        onClick={() =>
                                                            enlargeImage(
                                                                `${process.env.REACT_APP_MULTER_ORIGIN}/${image.image_file_name}`
                                                            )
                                                        }
                                                        style={{
                                                            backgroundImage: `url(${process.env.REACT_APP_MULTER_ORIGIN}/${image.image_file_name})`,
                                                            width: '50px',
                                                            height: '50px',
                                                            backgroundSize: 'cover',
                                                            backgroundPosition: 'center',
                                                            marginLeft: '0.5rem',
                                                        }}
                                                    ></div>
                                                </>
                                            );
                                        })}
                                </div>
                                <br />
                            </p>

                            <h5>維修詳情</h5>
                            <p>
                                維修地址: {order.district_name} {order.repair_address1}{' '}
                                {order.repair_address2 ? ' ' + order.repair_address2 : ''}
                                <br />
                                維修服務: {order.repair_category_name}
                                {repair_item_list.length > 0
                                    ? ' - ' +
                                    repair_item_list
                                        .map((name) =>
                                            name.repair_item_name.startsWith('其他問題')
                                                ? '其他問題'
                                                : name.repair_item_name
                                        )
                                        .join(', ')
                                    : ''}
                                <br />
                                損壞詳情: {order.problem_desc ? order.problem_desc : '未有提供'}
                                <br />
                                產品牌子:{' '}
                                {order.appliance_brand ? order.appliance_brand : '未有提供'}
                                <br />
                                產品型號:{' '}
                                {order.appliance_model ? order.appliance_model : '未有提供'}
                                <br />
                                產品安裝位置:{' '}
                                {order.appliance_install_location_desc
                                    ? order.appliance_install_location_desc
                                    : '未有提供'}
                                <br />
                                <div className="image-inline-container">
                                    相片:
                                    <div className="multi-image-container">
                                        {order_images_list &&
                                            order_images_list.map((image) => {
                                                return (
                                                    <div
                                                        className="square-image"
                                                        key={
                                                            'order-images-' +
                                                            image.order_id +
                                                            image.image_file_name
                                                        }
                                                        onClick={() =>
                                                            enlargeImage(
                                                                `${process.env.REACT_APP_MULTER_ORIGIN}/${image.image_file_name}`
                                                            )
                                                        }
                                                        style={{
                                                            backgroundImage: `url(${process.env.REACT_APP_MULTER_ORIGIN}/${image.image_file_name})`,
                                                            width: '50px',
                                                            height: '50px',
                                                            backgroundSize: 'cover',
                                                            backgroundPosition: 'center',
                                                            marginLeft: '0.5rem',
                                                        }}
                                                    ></div>
                                                );
                                            })}
                                    </div>
                                </div>
                            </p>
                        </div>
                        <div>
                            {adminHomepage && (
                                <>
                                    <div>
                                        <ChangePaymentStatusForm
                                            key={`changePaymentStatusForm-${order.order_id}`}
                                            order_id={order.order_id}
                                            payment_status_id={order.payment_status_id}
                                        />
                                    </div>
                                    <div>
                                        <AddAssignmentForm
                                            key={`addAssignmentForm-${order.order_id}`}
                                            order_id={order.order_id}
                                        />
                                    </div>
                                    <div>
                                        <ChangeOrderStatusForm
                                            key={`changeOrderStatusForm-${order.order_id}`}
                                            order_id={order.order_id}
                                            order_status_id={order.order_status_id}
                                        />
                                    </div>
                                </>
                            )}
                        </div>
                    </div>
                    <h5>師傅安排</h5>
                    {assignment_list.length == 0 && <li>未有任何安排</li>}
                    {assignment_list.length > 0 &&
                        assignment_list.map((assignment) => (
                            <div
                                className="dashboard__admin__assignment"
                                key={'assignment-in-orders-' + assignment.assignment.assignment_id}
                            >
                                <div className="assignment_first_row">
                                    <span>任務＃{assignment.assignment.assignment_id} </span>
                                    <span> {formatDate(assignment.assignment.repair_date)}{' - '}
                                        {assignment.assignment.repair_time == 'am'
                                            ? '上午'
                                            : '下午'}
                                    </span>
                                    <span>{assignment.cifu_name}</span>

                                    <i
                                        className="fa fa-pencil-square-o"
                                        onClick={() => {
                                            setEditAssignmentFormDisplay(
                                                editAssignmentFormDisplay.assignmentId ==
                                                    +assignment.assignment.assignment_id!
                                                    ? {
                                                        assignmentId:
                                                            +assignment.assignment.assignment_id!,
                                                        isOpen: !editAssignmentFormDisplay.isOpen,
                                                        //!editAssignmentFormDisplay.isOpen
                                                    }
                                                    : {
                                                        assignmentId:
                                                            +assignment.assignment.assignment_id!,
                                                        isOpen: true,
                                                    }
                                            );
                                        }}
                                    ></i>

                                    <i
                                        className="fa fa-trash"
                                        onClick={() =>
                                            deleteAssignment(assignment.assignment.assignment_id!)
                                        }
                                    ></i>
                                </div>
                                <div>
                                    <br />
                                    工資: ${assignment.assignment.total_amount_to_cifu}<br />
                                    備註: {assignment.assignment.remarks}
                                    <br />
                                    師傅確認完工:
                                    {assignment.assignment.mark_complete_by_cifu &&
                                        formatDateTime(assignment.assignment.mark_complete_by_cifu)}
                                    <br />
                                    <div className="image-inline-container">
                                        師傅上傳相片:
                                        <div className="multi-image-container">
                                            {assignment &&
                                                assignment.completion_images_list.map((image) => {
                                                    return (
                                                        <div
                                                            className="square-image"
                                                            key={
                                                                'completion-images-' +
                                                                image.assignment_id +
                                                                image.image_file_name
                                                            }
                                                            onClick={() =>
                                                                enlargeImage(
                                                                    `${process.env.REACT_APP_MULTER_ORIGIN}/${image.image_file_name}`
                                                                )
                                                            }
                                                            style={{
                                                                backgroundImage: `url(${process.env.REACT_APP_MULTER_ORIGIN}/${image.image_file_name})`,
                                                                width: '50px',
                                                                height: '50px',
                                                                backgroundSize: 'cover',
                                                                backgroundPosition: 'center',
                                                                marginLeft: '0.5rem',
                                                            }}
                                                        ></div>
                                                    );
                                                })}{' '}
                                        </div>
                                    </div>
                                </div>
                                {editAssignmentFormDisplay.assignmentId ==
                                    assignment.assignment.assignment_id &&
                                    editAssignmentFormDisplay.isOpen == true ? (
                                    <div>
                                        <EditAssignmentForm
                                            key={`form-${assignment.assignment.assignment_id}`}
                                            hideAssignmentForm={hideAssignmentForm}
                                            assignment={assignment.assignment}
                                        />
                                    </div>
                                ) : (
                                    ''
                                )}
                            </div>
                        ))}

                </div>
            )}
        </>
    );
};

export default OrderItem;
