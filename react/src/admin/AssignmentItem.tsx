import { AssignmentState, OrderState } from '../redux/data/state';
import './Admin.scss';
import {
    IonCard,
    IonCardHeader,
    IonCardContent,
    IonCardTitle,
    IonBadge,
    useIonToast,
} from '@ionic/react';
import { useState } from 'react';
import { formatDate, formatDateTime } from '../components/format';
import { statusReference } from '../data/status';
import { useSelector } from 'react-redux';
import { RootState } from '../redux/state';
import { Link } from 'react-router-dom';

export type AssignmentDisplayState = {
    assignment: AssignmentState;
    cifu_name: string;
    district_name: string;
    repair_category_name: string;
};

type Props = {
    assignment_id: number;
};

const AssignmentItem = ({ assignment_id }: Props) => {
    const assignmentDetails: AssignmentDisplayState = useSelector(
        (state: RootState): AssignmentDisplayState => {
            const assignment = Object.values(state.data.assignment_list).find(
                (assignment) => assignment.assignment_id == assignment_id
            )!;
            const cifu_name = Object.values(state.data.cifu_list).find(
                (cifu) => cifu.user_id === assignment.cifu_id
            )?.name!;
            const district_name = Object.values(state.data.order_list).find(
                (order) => order.order_id === assignment.order_id
            )?.district_name!;
            const repair_category_name = Object.values(state.data.order_list).find(
                (order) => order.order_id === assignment.order_id
            )?.repair_category_name!;

            let assignmentDetails = { assignment, cifu_name, district_name, repair_category_name };
            // console.log(Object.values(state.data.assignment_list));
            return assignmentDetails;
        }
    );

    return (
        <>
            <div className="dashboard__assignment__record">
                <span>{assignmentDetails!.cifu_name}</span>
                訂單#{assignmentDetails!.assignment!.order_id}: {assignmentDetails!.district_name} -{' '}
                {assignmentDetails!.repair_category_name}
                維修
            </div>
        </>
    );
};

export default AssignmentItem;
