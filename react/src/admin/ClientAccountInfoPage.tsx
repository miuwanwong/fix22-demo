import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Navbar from './Navbar';
import Sidebar from './Sidebar';
import './Admin.scss';
import {
    AssignmentState,
    ClientState,
    CompletionImageState,
    OrderImageState,
    OrderState,
    SelectedRepairItemList,
} from '../redux/data/state';
import { RootState } from '../redux';
import { useParams } from 'react-router';
import OrderItem from './OrderItem';
import { getAllAccountsListThunk, getAllOrderListThunk } from '../redux/data/thunk';
import { IonContent, IonPage } from '@ionic/react';

type AssignmentListState = {
    assignment: AssignmentState;
    cifu_name: string;
    completion_images_list: CompletionImageState[];
};

type OrderItemState = {
    order: OrderState;
    assignment_list: AssignmentListState[];
    repair_item_list: SelectedRepairItemList[];
    order_images_list: OrderImageState[];
    payment_images_list: OrderImageState[];
};

export type ClientItemState = {
    client: ClientState;
    orderList: OrderItemState[];
};

function ClientAccountInfoPage() {
    const clientId = Object.values(useParams()).join();
    const dispatch = useDispatch();
    const [sidebarOpen, setSidebarOpen] = useState(false);

 
  
    useEffect(()=>{
    
        if (oneClientList.length !== 0) return;
        dispatch(getAllAccountsListThunk('client'));
        dispatch(getAllOrderListThunk());
        

    },[dispatch])


    const openSidebar = () => {
        setSidebarOpen(true);
    };

    const closeSidebar = () => {
        setSidebarOpen(false);
    };

  

    const oneClientList: ClientItemState[] = useSelector((state: RootState): ClientItemState[] => {
        const oneClientList: ClientItemState[] = [];

        Object.values(state.data.client_list).forEach((client) => {
            let orderList: OrderItemState[] = [];

            if (client.user_id == parseInt(clientId)) {
                Object.values(state.data.order_list).forEach((order) => {
                    if (order.user_id == client.user_id) {
                        console.log('order', order);
                        console.log('xyz');
                        const assignment_list: AssignmentListState[] = [];

                        Object.values(state.data.assignment_list).forEach((assignment) => {
                            if (assignment.order_id == order.order_id) {
                                assignment_list.push({
                                    assignment,
                                    cifu_name: Object.values(state.data.cifu_list).find(
                                        (cifu) => cifu.user_id == assignment.cifu_id
                                    )?.name!,
                                    completion_images_list: Object.values(
                                        state.data.completion_images_list
                                    ).filter(
                                        (image) => image.assignment_id == assignment.assignment_id
                                    ),
                                });
                            }
                        });

                        orderList.push({
                            order,
                            assignment_list,
                            repair_item_list: Object.values(
                                state.data.selected_repair_items_list
                            ).filter((item) => item.order_id == order.order_id),
                            order_images_list: Object.values(state.data.order_images_list).filter(
                                (image) => image.order_id == order.order_id
                            ),
                            payment_images_list: Object.values(
                                state.data.payment_images_list
                            ).filter((image) => image.order_id == order.order_id),
                        });
                    }
                });

                oneClientList.push({
                    client,
                    orderList,
                });
            }
        });

        return oneClientList;
    });

 
   

    if(oneClientList?.[0]?.orderList.length>1){
        oneClientList?.[0]?.orderList.sort((a, b) => {
            return b.order.order_id - a.order.order_id;
        });
    }
    

    
  
    return (
        <IonPage>
            <IonContent>
        <div className="admin">
            <div className="container">
                <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
                <main>
                    <div className="main__container">
                        <div className="dashboard__left">
                            <div className="dashboard__left__card">
                                <div className="dashboard__left__title">
                                    <div>
                                        <h1>客戶資料</h1>
                                    </div>
                                    <i className="fa fa-file-text-o"></i>
                                </div>
                                {!oneClientList && <div>未有客戶資料</div>}
                                {oneClientList && (
                                    <div>
                                        <h6>客戶編號: #{oneClientList?.[0]?.client.user_id}</h6>
                                        <h6>客戶名稱: {oneClientList?.[0]?.client.company_name}</h6>
                                        <h6>
                                            客戶地址: {oneClientList?.[0]?.client.district_name}{' '}
                                            {oneClientList?.[0]?.client.company_address1}
                                            {oneClientList?.[0]?.client.company_address2
                                                ? `, ${oneClientList[0].client.company_address2}`
                                                : ''}
                                        </h6>
                                        <h6>
                                            負責人: {oneClientList?.[0]?.client.person_in_charge}{' '}
                                            {oneClientList?.[0]?.client.person_in_charge_prefix}
                                        </h6>
                                        <h6>電話: {oneClientList?.[0]?.client.company_tel}</h6>
                                        <h6>電郵: {oneClientList?.[0]?.client.email}</h6>
                                    </div>
                                )}
                            </div>
                        </div>

                        <div className="dashboard__left__card">
                            <div className="dashboard__left__title">
                                <div>
                                    <h1>訂單資料</h1>
                                </div>
                                <i className="fa fa-file-text-o"></i>
                            </div>
                            {oneClientList?.[0]?.orderList.length == 0 && <div>未有訂單資料</div>}

                            {oneClientList?.[0]?.orderList.length > 0 && (
                                <>
                                    <div className="dashboard__order__title">
                                        <span>#</span>
                                        <span>客戶</span>
                                        <span>地區</span>
                                        <span>維修服務</span>
                                        <span>訂單狀態</span>
                                        <span>付款狀態</span>
                                        <span></span>
                                    </div>
                                    {oneClientList?.[0]?.orderList.map(item => (

                                        <OrderItem adminHomepage={false} item={item}/>
                                    ))}
                                </>
                            )}
                        </div>
                    </div>
                </main>
                <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar} />
            </div>
        </div>
        </IonContent>
        </IonPage>
    );
}

export default ClientAccountInfoPage;
