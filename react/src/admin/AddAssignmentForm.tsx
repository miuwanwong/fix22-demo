import React, { useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { useDispatch } from 'react-redux';
import { createAssignmentThunk } from '../redux/data/thunk';
import Swal from 'sweetalert2';
import { useIonToast } from '@ionic/react';

type CifuType = {
    user_id: number;
    name: string;
};

type AssignmentType = {
    order_id: number;
    cifu_id: number;
    repair_date: Date | string;
    repair_time: string;
    remarks: string;
    total_amount_to_cifu: number;
};

type Props = {
    order_id: number;
};

function AddAssignmentForm({ order_id }: Props) {
    const dispatch = useDispatch()
    const [presentToast, dismissToast] = useIonToast();
    
    function onAddAssignmentClick() {

        const name = cifuList?.find((cifu) => cifu.user_id == cifu_id)?.name;

        Swal.fire({
            title: `確定新增下列安排?`,
            text: `訂單編號#${order_id} : ${name}  ${repair_date.getMonth() + 1}月${repair_date.getDate()}日${repair_time == 'am' ? '上午' : '下午'
                }`,
            icon: 'warning',
            heightAuto: false,
            showCancelButton: true,
            confirmButtonColor: '#6baa41',
            cancelButtonColor: '#7d8491ff',
            confirmButtonText: '確定',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(createAssignmentThunk(
                    order_id,
                    cifu_id,
                    repair_date.toISOString(),
                    repair_time,
                    remarks,
                    total_amount_to_cifu,
                ));

                presentToast({
                    message: `已新增維修安排!`,
                    duration: 3000,
                    color: 'success',
                });
            }
        })
    };

    //for assigning Cifu
    const [cifuList, setCifuList] = useState<CifuType[] | null>(null);
    const [cifu_id, setCifuID] = useState<number>(0);
    const [repair_date, setRepairDate] = useState<Date>(new Date());
    const [repair_time, setRepairTime] = useState<string>('am');
    const [remarks, setRemarks] = useState<string>('');
    const [total_amount_to_cifu, setTotalAmountToCifu] = useState<number>(0);

    const [addAssignmentFormDisplay, setAddAssignmentFormDisplay] = useState<boolean>(false);

    useEffect(() => {
        const url = `${process.env.REACT_APP_API_SERVER}/assignment/get-cifu-list`;
        const token = localStorage.getItem('token');
        fetch(url,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            })
            .then((res) => res.json())
            .then((data) => {
                setCifuList(data.cifuList);
                if (data.cifuList.length > 0) {
                    setCifuID(parseInt(data.cifuList[0].user_id));
                }
            });
    }, []);

    return (
        <>
            <button className="admin_button"
                onClick={() => {
                    setAddAssignmentFormDisplay(!addAssignmentFormDisplay);
                }}
            >
                安排師傅
                {addAssignmentFormDisplay? (
                        <i className="fa fa-caret-up"></i>
                    ) : (
                        <i className="fa fa-caret-down"></i>
                    )}
            </button>

            {addAssignmentFormDisplay && (
                <form>
                    {cifuList && (
                        <div>
                            <label>選擇師傅</label>
                            <select
                                onChange={(e) => {
                                    setCifuID(parseInt(e.target.value));
                                }}
                            >
                                {cifuList.map((cifu) => (
                                    <option key={'cifu' + cifu.user_id} value={cifu.user_id}>
                                        {cifu.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                    )}
                    <div>
                        <label>選擇日期</label>
                        {(() => {
                            return (
                                <DatePicker
                                    selected={repair_date}
                                    onChange={(date: Date) => setRepairDate(date)}
                                />
                            );
                        })()}
                    </div>
                    <div>
                        <label>選擇時間</label>
                        <select
                            defaultValue={repair_time}
                            onChange={(e) => setRepairTime(e.target.value)}
                        >
                            <option key="am" value="am">
                                上午
                            </option>
                            <option key="pm" value="pm">
                                下午
                            </option>
                        </select>
                    </div>
                    <div>
                        <label>師傅工資</label>
                        $
                        <input
                            defaultValue={total_amount_to_cifu}
                            type="number"
                            name="totalAmountToCifu"
                            onChange={(e) => {
                                if (!isNaN(parseInt(e.target.value)))
                                    setTotalAmountToCifu(parseInt(e.target.value));
                            }}
                        />
                    </div>
                    <div>
                        <label>備註</label>
                        <input
                            defaultValue={remarks}
                            type="text"
                            name="remarks"
                            onChange={(e) => setRemarks(e.target.value)}
                        />
                    </div>
                    <div>
                        <button className="small_button" type="button" onClick={onAddAssignmentClick}>
                            確定安排
                        </button>
                    </div>
                </form>
            )}
        </>
    );
};

export default AddAssignmentForm
