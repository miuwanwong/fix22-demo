import React, { useEffect, useState } from 'react';
import { useIonToast, IonPage, IonContent } from '@ionic/react';
import { useDispatch, useSelector } from 'react-redux';

//admin components
import OrderList from './OrderList';
import Navbar from './Navbar';
import Sidebar from './Sidebar';

//from redux
import { RootState } from '../redux/state';
import { getAllAssignmentListThunk, getAllOrderListThunk } from '../redux/data/thunk';

//other components
import '../hooks/socketio';
import { socket } from '../hooks/socketio';

// css
import './Admin.scss';
import { AssignmentItemState, OrderItemState } from './OrderItem';
import { AssignmentIdByDateState } from './AssignmentListByDate';
import AssignmentListByDate from './AssignmentListByDate';
import { setInitialStateAction } from '../redux/data/action';
import { timezoneDate } from '../components/format';

function AdminHomePage() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        if (orderList.length !== 0) return;
        dispatch(getAllOrderListThunk());
    }, [dispatch]);

    const openSidebar = () => {
        setSidebarOpen(true);
    };

    const closeSidebar = () => {
        setSidebarOpen(false);
    };

    const orderList: OrderItemState[] = useSelector((state: RootState): OrderItemState[] => {
        const orderList: OrderItemState[] = [];

        Object.values(state.data.order_list).forEach((order) => {

            const assignment_list: AssignmentItemState[] = [];
            Object.values(state.data.assignment_list).forEach((assignment) => {
                if (assignment.order_id === order.order_id) {
                    assignment_list.push({
                        assignment,
                        cifu_name: Object.values(state.data.cifu_list).find(
                            (cifu) => cifu.user_id === assignment.cifu_id
                        )?.name!,
                        completion_images_list: Object.values(
                            state.data.completion_images_list
                        ).filter((image) => image.assignment_id === assignment.assignment_id),
                    });
                }
            });

            orderList.push({
                order,
                assignment_list,
                order_images_list: Object.values(state.data.order_images_list).filter(
                    (image) => image.order_id === order.order_id
                ),
                payment_images_list: Object.values(state.data.payment_images_list).filter(
                    (image) => image.order_id === order.order_id
                ),
                repair_item_list: Object.values(state.data.selected_repair_items_list).filter(
                    (item) => item.order_id === order.order_id
                ),
            });
        });

        return orderList;
    });

    const assignmentIdByDate: AssignmentIdByDateState[] = useSelector(
        (state: RootState): AssignmentIdByDateState[] => {
            const assignmentIdByDate: AssignmentIdByDateState[] = [];

            Object.values(state.data.assignment_list).forEach((assignment) => {
                let repair_date = timezoneDate(assignment.repair_date);
                let group = assignmentIdByDate.find(
                    (group) =>
                        group.repair_date === repair_date &&
                        group.repair_time === assignment.repair_time
                );
                if (!group) {
                    group = {
                        repair_date,
                        repair_time: assignment.repair_time,
                        assignment_id: [],
                    };
                    assignmentIdByDate.push(group);
                }
                group.assignment_id.push(assignment.assignment_id!);
            });

            assignmentIdByDate.sort((a, b) => {
                let aTime = new Date(a.repair_date).getTime();
                if (a.repair_time === 'pm') {
                    aTime++;
                }
                let bTime = new Date(b.repair_date).getTime();
                if (b.repair_time === 'pm') {
                    bTime++;
                }
                return aTime - bTime;
            });

            return assignmentIdByDate;
        }
    );

    const [presentToast, dismissToast] = useIonToast();

    useEffect(() => {
        const onNewOrder = (data: { order_id: number }) => {
            dispatch(setInitialStateAction());
            dispatch(getAllOrderListThunk());
            presentToast({
                message: `收到新訂單#${data.order_id}，請發報價單`,
                color: 'danger',
                buttons: [
                    {
                        text: '收到',
                        role: 'cancel',
                    },
                ],
            });
        };

        const onOrderCompleted = (data: { id: number }) => {
            dispatch(setInitialStateAction());
            dispatch(getAllOrderListThunk());
            presentToast({
                message: `訂單#${data.id}已完工`,
                color: 'danger',
                // duration: 7 * DAY,
                buttons: [
                    {
                        text: '收到',
                        role: 'cancel',
                        // handler: () => {
                        //     dismissToast()
                        // },
                    },
                ],
            });
            //console.log('toast for order completed' + data.id);
        };

        const onPaymentProofReceived = (data: { id: number }) => {
            dispatch(setInitialStateAction());
            dispatch(getAllOrderListThunk());

            presentToast({
                message: `訂單#${data.id}已提交付款證明，請確認已收款`,
                color: 'danger',
                // duration: 7 * DAY,
                buttons: [
                    {
                        text: '收到',
                        role: 'cancel',
                        // handler: () => {
                        //     dismissToast()
                        // },
                    },
                ],
            });
            //console.log('toast for payment proof received' + data.id);
        };

        const onCifuCompletedAssignment = (data: { order_id: number }) => {
            dispatch(setInitialStateAction());
            dispatch(getAllOrderListThunk());
            //dispatch(getAllAssignmentListThunk());

            presentToast({
                message: `師傅已完成訂單#${data.order_id}`,
                color: 'danger',
                // duration: 7 * DAY,
                buttons: [
                    {
                        text: '收到',
                        role: 'cancel',
                        // handler: () => {
                        //     dismissToast()
                        // },
                    },
                ],
            });
            //console.log('toast for cifu completed assignment' + data.order_id);
        };

        socket.on('orderCompleted', onOrderCompleted);
        socket.on('newOrder', onNewOrder);
        socket.on('cifuCompletedAssignment', onCifuCompletedAssignment);
        socket.on('paymentProofReceived', onPaymentProofReceived);

        socket.on('test', (data) => console.log(data + 'testing'));

        return () => {
            socket.off('newOrder', onNewOrder);
            socket.off('paymentProofReceived', onPaymentProofReceived);
            socket.off('orderCompleted', onOrderCompleted);
            socket.off('cifuCompletedAssignment', onCifuCompletedAssignment);
        };
    }, [presentToast, dispatch]);

    return (
        <IonPage>
            <IonContent>
                <div className="admin">
                    <div className="container">
                        <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
                        <main>
                            <div className="main__container">
                                <div className="dashboard">
                                    <div className="dashboard__left">
                                        <div className="dashboard__left__card">
                                            <div className="dashboard__left__title">
                                                <div>
                                                    <h1>請即處理</h1>
                                                </div>
                                            </div>
                                            {!orderList && <div>未有訂單</div>}
                                            {orderList && (
                                                <OrderList
                                                    filterBy="needAdminAttention"
                                                    orderList={orderList}
                                                />
                                            )}
                                        </div>

                                        <div className="dashboard__left__card">
                                            <div className="dashboard__left__title">
                                                <div>
                                                    <h1>已安排訂單</h1>
                                                </div>
                                            </div>

                                            {!orderList && <div>未有訂單</div>}
                                            {orderList && (
                                                <OrderList
                                                    filterBy="noNeedAdminAttention"
                                                    orderList={orderList}
                                                />
                                            )}
                                        </div>
                                    </div>

                                    <div className="dashboard__right">
                                        <div className="dashboard__right__card">
                                            <div className="dashboard__right__title">
                                                <div>
                                                    <h1>師傅更表</h1>
                                                </div>
                                            </div>

                                            {!assignmentIdByDate && <div>未有安排</div>}
                                            {assignmentIdByDate && (
                                                <AssignmentListByDate
                                                    assignmentIdByDate={assignmentIdByDate}
                                                />
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>
                        <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar} />
                    </div>
                </div>
            </IonContent>
        </IonPage>
    );
}

export default AdminHomePage;
