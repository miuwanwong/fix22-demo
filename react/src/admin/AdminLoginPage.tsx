import {
    IonAlert,
    IonButton,
    IonContent,
    IonInput,
    IonItem,
    IonLabel,
    IonLoading,
    IonPage,
} from '@ionic/react';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { loginThunk, RootState } from '../redux';

function AdminLoginPage() {
    const history = useHistory();
    const isLoading = useSelector((state: RootState) => state.auth.isLoading);
    const hasError = useSelector((state: RootState) => state.auth.errorMessage);
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const { handleSubmit, register } = useForm();
    const dispatch = useDispatch();

    // LOGIN THUNK
    const onSubmit = () => {
        console.log('admin login button clicked');
        try {
            dispatch(loginThunk({ email, password }, history));
        } catch (error) {
            console.error(error);
            return;
        }
    };

    return (
        <IonPage>
            <IonContent fullscreen>
                <h2>管理員登入</h2>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <IonItem>
                        <IonLabel position="floating">登入電郵</IonLabel>
                        <IonInput
                            value={email}
                            required={true}
                            type="email"
                            onIonChange={(e) =>
                                setEmail((e.currentTarget as HTMLInputElement).value)
                            }
                            {...register('email')}
                            disabled={isLoading}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel position="floating">登入密碼</IonLabel>
                        <IonInput
                            value={password}
                            required={true}
                            type="password"
                            onIonChange={(e) =>
                                setPassword((e.currentTarget as HTMLInputElement).value)
                            }
                            {...register('password')}
                            disabled={isLoading}
                        />
                    </IonItem>
                    <IonButton
                        type="submit"
                        className="ion-margin-top"
                        expand="full"
                        disabled={isLoading}
                    >
                        登入
                    </IonButton>
                </form>
                <IonAlert
                    isOpen={!!hasError}
                    header={'登入資料不正確'}
                    message={'請輸入正確的登入名稱及密碼'}
                    buttons={['OK']}
                    backdropDismiss={false}
                />
                <IonLoading isOpen={isLoading} message={'處理中'} />
            </IonContent>
        </IonPage>
    );
}

export default AdminLoginPage;
