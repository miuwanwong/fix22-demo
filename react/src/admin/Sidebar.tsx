import './Sidebar.scss';
import { Link, NavLink, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { logoutThunk } from '../redux';
import { SquareLogoReverse } from '../components/Logo';

type Props = {
    sidebarOpen: boolean;
    closeSidebar: () => void;
};

const Sidebar = ({ sidebarOpen, closeSidebar }: Props) => {
    const dispatch = useDispatch();
    const history = useHistory();

    const logout = () => {
        dispatch(logoutThunk());
        history.push('/admin/login');
    };

    return (
        <div className={sidebarOpen ? 'sidebar_responsive' : ''} id="sidebar">
            <div className="sidebar__title">
                <SquareLogoReverse />
                <i className="fa fa-times" id="sidebarIcon" onClick={() => closeSidebar()}></i>
            </div>

            <div className="sidebar__menu">
                <h2>維修訂單</h2>
                <NavLink
                    to="/admin/home"
                    className="sidebar__link"
                    activeClassName="active_menu_link"
                >
                    <i className="fa fa-pencil-square-o"></i>
                    管理平台
                </NavLink>
                <NavLink
                    to="/admin/orders"
                    className="sidebar__link"
                    activeClassName="active_menu_link"
                >
                    <i className="fa fa-files-o"></i>
                    過往訂單
                </NavLink>


                <h2>戶口管理</h2>
                <NavLink
                    to="/admin/client-accounts"
                    className="sidebar__link"
                    activeClassName="active_menu_link"
                >
                    <i className="fa fa-users"></i>
                    客戶
                </NavLink>
                <NavLink
                    to="/admin/cifu-accounts"
                    className="sidebar__link"
                    activeClassName="active_menu_link"
                >
                    <i className="fa fa-wrench"></i>
                    師傅
                </NavLink>

                <Link className="sidebar__logout" to="#" onClick={logout}>
                    <i className="fa fa-sign-out"></i>
                    登出
                </Link>
            </div>
        </div>
    );
};

export default Sidebar;
