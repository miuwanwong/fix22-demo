import './Navbar.scss';
import { LongLogo } from '../components/Logo';

type Props = {
    sidebarOpen: boolean;
    openSidebar: () => void;
};

const Navbar = ({ sidebarOpen, openSidebar }: Props) => {
    return (
        <nav className="navbar">
            <div className="nav_icon" onClick={() => openSidebar()}>
                <i className="fa fa-bars"></i>
            </div>

            <LongLogo />
        </nav>
    );
};

export default Navbar;
