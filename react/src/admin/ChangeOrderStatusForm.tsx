import React, { useState } from 'react'
import './Admin.scss';
import { statusReference } from '../data/status';
import { useDispatch } from 'react-redux';
import { updateOrderStatusThunk } from '../redux/data/thunk';
import { useIonToast } from '@ionic/react';
import Swal from 'sweetalert2';


type Props = {
    order_id: number;
    order_status_id: number;
};

function ChangeOrderStatusForm({
    order_id,
    order_status_id
}: Props) {

    const dispatch = useDispatch()
    const [orderStatusId, selectedOrderStatus] = useState<number>(order_status_id)
    const [formDisplay, setFormDisplay] = useState<boolean>(false)

    const [presentToast, dismissToast] = useIonToast();

    function changeOrderStatus() {

        Swal.fire({
            title: `確定更改訂單狀態?`,
            text: '',
            icon: 'warning',
            heightAuto: false,
            showCancelButton: true,
            confirmButtonColor: '#6baa41',
            cancelButtonColor: '#7d8491ff',
            confirmButtonText: '確定更改',
            cancelButtonText: '取消更改',
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(updateOrderStatusThunk(order_id, orderStatusId));
                presentToast({
                    message: `訂單#${order_id}狀態已更新`,
                    duration: 3000,
                    color: 'success',
                });
            }
        });
    }

    return (
        <>
            <button className="admin_button"
                onClick={() => {
                    setFormDisplay(!formDisplay);
                }}>
                更新訂單狀態
                {formDisplay ? (
                    <i className="fa fa-caret-up"></i>
                ) : (
                    <i className="fa fa-caret-down"></i>
                )}
            </button>
            {formDisplay &&
                <div className="dashboard__form">
                    <select
                        onChange={(e) => {
                            selectedOrderStatus(parseInt(e.target.value));
                        }}
                        defaultValue={order_status_id}
                    >
                        <option value={statusReference.orderStatusPendingAssignment}>待安排師傅</option>
                        <option value={statusReference.orderStatusAssigned}>已安排師傅</option>
                        <option value={statusReference.orderStatusCancelled}>訂單已取消</option>
                        <option value={statusReference.orderStatusCompleted}>訂單已完成</option>
                    </select>

                    <button className="small_button"
                        type="button"
                        onClick={() => changeOrderStatus()}
                    >
                        更改
                    </button>
                </div>
            }

        </>
    );
};

export default ChangeOrderStatusForm