import { useSelector } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router';
import { RootState } from '../../redux';

export function ClientRoute(props: RouteProps) {
    const user_type_id = useSelector((state: RootState) => state.auth.user?.user_type_id);

    if (!process.env.REACT_APP_USERS_CLIENT) {
      throw new Error('missing env')
    }

    const isClient = user_type_id === parseInt(process.env.REACT_APP_USERS_CLIENT);

    // console.log('Why does this re-render???');

    if (isClient) {
        return <Route {...props} />;
    }

    // Not yet login OR logged in as other roles
    let fallbackComponent = () => {
        return (
            <Redirect
                to={{
                    pathname: '/client/login',
                    state: { from: props.location },
                }}
            />
        );
    };

    return <Route {...props} component={fallbackComponent} />;
}
