import React from 'react'
import { IonIcon } from '@ionic/react';
import { arrowForwardOutline } from 'ionicons/icons';


type Props={
    first?:string | null,
    second?:string | null,
    third?:string | null,
}

function CircleHeader({first, second, third} :Props ) {
    return (
        <div >
            <div style={first?{background:first}:{}} >
                預約<br />維修
            </div>
            <IonIcon icon={arrowForwardOutline} />
            <div style={second?{background:second}:{}} >
                預計<br />收費
            </div>
            <IonIcon icon={arrowForwardOutline} />
            <div style={third?{background:third}:{}} >
                確認<br />預約
            </div>
        </div>
    )
}

export default CircleHeader