export function formatDateIncludeWeekday(time: number | string | Date) {
  let date = new Date(time);
  return new Intl.DateTimeFormat('zh-hk', { dateStyle: 'full' }).format(date);
}

export function formatDate(time: number | string | Date) {
  let date = new Date(time);
  return new Intl.DateTimeFormat('zh-hk', { dateStyle: 'long' }).format(date);
}

export function formatDateTime(time: number | string | Date) {
  let date = new Date(time);
  return new Intl.DateTimeFormat('zh-hk', {
    dateStyle: 'long',
    timeStyle: 'short',
  }).format(date);
}

export function timezoneDate(date: Date | number | string) {
  const d = new Date(date);
  const year = d.getFullYear();
  const month = d.getMonth() + 1;
  const day = d.getDate();
  return `${year}-${month}-${day}`;
}
