import { useSelector } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router';
import { RootState } from '../../redux';

function CifuRoute(props: RouteProps) {
    const user_type_id = useSelector((state: RootState) => state.auth.user?.user_type_id);

    if (!process.env.REACT_APP_USERS_CIFU) {
      throw new Error('missing env')
    }

    const isCifu = user_type_id === parseInt(process.env.REACT_APP_USERS_CIFU);

    if (isCifu) {
        return <Route {...props} />;
    }

    // Not yet login OR logged in as other roles
    let fallbackComponent = () => {
        return (
            <Redirect
                to={{
                    pathname: '/cifu/login',
                    state: { from: props.location },
                }}
            />
        );
    };

    return <Route {...props} component={fallbackComponent} />;
}

export default CifuRoute;
