import logo from '../logos/logo.svg';
import logo_white from '../logos/logo-white.svg';
 
export function SquareLogo() {
    return (
        <div className="square__logo__div">
            <img src={logo} alt="logo" />
            <div className="logo__text">
                <span>F</span>
                <span>I</span>
                <span>X</span>
                <span>2</span>
                <span>2</span>
            </div>
        </div>
    );
}

export function SquareLogoReverse() {
    return (
        <div className="square__logo__div">
            <img src={logo_white} alt="logo" />
            <div className="logo__text">
                <span>F</span>
                <span>I</span>
                <span>X</span>
                <span>2</span>
                <span>2</span>
            </div>
        </div>
    );
}

export function LongLogo() {
    return (
        <div className="long__logo__div">
            <img src={logo} alt="logo" />
            <div className="logo__text">
                <span>F</span>
                <span>I</span>
                <span>X</span>
                <span>2</span>
                <span>2</span>
            </div>
        </div>
    );
}
