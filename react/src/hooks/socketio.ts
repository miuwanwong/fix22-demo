import { io } from 'socket.io-client';

const { REACT_APP_API_SERVER } = process.env;
let url = REACT_APP_API_SERVER!;
console.log(`ws url: ${url}`);

export let socket = io(url);

socket.on('connect', () => {
  console.log(`ws connected to ${REACT_APP_API_SERVER}`);
});
socket.on('error', (error) => {
  console.log('ws error:', error);
});
