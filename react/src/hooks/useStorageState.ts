import { useCallback, useEffect, useState } from 'react'

export function useStorageState<T>(key: string, defaultValue: T | (() => T)) {
  const loadState = useCallback(
    function loadState(): T {
      try {
        let text = localStorage.getItem(key)
        if (text === null) {
          return typeof defaultValue === 'function'
            ? (defaultValue as any)()
            : defaultValue
        }
        return JSON.parse(text)
      } catch (error) {
        console.error('failed to load state:', error)
        return typeof defaultValue === 'function'
          ? (defaultValue as any)()
          : defaultValue
      }
    },
    [key, defaultValue],
  )

  const [state, setState] = useState<T>(loadState)

  function saveState(newState: T | ((state: T) => T)) {
    if (typeof newState === 'function') {
      newState = (newState as any)(state)
    }
    try {
      localStorage.setItem(key, JSON.stringify(newState))
    } catch (error) {
      console.error('failed to save state:', { key, error })
    }
    try {
      let event = new StorageEvent('storage', {
        key,
        oldValue: JSON.stringify(state),
        newValue: JSON.stringify(newState),
        storageArea: localStorage,
      })
      window.dispatchEvent(event)
    } catch (error) {
      console.error('failed to dispatch storage event:', { key, error })
    }

    setState(newState)
  }

  useEffect(() => {
    function reload() {
      let newState = loadState()
      if (JSON.stringify(newState) !== JSON.stringify(state)) {
        setState(newState)
      }
    }
    window.addEventListener('storage', reload)
    return () => {
      window.removeEventListener('storage', reload)
    }
  }, [loadState, state])

  return [state, saveState] as const
}
