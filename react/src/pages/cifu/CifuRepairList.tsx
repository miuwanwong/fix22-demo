import React, { useEffect, useState } from 'react';
import {
    IonBadge,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonItem,
    IonSelect,
    IonSelectOption,
    useIonToast,
} from '@ionic/react';

import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { getAllAssignmentListThunk } from '../../redux/data/thunk';

import { AssignmentState, OrderState } from '../../redux/data/state';
import { TimezoneDate } from 'timezone-date.ts';
import { Link } from 'react-router-dom';
import './Cifu.scss';

import '../../hooks/socketio';
import { socket } from '../../hooks/socketio';
import { statusReference } from '../../data/status';
import { formatDate, timezoneDate } from '../../components/format';

type AssignmentItem = {
    order: OrderState;
    assignment: AssignmentState;
    order_images_list: string[];
};

function CifuRepairList() {
    const dispatch = useDispatch();
    const [presentToast, dismissToast] = useIonToast();

    useEffect(() => {
        if (assignment_list.length !== 0) return;
        dispatch(getAllAssignmentListThunk());
    }, [dispatch]);

    useEffect(() => {
        const onNewAssignment = (data: { assignment_id: number }) => {
            dispatch(getAllAssignmentListThunk());
            presentToast({
                message: `收到新任務#${data.assignment_id}!`,
                color: 'danger',
                buttons: [
                    {
                        text: '收到',
                        role: 'cancel',
                    },
                ],
            });
        };

        const onEditAssignment = (data: { assignment_id: number }) => {
            dispatch(getAllAssignmentListThunk());
            presentToast({
                message: `任務#${data.assignment_id}已更改`,
                color: 'danger',
                buttons: [
                    {
                        text: '收到',
                        role: 'cancel',
                    },
                ],
            });
        };

        const onDeleteAssignment = (data: { assignment_id: number }) => {
            dispatch(getAllAssignmentListThunk());
            presentToast({
                message: `任務#${data.assignment_id}已被取消`,
                color: 'danger',
                buttons: [
                    {
                        text: '收到',
                        role: 'cancel',
                    },
                ],
            });
        };

        socket.on('newAssignment', onNewAssignment);
        socket.on('editAssignment', onEditAssignment);
        socket.on('deleteAssignment', onDeleteAssignment);
        return () => {
            socket.off('newAssignment', onNewAssignment);
            socket.off('editAssignment', onEditAssignment);
            socket.off('deleteAssignment', onDeleteAssignment);
        };
    }, [presentToast, dispatch]);

    let todayDate = timezoneDate(new TimezoneDate());

    const [repairDate, setRepairDate] = useState<string | Date | number>('all');
    const [processStatus, setProcessStatus] = useState<string | null>('all');

    let assignment_list: AssignmentItem[] = useSelector((state: RootState): AssignmentItem[] => {
        const assignment_list: AssignmentItem[] = [];

        Object.values(state.data.assignment_list).forEach((assignment) => {
            const order = Object.values(state.data.order_list).find((order) => {
                return order.order_id == assignment.order_id;
            });
            if (!order) return;

            const order_images_list: string[] = [];

            Object.values(state.data.order_images_list).forEach((image) => {
                if (image.order_id !== order.order_id) return;

                order_images_list.push(image.image_file_name);
            });

            assignment_list.push({
                assignment,
                order,
                order_images_list,
            });
        });

        return assignment_list.sort((a, b) => {
            let aTime = new Date(a.assignment.repair_date).getTime();
            if (a.assignment.repair_time === 'pm') {
                aTime++;
            }
            let bTime = new Date(b.assignment.repair_date).getTime();
            if (b.assignment.repair_time === 'pm') {
                bTime++;
            }
            return aTime - bTime;
        });
    });

    const dropDownDateList = [...assignment_list];

    assignment_list = assignment_list.filter((item) => {
        if (repairDate == 'all') {
            return timezoneDate(item.assignment.repair_date) >= timezoneDate(Date.now());
        }
        return timezoneDate(item.assignment.repair_date) == timezoneDate(repairDate);
    });

    const repairDateList = dropDownDateList.map((item) => {
        const repairDate = timezoneDate(item.assignment.repair_date);

        return repairDate;
    });

    let uniqueRepairDateList: string[] = [];
    repairDateList.forEach((item) => {
        if (!uniqueRepairDateList.includes(timezoneDate(item)))
            uniqueRepairDateList.push(timezoneDate(item));
    });

    uniqueRepairDateList.sort((a, b) => {
        return new Date(a).getTime() - new Date(b).getTime();
    });

    assignment_list = assignment_list.filter((item) => {
        if (processStatus == 'all') {
            return item.assignment;
        }

        if (processStatus == 'processing') {
            return item.assignment.mark_complete_by_cifu == null;
        }
        return item.assignment.mark_complete_by_cifu != null;
    });

    return (
        <>
            <IonItem>
                <IonSelect
                    style={{ paddingRight: '15px' }}
                    value={repairDate}
                    onIonChange={(e) => setRepairDate(e.detail.value)}
                >
                    <IonSelectOption value={'all'}>所有更表</IonSelectOption>
                    {uniqueRepairDateList &&
                        uniqueRepairDateList.map((item) =>
                            new Date(item).getTime() >= new Date(todayDate).getTime() ? (
                                <IonSelectOption key={'uniqueDates-' + item} value={item}>
                                    {item}
                                </IonSelectOption>
                            ) : (
                                ''
                            )
                        )}
                </IonSelect>

                <IonSelect
                    value={processStatus}
                    onIonChange={(e) => setProcessStatus(e.detail.value)}
                >
                    <IonSelectOption value="all">所有維修狀態</IonSelectOption>

                    <IonSelectOption value="processing">有待處理</IonSelectOption>
                    <IonSelectOption value="paid">已完成</IonSelectOption>
                </IonSelect>
            </IonItem>
            {assignment_list.length == 0 && <div>未有資料</div>}
            {assignment_list &&
                assignment_list.map((item) => (
                    <Link
                        to={
                            item.assignment.mark_complete_by_cifu === null
                                ? `/cifu/repairinfo/${item.assignment.assignment_id}`
                                : '#/'
                        }
                        key={'link-to-info-' + item.assignment.assignment_id}
                    >
                        <IonCard
                            key={'assignment-' + item.assignment.assignment_id}
                            button={item.assignment.mark_complete_by_cifu === null ? true : false}
                        >
                            <IonCardHeader>
                                <IonCardTitle>
                                    <h6>
                                        <strong>
                                            <span>任務#{item.assignment.assignment_id}</span> -{' '}
                                            <span>{item.order.repair_category_name}</span>
                                        </strong>
                                    </h6>

                                    {item.order.order_status_id ==
                                    statusReference.orderStatusCancelled ? (
                                        <IonBadge color="success">已取消</IonBadge>
                                    ) : item.assignment.mark_complete_by_cifu === null ? (
                                        <IonBadge className="processing">有待處理</IonBadge>
                                    ) : (
                                        <IonBadge color="success">已完成</IonBadge>
                                    )}
                                </IonCardTitle>
                            </IonCardHeader>
                            <IonCardContent>
                                <p>
                                    維修日期： {formatDate(item.assignment.repair_date)}{' '}
                                    {item.assignment.repair_time == 'am' ? '上午' : '下午'}
                                </p>
                                <p>
                                    地址：{item.order.district_name}
                                    <br />
                                    {item.order.repair_address1}
                                    <br />
                                    {item.order.repair_address2 ? item.order.repair_address2 : ''}
                                </p>
                                <p>訂單編號： #{item.assignment.order_id}</p>
                                <p>公司名稱： {item.order.company_name}</p>
                                <p>管理員備註： {item.assignment.remarks}</p>
                                <p>工資: ${item.assignment.total_amount_to_cifu}</p>
                            </IonCardContent>
                        </IonCard>
                    </Link>
                ))}
        </>
    );
}

export default CifuRepairList;
