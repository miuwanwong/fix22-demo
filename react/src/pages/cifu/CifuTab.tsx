import React from 'react'
import { build, card } from 'ionicons/icons';
import { IonButton, IonIcon} from '@ionic/react';
import './Cifu.scss';

function CifuTab(){
    return(
        <div className="cifuTab">
            <IonButton style={{flex:1}} color="secondary" expand="block" routerLink="/cifu/repair"><IonIcon className="whiteFont" icon={build} /><span className="whiteFont">維修日程</span></IonButton>
        
            <IonButton style={{flex:1, marginLeft:'0.5em'}} color="secondary" expand="block" routerLink="/cifu/payment"><IonIcon className="whiteFont"icon={card} /><span className="whiteFont">付款狀態</span></IonButton>
        </div>
    )
}

export default CifuTab