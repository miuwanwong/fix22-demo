import React, { useEffect } from 'react'
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton } from '@ionic/react';
import CifuLogoutBtn from './CifuLogoutBtn';
import CifuPaymentList from './CifuPaymentList';
import CifuTab from './CifuTab';
import styles from './Cifu.module.scss';
import './Cifu.scss'


function CifuPaymentPage(){


  return (
    <IonPage className="CifuPage">
    <IonHeader>
      <IonToolbar>
        
        <div className={styles.cifuHeader}>
          <IonTitle>付款狀態</IonTitle> <CifuLogoutBtn/>
        </div>
      </IonToolbar>
    </IonHeader>
    <IonContent  class="cifu-content">

    <CifuPaymentList/>
  
    </IonContent>

    <CifuTab/>

  </IonPage>
  );
};

export default CifuPaymentPage;
