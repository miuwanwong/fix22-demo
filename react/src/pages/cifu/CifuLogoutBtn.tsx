import { IonButton } from '@ionic/react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { logoutThunk } from '../../redux';
import './Cifu.scss';

function CifuLogoutBtn() {
    const dispatch = useDispatch();
    const history = useHistory();

    const logout = () => {
        dispatch(logoutThunk());
        history.push('/cifu/login');
    };

    return (
        <>
            <IonButton color="secondary" onClick={logout}>
                <span className="whiteFont">登出</span>
            </IonButton>
        </>
    );
}

export default CifuLogoutBtn;
