import { useEffect, useState } from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonButton,
    IonToolbar,
    IonListHeader,
    IonModal,
    IonAlert,
    IonText,
    IonSpinner,
} from '@ionic/react';
import CifuLogoutBtn from './CifuLogoutBtn';
import styles from './Cifu.module.scss';
import { useHistory, useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { cifuChangeCompletionDateThunk, getAllAssignmentListThunk } from '../../redux/data/thunk';
import { RootState } from '../../redux';
import { selectImage, filesToBase64Strings } from '@beenotung/tslib/file';
import { compressImageToBase64, toImage } from '@beenotung/tslib/image';
import { AssignmentState, OrderState } from '../../redux/data/state';

type AssignmentItem = {
    order: OrderState;
    assignment: AssignmentState;
};

export type ModalState = {
    photo: string;
    isOpen: boolean;
};

function CifuConfirmedPage() {
    const history = useHistory();
    const assignmentId = Object.values(useParams()).join();

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getAllAssignmentListThunk());
    }, [dispatch]);

    const assignment_list: AssignmentItem[] = useSelector((state: RootState): AssignmentItem[] => {
        console.log('data', state.data);
        const assignment_list: AssignmentItem[] = [];

        Object.values(state.data.assignment_list).forEach((assignment) => {
            const order = Object.values(state.data.order_list).find((order) => {
                return order.order_id == assignment.order_id;
            });
            if (!order) return;

            assignment_list.push({
                assignment,
                order,
            });
        });

        return assignment_list;
    });
    const assignmentInfo = assignment_list?.find((item) => {
        return item.assignment.assignment_id === parseInt(assignmentId);
    });

    const [photoList, setPhotoList] = useState<string[]>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [modalPhoto, setModalPhoto] = useState<ModalState>({ photo: '', isOpen: false });
    const [showAlert, setShowAlert] = useState<boolean>(false);

    async function select() {
        setPhotoList([]);

        let files = await selectImage({ multiple: true });
        let photoList = await filesToBase64Strings(files);

        photoList = photoList.slice(0, 3);

        if (photoList.length > 0) {
            setLoading(true);
        }

        for (let i = 0; i < photoList.length; i++) {
            let image = await toImage(photoList[i]);
            photoList[i] = compressImageToBase64({ image, maximumLength: 300 * 1000 });
        }

        setPhotoList(photoList);
        setLoading(false);
    }

    async function submit() {
        if (photoList.length === 0) {
            setShowAlert(true);
            return;
        }

        dispatch(
            cifuChangeCompletionDateThunk(
                history,
                assignmentInfo!.order.order_id,
                assignmentInfo!.assignment.assignment_id!,
                photoList
            )
        );

    }

    const backToRepairInfo = () => {
        history.push(`/cifu/repairinfo/${assignmentId}`);
    };

    return (
        <IonPage className="CifuPage">
            <IonHeader>
                <IonToolbar>
                    <div className={styles.cifuHeader}>
                        <IonTitle>確認完工</IonTitle> <CifuLogoutBtn />
                    </div>
                </IonToolbar>
            </IonHeader>

            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                cssClass="my-custom-class"
                header={''}
                subHeader={''}
                message={'請提交圖片'}
                buttons={['確定']}
            />

            <IonModal animated isOpen={modalPhoto.isOpen}>
                <IonContent color="tertiary">
                    <div className="client-header-logo">• 圖片 •</div>
                    <div className="modal-div">
                        <img style={{ width: '80%' }} src={modalPhoto.photo}></img>
                    </div>
                </IonContent>
                <IonButton onClick={() => setModalPhoto({ photo: '', isOpen: false })}>
                    關閉
                </IonButton>
            </IonModal>
            {assignmentInfo && (
                <IonContent class="cifu-content">
                    <IonListHeader className="ion-no-padding">
                        預約編號: #{assignmentInfo.order.order_id} -{' '}
                        {assignmentInfo.order.repair_category_name}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">
                        服務範疇: {assignmentInfo.order.repair_category_name}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">
                        預約日期:{' '}
                        {new Intl.DateTimeFormat('zh', { dateStyle: 'long', timeStyle: 'short' })
                            .format(new Date(assignmentInfo.assignment.repair_date))
                            .substring(0, 10)}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">
                        維修時間: {assignmentInfo.assignment.repair_time == 'am' ? '上午' : '下午'}
                    </IonListHeader>

                    <div style={{ display: 'flex', marginTop: '20px' }}>
                        <IonButton color="secondary" onClick={select}>
                            <IonText color="light">選擇圖片</IonText>
                        </IonButton>

                        {photoList.length == 0 && loading == false && (
                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                <div className="emptyPhotoDiv"></div>
                                <div className="emptyPhotoDiv"></div>
                                <div className="emptyPhotoDiv"></div>
                            </div>
                        )}
                        {loading && (
                            <div>
                                <IonSpinner name="dots" />
                                <IonSpinner name="dots" /> <IonSpinner name="bubbles" />
                            </div>
                        )}

                        {photoList.length > 0 &&
                            photoList.slice(0, 3).map((photo) => (
                                <button
                                    key={'photo-' + photo}
                                    className="openModalBtn"
                                    onClick={() => setModalPhoto({ photo, isOpen: true })}
                                >
                                    <div
                                        style={{
                                            backgroundImage: `url(${photo})`,
                                            width: '50px',
                                            height: '50px',
                                            backgroundSize: 'cover',
                                            backgroundPosition: 'center',
                                            paddingLeft: '20px',
                                        }}
                                    ></div>
                                </button>
                            ))}
                    </div>
                </IonContent>
            )}

            <div className="cifuTab">
                <IonButton
                    style={{ flex: 1 }}
                    color="secondary"
                    expand="block"
                    onClick={backToRepairInfo}
                >
                    <span className="whiteFont">上一頁</span>
                </IonButton>

                <IonButton
                    style={{ flex: 1, marginLeft: '0.5em' }}
                    color="secondary"
                    expand="block"
                    onClick={submit}
                >
                    <span className="whiteFont">確認完工</span>
                </IonButton>
            </div>
        </IonPage>
    );
}

export default CifuConfirmedPage;
