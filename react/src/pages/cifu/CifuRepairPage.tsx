import { useEffect } from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
} from '@ionic/react';
import CifuLogoutBtn from './CifuLogoutBtn';
import CifuRepairList from './CifuRepairList';
import styles from './Cifu.module.scss';
import CifuTab from './CifuTab';
import { useDispatch } from 'react-redux';
import './Cifu.scss';
import { getAllAssignmentListThunk } from '../../redux/data/thunk';
import { setInitialStateAction } from '../../redux/data/action';

function CifuRepairPage() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getAllAssignmentListThunk());
    }, [dispatch]);

    const refresh = () => {
        dispatch(setInitialStateAction());
        dispatch(getAllAssignmentListThunk());

        console.log('hihihi cifu');
    };

    return (
        <IonPage className="CifuPage">
            <IonHeader>
                <IonToolbar>
                    <div className={styles.cifuHeader}>
                        <IonTitle>維修日程</IonTitle> <CifuLogoutBtn />
                    </div>
                </IonToolbar>
            </IonHeader>
            <IonContent class="cifu-content">
                <CifuRepairList />
            </IonContent>

            <CifuTab />
        </IonPage>
    );
}

export default CifuRepairPage;
