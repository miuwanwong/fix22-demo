import React, { useEffect, useState } from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonButton,
    IonToolbar,
    IonTabBar,
    IonTabButton,
    IonIcon,
    IonLabel,
    IonTabs,
    IonListHeader,
    IonButtons,
    IonBackButton,
    IonModal,
} from '@ionic/react';
import CifuLogoutBtn from './CifuLogoutBtn';
import styles from './Cifu.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { useParams } from 'react-router';
import { AssignmentState, OrderState } from '../../redux/data/state';
import { getAllAssignmentListThunk } from '../../redux/data/thunk';
import './Cifu.scss';
import { ModalState } from './CifuConfirmedPage';
import { timezoneDate } from '../../components/format';

type AssignmentItem = {
    order: OrderState;
    assignment: AssignmentState;

    order_images_list: string[];

    selected_repair_items_list: {
        order_id: number;
        repair_item_id: number;
        repair_category_id: number;
        repair_item_name: string;
        repair_fee: number;
    }[];
};



function CifuRepairInfoPage() {
    const [modalPhoto, setModalPhoto] = useState<ModalState>({ photo: '', isOpen: false })
    const assignmentId = Object.values(useParams()).join();

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getAllAssignmentListThunk());

    }, [dispatch]);

    const assignment_list: AssignmentItem[] = useSelector((state: RootState): AssignmentItem[] => {
        const assignment_list: AssignmentItem[] = [];

        Object.values(state.data.assignment_list).forEach((assignment) => {
            const order = Object.values(state.data.order_list).find((order) => {
                return order.order_id == assignment.order_id;
            });
            if (!order) return;

            const selected_repair_items_list = Object.values(
                state.data.selected_repair_items_list
            ).filter((item) => {
                return item.order_id == assignment.order_id;
            });

            if (!selected_repair_items_list) return;

            const order_images_list: string[] = [];

            Object.values(state.data.order_images_list).forEach((image) => {
                if (image.order_id !== order.order_id) return;

                order_images_list.push(image.image_file_name);
            });

            assignment_list.push({
                assignment,
                order,
                order_images_list,
                selected_repair_items_list,
            });
        });
        return assignment_list;
    });
    const assignmentInfo = assignment_list?.find((item) => {
        return item.assignment.assignment_id === parseInt(assignmentId);
    });
    return (
        <IonPage className="CifuPage">
            <IonHeader>
                <IonToolbar>
                    <div className={styles.cifuHeader}>
                        <IonTitle>維修詳情</IonTitle> <CifuLogoutBtn />
                    </div>
                </IonToolbar>
            </IonHeader>

            <IonModal animated isOpen={modalPhoto.isOpen}>
                <IonContent color="tertiary">
                    <div className="client-header-logo">• 圖片 •</div>
                    <div className="modal-div">
                        <img style={{ width: '80%' }} src={modalPhoto.photo}></img>
                    </div>
                </IonContent>
                <IonButton
                    onClick={() => setModalPhoto({ photo: '', isOpen: false })}
                >
                    關閉
                </IonButton>
            </IonModal>

            {assignmentInfo && (
                <IonContent class="cifu-content">
                    <IonListHeader className="ion-no-padding">
                        任務#{assignmentInfo?.assignment.assignment_id} -{' '}
                        {assignmentInfo?.order.repair_category_name}
                    </IonListHeader>

                    <IonListHeader className="ion-no-padding">
                        預約編號: #{assignmentInfo?.order.order_id}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">
                        餐廳名稱: {assignmentInfo?.order.company_name}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">
                        負責人: {assignmentInfo?.order.person_in_charge}{' '}
                        {assignmentInfo?.order.person_in_charge_prefix}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">
                        聯絡電話: {assignmentInfo?.order.contact_tel}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">
                        維修時間: {timezoneDate(assignmentInfo?.assignment?.repair_date!)}{' '}
                        {assignmentInfo?.assignment.repair_time == 'am' ? '上午' : '下午'}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">客戶地址:</IonListHeader>
                    <div>
                        {assignmentInfo?.order.district_name}{' '}
                        {assignmentInfo?.order.repair_address1}
                    </div>
                    {assignmentInfo?.order.repair_address2 ? (
                        <div>{assignmentInfo?.order.repair_address2}</div>
                    ) : (
                        ''
                    )}

                    <IonListHeader className="ion-no-padding">
                        維修詳情:
                        {assignmentInfo?.selected_repair_items_list.map((item, index) => (
                            <>
                                <span>&nbsp;{item.repair_item_name}</span>
                                {index >=
                                    assignmentInfo?.selected_repair_items_list?.length! - 1 ? (
                                    ''
                                ) : (
                                    <span> ,</span>
                                )}
                            </>
                        ))}
                    </IonListHeader>

                    <IonListHeader className="ion-no-padding">
                        文字描述:{' '}
                        {assignmentInfo?.order.problem_desc
                            ? assignmentInfo?.order.problem_desc
                            : '沒有'}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">
                        備註:{' '}
                        {assignmentInfo?.assignment.remarks
                            ? assignmentInfo?.assignment.remarks
                            : '沒有'}
                    </IonListHeader>
                    <IonListHeader className="ion-no-padding">機器圖片:</IonListHeader>

                    {assignmentInfo.order_images_list.length > 0 &&
                        assignmentInfo.order_images_list.map((p) => (
                            <button
                                key={'photo-' + p}
                                className="openModalBtn"
                                onClick={() => setModalPhoto({ photo: `${process.env.REACT_APP_MULTER_ORIGIN}/${p}`, isOpen: true })}
                            >
                                <div
                                    style={{
                                        backgroundImage: `url(${process.env.REACT_APP_MULTER_ORIGIN}/${p})`,
                                        width: '50px',
                                        height: '50px',
                                        backgroundSize: 'cover',
                                        backgroundPosition: 'center',
                                        paddingLeft: '20px',
                                    }}
                                ></div>
                            </button>
                        ))}
                </IonContent>
            )}

            <div className="cifuTab">
                <IonButton style={{ flex: 1 }} color="secondary" expand="block" routerLink="/cifu/repair"><span className="whiteFont">上一頁</span></IonButton>

                <IonButton style={{ flex: 1, marginLeft: '0.5em' }} color="secondary" expand="block" routerLink={`/cifu/confirmed/${assignmentInfo?.assignment.assignment_id}`}><span className="whiteFont">確認完工</span></IonButton>
            </div>


        </IonPage>
    );
}

export default CifuRepairInfoPage;
