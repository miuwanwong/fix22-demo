import React, { useEffect, useState } from 'react'
import { IonBadge, IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonItem, IonSelect, IonSelectOption } from '@ionic/react';
import styles from '../../pages/cifu/Cifu.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getAllAssignmentListThunk } from '../../redux/data/thunk';
import { RootState } from '../../redux';
import { AssignmentState, OrderState } from '../../redux/data/state';
import './Cifu.scss'
import { timezoneDate } from '../../components/format';

type AssignmentItem = {

    order: OrderState;
    assignment: AssignmentState;



}

function CifuPaymentList() {
    const dispatch = useDispatch()

    const [repairDate, setRepairDate] = useState<string>('all')
    const [paymentStatus, setPaymentStatus] = useState<string>('all')

    useEffect(() => {
        if(assignment_list.length !==0) return
        dispatch(getAllAssignmentListThunk())
    }, [dispatch])

    let assignment_list: AssignmentItem[] = useSelector(
        (state: RootState): AssignmentItem[] => {
            console.log("data", state.data)
            const assignment_list: AssignmentItem[] = []

            Object.values(state.data.assignment_list).forEach((assignment) => {

                const order = Object.values(state.data.order_list).find((order) => {
                    return order.order_id == assignment.order_id

                })
                if (!order) return

                assignment_list.push({
                    assignment,
                    order
                })

            })

            return assignment_list

        },
    )
    assignment_list?.sort((a, b) => {
        return new Date(b.assignment.repair_date).getTime() - new Date(a.assignment.repair_date).getTime()
    })

    const dropDownDateList = [...assignment_list]

    assignment_list = assignment_list.filter(item => {
        if (repairDate == 'all') {
            return timezoneDate(item.assignment.repair_date)
        }
        return timezoneDate(item.assignment.repair_date) == timezoneDate((repairDate))
    })
   

   assignment_list = assignment_list.filter(item => {
       if (paymentStatus == 'all') {
           return item.assignment
       }
       
       if(paymentStatus == 'processing'){
           return item.assignment.cifu_payment_status == false 
       }
       return item.assignment.cifu_payment_status == true
   })

    const repairDateList = dropDownDateList.map(item => {

        const repairDate = timezoneDate(item.assignment.repair_date)

        return repairDate
    })

    let uniqueRepairDateList: string[] = []
    repairDateList.forEach(item => {
        if (!uniqueRepairDateList.includes(timezoneDate(item)))
            uniqueRepairDateList.push(timezoneDate(item))
    })

    console.log(uniqueRepairDateList, "hi")

    return (
        <>

            <IonItem>

                <IonSelect style={{paddingRight:"15px"}} value={repairDate} onIonChange={e => setRepairDate(e.detail.value)}>
                    <IonSelectOption value={'all'}>所有維修記錄</IonSelectOption>
                    {uniqueRepairDateList && uniqueRepairDateList.map(item => (

                        <IonSelectOption value={item}>{item}</IonSelectOption>
                    ))}

                </IonSelect>
               
                <IonSelect value={paymentStatus} onIonChange={e => setPaymentStatus(e.detail.value)}>
                    <IonSelectOption value={'all'}>所有付款狀態</IonSelectOption>
                    

                        <IonSelectOption value="processing">未付款</IonSelectOption>
                        <IonSelectOption value="paid">已付款</IonSelectOption>

                </IonSelect>
            </IonItem>
            {assignment_list.length == 0 && <div>還沒有資料</div>}
            {assignment_list && assignment_list.map(item => (
                <IonCard >
                    <IonCardHeader >
                        <IonCardTitle>
                            <h6>
                                <strong><span>#{item.assignment.order_id}</span> - <span>{item.order.repair_category_name} - {timezoneDate(item.assignment.repair_date)}</span></strong>
                            </h6>
                            {item.assignment.cifu_payment_status ? <IonBadge color="success">已付款</IonBadge> : <IonBadge className={styles.processing}>正在處理</IonBadge>}
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <div className={styles.paymentList}>

                            {item.assignment.total_amount_to_cifu != null ? <span>${item.assignment.total_amount_to_cifu}</span> : "工資待定"}
                            <span>{item.order.district_name}</span> <span>{item.order.company_name}</span>
                        </div>
                    </IonCardContent>
                </IonCard>
            ))}
        </>
    )
}

export default CifuPaymentList

