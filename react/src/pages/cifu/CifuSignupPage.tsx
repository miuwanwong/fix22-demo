import {
    IonPage,
    IonContent,
    IonItem,
    IonLabel,
    IonButton,
    IonInput,
    IonBackButton,
    IonButtons,
    IonHeader,
    IonTitle,
    IonToolbar,
    IonAlert,
    IonLoading,
} from '@ionic/react';
import 'react-datepicker/dist/react-datepicker.css';
import { cifuSignUpFields } from '../../data/formFields';
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { User } from 'shared';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';
import { loginThunk } from '../../redux';

function CifuSignupPage() {
    const [error, setError] = useState<boolean>(false);
    const [success, setSuccess] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const history = useHistory();
    const dispatch = useDispatch();
    let username = '';
    let password = '';

    const { handleSubmit, register, reset } = useForm();

    const onSubmit = async (data: User) => {
        setIsLoading(true);

        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/cifu/signup`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });

        const result = await res.json();
        if (res.status !== 200) {
            console.log(JSON.stringify(result));
            setIsLoading(false);
            setError(true);
            return;
        }

        setIsLoading(false);
        reset();
        setSuccess(true);
        username = data.username!;
        password = data.password!;
        try {
            dispatch(loginThunk({ username, password }, history));
        } catch (error) {
            console.error(error.message);
            return;
        }
    };

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/cifu/login" text="返回" />
                    </IonButtons>
                    <IonTitle>註冊帳戶</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <div>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        {cifuSignUpFields.map((field, index) => {
                            const { label, props } = field;
                            return (
                                <IonItem key={index} lines="full">
                                    <>
                                        <IonLabel position="stacked">{label}</IonLabel>
                                        <IonInput {...register(props.name)} {...props} />
                                    </>
                                </IonItem>
                            );
                        })}
                        <IonButton type="submit" className="ion-margin-top" expand="full">
                            註冊
                        </IonButton>
                    </form>
                </div>
                <IonAlert
                    isOpen={error}
                    header={'登入名稱已被使用'}
                    message={'請選擇另一名稱'}
                    buttons={['OK']}
                    backdropDismiss={false}
                />
                <IonLoading isOpen={isLoading} message={'處理中'} />
                <IonAlert
                    isOpen={success}
                    header={'帳戶註冊成功'}
                    message={'正為您自動登入...'}
                    backdropDismiss={false}
                />
            </IonContent>
        </IonPage>
    );
}

export default CifuSignupPage;
