import { useState } from 'react';
//import styles from './Cifu.module.scss';
import {
    IonAlert,
    IonButton,
    IonContent,
    IonInput,
    IonItem,
    IonList,
    IonLoading,
    IonPage,
    IonText,
} from '@ionic/react';
// import logo from './download.jpg';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { loginThunk, RootState } from '../../redux';
import { Link, useHistory } from 'react-router-dom';
import './Cifu.scss'
import { SquareLogo } from '../../components/Logo';







function CifuLoginPage() {

    const history = useHistory();
    const isLoading = useSelector((state: RootState) => state.auth.isLoading);
    const hasError = useSelector((state: RootState) => state.auth.errorMessage);
    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const { handleSubmit, register } = useForm();
    const dispatch = useDispatch();

    // LOGIN THUNK
    const onSubmit = () => {
        console.log('cifu login button clicked');
        try {
            dispatch(loginThunk({ username, password }, history));
        } catch (error) {
            console.error(error.message);
            return;
        }
    };

    return (
        <IonPage className="CifuPage">
            <IonContent class="cifu-content">
                <div >

                    <SquareLogo />



                    <div className="cifu-div ion-padding">
                        <div className="cifu-title" >
                            <p color="primary" className="cifu-login-title">•  師傅登入  •</p>
                        </div>

                        <form onSubmit={handleSubmit(onSubmit)}>
                            <IonList>
                                <IonItem className="white-item ion-no-padding">
                                    <IonInput className="cifu-login-input"
                                        placeholder="登入名稱"
                                        value={username}
                                        required={true}
                                        type="text"
                                        onIonChange={(e) =>
                                            setUsername((e.currentTarget as HTMLInputElement).value)
                                        }
                                        {...register('username')}
                                        disabled={isLoading}
                                    />
                                </IonItem>
                                <IonItem className="white-item">

                                    <IonInput className="cifu-login-input"
                                        placeholder="登入密碼"
                                        value={password}
                                        required={true}
                                        type="password"
                                        onIonChange={(e) =>
                                            setPassword((e.currentTarget as HTMLInputElement).value)
                                        }
                                        {...register('password')}
                                        disabled={isLoading}
                                    />
                                </IonItem>
                            </IonList>
                            <div className="ion-text-center">
                            <IonButton
                                type="submit"
                             
                                className={`ion-margin-top  cifu-login-button-width`}
                                disabled={isLoading}
                            >
                                登入
                            </IonButton>
                            </div>
                        </form>
                        <div>
                            <h6 ><IonText color="light">或</IonText></h6>
                            <IonButton
                                routerLink="/cifu/signup"
                                disabled={isLoading}
                                className={`ion-margin-top  cifu-login-button-width`}
                            >
                                註冊帳戶
                            </IonButton>
                        </div>

                    </div>
                    <div className="cifu-admin-login-div">
                        <Link to={"/client/login"}><div className="cifu-admin-login">客戶登入</div></Link>
                    </div>

                </div>
                <IonAlert
                    isOpen={!!hasError}
                    header={'登入資料不正確'}
                    message={'請輸入正確的登入名稱及密碼'}
                    buttons={['OK']}
                    backdropDismiss={false}
                />
                <IonLoading isOpen={isLoading} message={'處理中'} />
            </IonContent>
        </IonPage>
    );
}

export default CifuLoginPage;
