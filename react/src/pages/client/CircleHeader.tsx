import React from 'react'
import { IonIcon } from '@ionic/react';
import { arrowForwardOutline } from 'ionicons/icons';


type Props = {
    first?: string | null,
    second?: string | null,
    third?: string | null,
}

function CircleHeader({ first, second, third }: Props) {
    return (
        <div style={{display:'flex', justifyContent:'center'}}>
            <div className="form-dot-div">
                <div style={first ? { background: first } : {}} className="form-dot">

                </div>

                <div style={second ? { background: second } : {}} className="form-dot">

                </div>

                <div style={third ? { background: third } : {}} className="form-dot">

                </div>
            </div>
        </div>
    )
}

export default CircleHeader