import {
    IonAlert,
    IonBackButton,
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonInput,
    IonItem,
    IonLabel,
    IonLoading,
    IonPage,
    IonSelect,
    IonSelectOption,
    IonTitle,
    IonToolbar,
} from '@ionic/react';
import { useForm } from 'react-hook-form';
import { JWTPayload } from 'shared';
import dotenv from 'dotenv';
import { useState } from 'react';
import { clientInfoFields } from '../../data/formFields';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { LongLogo } from '../../components/Logo';

dotenv.config();

function ClientSettingsPage() {
    const { register, handleSubmit, reset } = useForm();
    const [processing, setProcessing] = useState<boolean>(false);
    const [updated, setUpdated] = useState<boolean>(false);
    const [updateFailed, setUpdateFailed] = useState<boolean>(false)

    const user_id = useSelector((state: RootState) => state.auth.user?.user_id);
    const token = localStorage.getItem('token');

    const onSubmit = async (info: JWTPayload) => {
        setProcessing(true);
        try {
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/account/${user_id}`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({ info, user_id }),
            });

            if (res.status !== 200) {
                setProcessing(false);
                setUpdateFailed(true)
                console.error('cannot update');
                return;
            } else {
                reset();
                setProcessing(false);
                setUpdated(true);
                return

            }
        } catch (e) {
            console.error(e.message);
        }
    };

    return (
        <IonPage className="ClientPage">
            <IonHeader >
                <IonToolbar>

                    <IonButtons className="client-back-button" slot="start">
                        <IonBackButton defaultHref="/client/service" />
                    </IonButtons>
                    <IonTitle ><div className="client-bar-logo"><LongLogo /></div></IonTitle>
                </IonToolbar>
            </IonHeader>
            <div className="client-header-logo">•  更改帳戶資料  •</div>
            <IonContent class="client-content ion-text-center" >


                <form onSubmit={handleSubmit(onSubmit)}>
                    {clientInfoFields.map((field, index) => {
                        const { label, props } = field;
                        return (
                            <IonItem className="orangeItem" key={index} lines="full">
                                <>
                                    {props.selectOptions && (
                                        <>
                                            <IonLabel className="dropDown-label" color="primary" position="fixed" slot="start">
                                                {label}
                                            </IonLabel>
                                            <IonSelect {...register(props.name)} {...props}>
                                                {props.selectOptions.map((option, index) => {
                                                    return (
                                                        <IonSelectOption color="primary"
                                                            key={`form_field_${index}`}
                                                            slot="end"
                                                            value={option.value}
                                                        >
                                                            {option.placeholder}
                                                        </IonSelectOption>
                                                    );
                                                })}
                                            </IonSelect>
                                        </>
                                    )}
                                    {props.type && (
                                        <>

                                            <IonLabel color="primary" className="signup-label" position="stacked">{label}</IonLabel>
                                            <IonInput color="primary" {...props} {...register(props.name)} />

                                        </>
                                    )}
                                </>
                            </IonItem>
                        );
                    })}
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <IonButton type="submit" className="ion-margin-top orangeButton" expand="block">
                            確定
                        </IonButton>
                    </div>

                </form>
                <IonAlert
                    isOpen={updated}
                    header={'更新成功'}
                    buttons={['OK']}
                    backdropDismiss={false}
                />

                <IonAlert
                    isOpen={updateFailed}
                    onDidDismiss={() => setUpdateFailed(false)}
                    header={'請輸入最少一項資料'}
                    buttons={['OK']}
                    backdropDismiss={false}
                />

                <IonLoading isOpen={processing} message={'處理中'} />
            </IonContent>
        </IonPage>
    );
}

export default ClientSettingsPage;
