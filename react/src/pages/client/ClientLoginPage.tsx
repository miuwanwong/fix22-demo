import {
    IonButton,
    IonLabel,
    IonPage,
    IonContent,
    IonItem,
    IonInput,
    IonLoading,
    IonAlert,
    IonList,
    IonText,
    IonHeader,
    IonToolbar,
    IonTitle,
} from '@ionic/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loginThunk, RootState } from '../../redux';
import { useForm } from 'react-hook-form';
import { Link, useHistory, useLocation } from 'react-router-dom';
import './Client.scss';
import { LongLogo, SquareLogo } from '../../components/Logo';

function ClientLoginPage() {
    const history = useHistory();
    const [email, setEmail] = useState<string>('');
    const [hasRequestCode, setHasRequestCode] = useState(false);
    const [sendingEmail, setSendingEmail] = useState<boolean>(false);
    const [emailError, setEmailError] = useState<boolean>(false);
    const [passcodeError, setPasscodeError] = useState<boolean>(false);
    const isLoading = useSelector((state: RootState) => state.auth.isLoading);
    const { handleSubmit, register } = useForm();
    const dispatch = useDispatch();
    const [truePasscode, setTruePasscode] = useState<string>('');

    // AJAX REQUEST CODE LOGIC
    async function requestCode(email: string) {
        try {
            setSendingEmail(true);
            setEmailError(false);

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/client/code`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email }),
            });

            const code = await res.json();
            if (!code) return console.error('internal server error');
            setTruePasscode(code);

            if (res.status !== 200) {
                setEmailError(true);
                setSendingEmail(false);
                return;
            } else {
                setHasRequestCode(true);
                setSendingEmail(false);
            }
        } catch (error) {
            console.error(error);
            setSendingEmail(false);
        }
    }

    // LOGIN WITH PASSCODE LOGIC
    // NO-CLICK LOGIN
    const location = useLocation();
    const params = new URLSearchParams(location.search);
    const passcode = params.get('passcode') || '';

    function setCodeURL(passcode: string) {
        history.replace(location.pathname + '?passcode=' + passcode);
    }

    useEffect(() => {
        if (passcode.length === 6 && passcode === truePasscode) {
            console.log(passcode === truePasscode);

            dispatch(loginThunk({ passcode }, history));
        }
    }, [passcode, dispatch, history, truePasscode]);

    // LINK LOGIN
    useEffect(() => {
        if (passcode.length === 6) {
            dispatch(loginThunk({ passcode }, history));
        }
    }, [dispatch]);

    // CLICK LOGIN
    function sendCode(passcode: string) {
        setPasscodeError(false); // Only able to check error once for now, dunno how to fix yet!
        if (passcode !== truePasscode) {
            setPasscodeError(true);
            return;
        }
        dispatch(loginThunk({ passcode }, history));
    }

    return (
        <IonPage className="ClientPage">
            <IonContent class="client-content" hidden={hasRequestCode}>
                <div>
                    <SquareLogo />
                    <div className="client-bg rounded ion-padding ion-text-center">
                        <div className="client-title">
                            <p color="primary" className="client-login-title">
                                • 客戶登入 •
                            </p>
                        </div>

                        <form onSubmit={handleSubmit(() => requestCode(email))}>
                            <IonList>
                                <IonItem className="white-item ion-no-padding">
                                    <IonInput
                                        className="client-login-input"
                                        placeholder="   登入電郵"
                                        value={email}
                                        required={true}
                                        type="email"
                                        onIonChange={(e) =>
                                            setEmail((e.currentTarget as HTMLInputElement).value)
                                        }
                                        {...register('email')}
                                        disabled={sendingEmail}
                                    />
                                </IonItem>
                            </IonList>
                            <IonButton
                                type="submit"
                                className="ion-margin-top client-login-button-width"
                                disabled={sendingEmail}
                            >
                                驗證碼登入
                            </IonButton>
                        </form>
                        <IonAlert
                            isOpen={emailError}
                            header={'電郵地址未註冊'}
                            message={'請先註冊帳戶'}
                            buttons={['OK']}
                            backdropDismiss={false}
                        />
                        <h6>
                            <IonText color="light">或</IonText>
                        </h6>

                        <IonButton
                            routerLink="/client/signup"
                            className={`ion-margin-top  client-login-button-width`}
                            disabled={sendingEmail}
                        >
                            註冊帳戶
                        </IonButton>
                    </div>
                    <div className="cifu-client-login-div">
                        <Link to={'/cifu/login'}>
                            <div className="cifu-client-login">師傅登入</div>
                        </Link>
                    </div>
                </div>
                <IonLoading isOpen={sendingEmail} message={'處理中'} />
            </IonContent>

            <IonHeader hidden={!hasRequestCode}>
                <IonToolbar>
                    <IonTitle>
                        <div className="client-bar-logo">
                            <LongLogo />
                        </div>
                    </IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent class="client-content" hidden={!hasRequestCode}>
                <div>
                    <div className="client-header-logo">• 客戶登入 •</div>
                    <h1 className="client-token">
                        <IonText color="secondary">請查閱電郵內的驗證碼</IonText>
                    </h1>

                    <span>
                        <IonText color="primary">輸入6位數字驗證碼*</IonText>
                    </span>

                    <IonItem className="orangeItem">
                        <IonLabel position="floating"></IonLabel>
                        <IonInput
                            value={passcode}
                            onIonChange={(e) =>
                                setCodeURL((e.currentTarget as HTMLInputElement).value)
                            }
                        />
                    </IonItem>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <IonButton
                            onClick={() => sendCode(passcode)}
                            className="ion-margin-top orangeButton"
                            expand="block"
                            disabled={isLoading}
                        >
                            登入
                        </IonButton>
                    </div>

                    <IonAlert
                        isOpen={passcodeError}
                        header={'驗證碼不正確'}
                        message={'請輸入正確驗證碼'}
                        buttons={['OK']}
                        backdropDismiss={false}
                    />
                </div>
            </IonContent>
        </IonPage>
    );
}

export default ClientLoginPage;
