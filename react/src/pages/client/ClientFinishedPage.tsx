import { useEffect, useState } from 'react'
import { IonButtons, IonHeader, IonToolbar, IonButton, IonTitle, IonPage, IonContent, IonListHeader, IonBackButton } from '@ionic/react';
import { useHistory, useParams } from 'react-router';
import { RootState } from '../../redux';
import { useSelector, useDispatch } from 'react-redux';
import { AssignmentState, OrderState } from '../../redux/data/state';
import { clientChangeCompletionDateThunk, getAllOrderListThunk } from '../../redux/data/thunk';
import { LongLogo } from '../../components/Logo';

type OrderItem = {
    order: OrderState

    assignment: AssignmentState[]
}

function ClientFinishedPage() {
    const history = useHistory()
    const dispatch = useDispatch();
    const orderId = Object.values(useParams()).join();

    useEffect(() => {
        if (orderList.length !== 0) return
        dispatch(getAllOrderListThunk());

    }, [dispatch])



    const [photoList, setPhotoList] = useState<string[]>([])

    const orderList: OrderItem[] = useSelector(
        (state: RootState): OrderItem[] => {

            const orderList: OrderItem[] = []

            Object.values(state.data.order_list).forEach((order) => {
                const assignment = Object.values(state.data.assignment_list).sort((a, b) => {
                    return new Date(b.created_at!).getTime() - new Date(a.created_at!).getTime()
                }).filter((assignment) => {
                    return assignment.order_id == order.order_id
                })

                if (!assignment) return

                orderList.push({
                    order,
                    assignment
                })
            })

            return orderList
        }

    )

    let findedOrder = orderList.find(item => {
        return item.order.order_id === parseInt(orderId)
    })

    findedOrder?.assignment.sort(function (a, b) {
        return (
            new Date(b.repair_date).getTime() -
            new Date(a.repair_date).getTime()
        );
    });



    let assignmentId = findedOrder?.assignment[0].assignment_id



    async function submit() {

        dispatch(clientChangeCompletionDateThunk(history, assignmentId!, parseInt(orderId)))


    }
    return (


        <IonPage className="ClientPage">
            <IonHeader>
                <IonToolbar>

                    <IonButtons className="client-back-button" slot="start">
                        <IonBackButton defaultHref="/client/status" />
                    </IonButtons>


                    <IonTitle ><div className="client-bar-logo"><LongLogo /></div></IonTitle>

                </IonToolbar>
            </IonHeader>
            <div className="client-header-logo">• 確認完工 •</div>
            <IonContent className="client-content" ion-text-center ion-padding>
                
                <IonListHeader className="ion-no-padding">預約編號: #{findedOrder?.order.order_id}</IonListHeader>
                <IonListHeader className="ion-no-padding">
                    服務範疇: {findedOrder?.order.repair_category_name}
                </IonListHeader>

                {(() => {
                    if (findedOrder && findedOrder.assignment.length !== 0) {

                        let date = findedOrder.assignment[0].repair_date
                            .toString()
                            .substring(0, 10);



                        return (
                            <>
                                <IonListHeader className="ion-no-padding">
                                    預約日期: {date}
                                </IonListHeader>
                                <IonListHeader className="ion-no-padding">
                                    預約時間: {findedOrder.assignment[0].repair_time == "am" ? '上午 9:00至12:00' : '下午 2:00至6:00'}
                                </IonListHeader>
                            </>
                        );
                    } else {
                        return (
                            <>
                                <IonListHeader className="ion-no-padding">
                                    預約日期: 待定
                                </IonListHeader>
                                <IonListHeader className="ion-no-padding">
                                    預約時間: 待定
                                </IonListHeader>
                            </>
                        );
                    }
                })()}






            </IonContent>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <IonButton style={{ marginTop: '60px' }} className="formButton" expand="block" onClick={submit}>確認完工</IonButton>
            </div>
        </IonPage>
    )

}
export default ClientFinishedPage