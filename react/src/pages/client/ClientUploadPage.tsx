import {
    IonButtons,
    IonListHeader,
    IonHeader,
    IonToolbar,
    IonButton,
    IonTitle,
    IonPage,
    IonContent,
    IonBackButton,
    IonText,
    IonSpinner,
    IonModal,
    IonAlert,
} from '@ionic/react';
import { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { selectImage, filesToBase64Strings } from '@beenotung/tslib/file';
import {
    compressImageToBase64,
    toImage,
} from '@beenotung/tslib/image';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { getAllOrderListThunk, uploadPaymentImageThunk } from '../../redux/data/thunk';
import { AssignmentState, OrderState } from '../../redux/data/state';
import { LongLogo } from '../../components/Logo';
import { ModalState } from '../cifu/CifuConfirmedPage';

type OrderItem = {
    order: OrderState;

    assignment: AssignmentState[];
};

function ClientUploadPage() {
    const history = useHistory();
    const dispatch = useDispatch();
    const orderId = Object.values(useParams()).join();

    useEffect(() => {
        if (orderList.length !== 0) return;
        dispatch(getAllOrderListThunk());
    }, [dispatch]);

    const [loading, setLoading] = useState<boolean>(false);
    const [photoList, setPhotoList] = useState<string[]>([]);
    const [modalPhoto, setModalPhoto] = useState<ModalState>({ photo: '', isOpen: false });
    const [showAlert, setShowAlert] = useState<boolean>(false)

    const orderList: OrderItem[] = useSelector((state: RootState): OrderItem[] => {
        const orderList: OrderItem[] = [];

        Object.values(state.data.order_list).forEach((order) => {
            const assignment = Object.values(state.data.assignment_list)
                .sort((a, b) => {
                    return new Date(b.created_at!).getTime() - new Date(a.created_at!).getTime();
                })
                .filter((assignment) => {
                    return assignment.order_id == order.order_id;
                });

            if (!assignment) return;

            orderList.push({
                order,
                assignment,
            });
        });

        return orderList;
    });

    const findedOrder = orderList.find((item) => {
        return item.order.order_id === parseInt(orderId);
    });

    async function select() {
        setPhotoList([]);
        let files = await selectImage({ multiple: false });
        let photoList = await filesToBase64Strings(files);

        if (photoList.length > 0) {
            setLoading(true);
        }
        for (let i = 0; i < photoList.length; i++) {
            let image = await toImage(photoList[i]);
            photoList[i] = compressImageToBase64({ image, maximumLength: 300 * 1000 });
        }

        setPhotoList(photoList);
        setLoading(false);
    }

    function submit() {

        if(photoList.length === 0){
            setShowAlert(true)
            return
        }
        dispatch(uploadPaymentImageThunk(history, photoList, parseInt(orderId)));

    }

    return (
        <IonPage className="ClientPage">
            <IonHeader>
                <IonToolbar>
                    <IonButtons className="client-back-button" slot="start">
                        <IonBackButton defaultHref="/client/status" />
                    </IonButtons>

                    <IonTitle>
                        <div className="client-bar-logo">
                            <LongLogo />
                        </div>
                    </IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                cssClass='my-custom-class'
                header={''}
                subHeader={''}
                message={'請提交付款證明'}
                buttons={['確定']}
            />

            <IonModal animated isOpen={modalPhoto.isOpen}>
                <IonContent color="tertiary">
                    <div className="client-header-logo">• 圖片 •</div>
                    <div className="modal-div">
                        <img style={{ width: '80%' }} src={modalPhoto.photo}></img>
                    </div>
                </IonContent>
                <IonButton
                    className="modalBtn"
                    onClick={() => setModalPhoto({ photo: '', isOpen: false })}
                >
                    關閉
                </IonButton>
            </IonModal>
            <div className="client-header-logo">• 上傳付款證明 •</div>
            <IonContent className="client-content">
                
                <IonListHeader className="ion-no-padding">
                    預約編號: #{findedOrder?.order.order_id}
                </IonListHeader>
                <IonListHeader className="ion-no-padding">
                    服務範疇: {findedOrder?.order.repair_category_name}
                </IonListHeader>

                {(() => {
                    if (findedOrder?.assignment && findedOrder.assignment.length !== 0) {
                        findedOrder.assignment.sort(function (a, b) {
                            return (
                                new Date(b.repair_date).getTime() -
                                new Date(a.repair_date).getTime()
                            );
                        });
                        let date = findedOrder.assignment[0].repair_date
                            .toString()
                            .substring(0, 10);

                        return (
                            <>
                                <IonListHeader className="ion-no-padding">
                                    預約日期: {date}
                                </IonListHeader>
                                <IonListHeader className="ion-no-padding">
                                    預約時間:{' '}
                                    {findedOrder.assignment[0].repair_time == 'am'
                                        ? '上午 9:00至12:00'
                                        : '下午 2:00至6:00'}
                                </IonListHeader>
                            </>
                        );
                    } else {
                        return (
                            <>
                                <IonListHeader className="ion-no-padding">
                                    預約日期: 待定
                                </IonListHeader>
                                <IonListHeader className="ion-no-padding">
                                    預約時間: 待定
                                </IonListHeader>
                            </>
                        );
                    }
                })()}
                <div style={{ display: 'flex', marginTop: '20px' }}>
                    <IonButton color="secondary" onClick={select}>
                        <IonText color="light">選擇圖片</IonText>
                    </IonButton>

                    {photoList.length == 0 && loading == false && (
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div className="emptyPhotoDiv"></div>
                        </div>
                    )}
                    {loading && (
                        <div>
                            <IonSpinner name="dots" />
                            <IonSpinner name="dots" /> <IonSpinner name="bubbles" />
                        </div>
                    )}

                    {photoList.length > 0 &&
                        photoList.slice(0, 3).map((photo) => (
                            <button
                                key={'photo-' + photo}
                                className="openModalBtn"
                                onClick={() => setModalPhoto({ photo, isOpen: true })}
                            >
                                <div
                                    style={{
                                        backgroundImage: `url(${photo})`,
                                        width: '50px',
                                        height: '50px',
                                        backgroundSize: 'cover',
                                        backgroundPosition: 'center',
                                        paddingLeft: '20px',
                                    }}
                                ></div>
                            </button>
                        ))}
                </div>
            </IonContent>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <IonButton
                    style={{ marginTop: '60px' }}
                    className="formButton"
                    expand="block"
                    onClick={submit}
                >
                    提交
                </IonButton>
            </div>
        </IonPage>
    );
}

export default ClientUploadPage;
