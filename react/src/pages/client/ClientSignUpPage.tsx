import './Client.scss'

import { LongLogo } from '../../components/Logo';
import {
    IonAlert,
    IonBackButton,
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonPage,
    IonText,
    IonTitle,
    IonToolbar,
} from '@ionic/react';
import { useForm } from 'react-hook-form';
import { RequestCodeInput } from 'shared';
import { useEffect, useState } from 'react';
import { clientSignUpFields } from '../../data/formFields';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { loginThunk, RootState } from '../../redux';

function ClientSignUpPage() {
    const { register, handleSubmit, reset } = useForm();
    const [sendingEmail, setSendingEmail] = useState<boolean>(false);
    const [emailSent, setEmailSent] = useState<boolean>(false);
    const [emailTaken, setEmailTaken] = useState<boolean>(false);
    const [truePasscode, setTruePasscode] = useState<string>('');
    const [passcodeError, setPasscodeError] = useState<boolean>(false);
    const isLoading = useSelector((state: RootState) => state.auth.isLoading);

    const history = useHistory();
    const dispatch = useDispatch();

    const onSubmit = async (data: RequestCodeInput) => {
        setSendingEmail(true);
        try {
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/client/signup`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            });

            if (res.status !== 200) {
                setSendingEmail(false);
                setEmailTaken(true);
                return;
            }

            const code = await res.json();
            setTruePasscode(code);
            setEmailSent(true);
            setSendingEmail(false);
            reset();
        } catch (e) {
            console.error(e.message);
        }
    };

    // LOGIN WITH PASSCODE LOGIC
    const location = useLocation();
    const params = new URLSearchParams(location.search);
    const passcode = params.get('passcode') || '';

    function setCodeURL(passcode: string) {
        history.replace(location.pathname + '?passcode=' + passcode);
    }

    useEffect(() => {
        if (passcode.length === 6 && passcode === truePasscode) {
            console.log(passcode === truePasscode);
            dispatch(loginThunk({ passcode }, history));
        }
    }, [passcode, dispatch, history, truePasscode]);

    // CLICK LOGIN
    function sendCode(passcode: string) {
        setPasscodeError(false); // Only able to check error once for now, dunno how to fix yet!
        if (passcode !== truePasscode) {
            setPasscodeError(true);
            return;
        }
        dispatch(loginThunk({ passcode }, history));
    }

    return (
        <IonPage className="ClientPage">
            <IonHeader>
                <IonToolbar>
                    
                    <IonButtons className="client-back-button" slot="start">
                        <IonBackButton defaultHref="/client/login"  />
                    </IonButtons>
                    
                    
                    <IonTitle ><div className="client-bar-logo"><LongLogo /></div></IonTitle>
                </IonToolbar>
            </IonHeader>
            {emailSent == false?<div className="client-header-logo">•  註冊帳戶  •</div>:<div className="client-header-logo">•  客戶登入  •</div>}
            <IonContent className="client-content" hidden={emailSent} ion-padding>
                
                
                <form onSubmit={handleSubmit(onSubmit)}>
                <IonList>
                    {clientSignUpFields.map((field, index) => {
                        const { label, props } = field;
                        return (
                            
                            <IonItem className="orangeItem" key={index} lines="full">
                                <>
                                    <IonLabel className="signup-label" position="stacked">{label}</IonLabel>
                                    <IonInput
                                        {...register(props.name)}
                                        {...props}
                                        disabled={sendingEmail}
                                    />
                                </>
                            </IonItem>
                            
                        );
                    })}
                    </IonList>
                    <div style={{display:'flex',justifyContent:'center'}}>
                    <IonButton
                        type="submit"
                        className="ion-margin-top orangeButton"
                        expand="block"
                        disabled={sendingEmail}
                    >
                        註冊帳戶
                    </IonButton>
                    </div>
                    
                </form>
                <IonAlert
                    isOpen={emailTaken}
                    header={'電郵地址已註冊'}
                    message={'請使用另一電郵'}
                    buttons={['OK']}
                    backdropDismiss={false}
                />
                <IonLoading isOpen={sendingEmail} message={'處理中'} />
            </IonContent>
            
            <IonContent class="client-content"  hidden={!emailSent}>
                <div >
                   
                
                <h1 className="client-token"><IonText color="secondary">請查閱電郵內的驗證碼</IonText></h1>
                  
                <span><IonText color="primary" >輸入6位數字驗證碼*</IonText></span>

                    <IonItem className="orangeItem">
                        <IonLabel position="floating"></IonLabel>
                        <IonInput
                            value={passcode}
                            onIonChange={(e) =>
                                setCodeURL((e.currentTarget as HTMLInputElement).value)
                            }
                        />
                    </IonItem>
                    <div style={{display:'flex', justifyContent:'center'}}>
                    <IonButton
                        onClick={() => sendCode(passcode)}
                        className="ion-margin-top orangeButton"
                        expand="block"
                        disabled={isLoading}
                    >
                        登入
                    </IonButton>
                            </div>
                    <IonAlert
                        isOpen={passcodeError}
                        header={'驗證碼不正確'}
                        message={'請輸入正確驗證碼'}
                        buttons={['OK']}
                        backdropDismiss={false}
                    />
                </div>
            </IonContent>
        </IonPage>
    );
}

export default ClientSignUpPage;
