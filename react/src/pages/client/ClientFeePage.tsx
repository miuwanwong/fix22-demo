import { useEffect } from 'react';

import {
    IonButton,
    IonPage,
    IonHeader,
    IonContent,
    IonToolbar,
    IonTitle,
    IonItem,
    IonSelect,
    IonSelectOption,
    IonListHeader,
    IonInput,
    IonButtons,
    IonBackButton,
} from '@ionic/react';
import { useHistory } from 'react-router';
import CircleHeader from './CircleHeader';
import { useStorageState } from '../../hooks/useStorageState';
import { EmptyObject } from '../../constants';
import { useDispatch, useSelector } from 'react-redux';
import {
    dataURItoMimeType,
    dataURItoBlob,
} from '@beenotung/tslib/image';
import { LongLogo } from '../../components/Logo';
import './Client.scss';
import { RootState } from '../../redux/state';
import { getAllAccountsListThunk } from '../../redux/data/thunk';
import { ClientState } from '../../redux/data/state';

function ClientFeePage() {
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getAllAccountsListThunk('client'));
    }, [dispatch]);

    let client: ClientState | undefined = useSelector((state: RootState) => {
        return Object.values(state.data.client_list).find(
            (c) => c.user_id == state.auth.user?.user_id
        );
    });

    let address1: string = client?.company_address1 || client?.company_name || '';
    let address2: string = client?.company_address2 || '';
    let tel: string = client?.company_tel || '';
    let email: string = client?.email || '';
    let databaseDistrict: string = client?.district_id?.toString() || '14';
    let databasePersonInCharge: string = client?.person_in_charge || '';
    let databasePersonInChargePrefix: string = client?.person_in_charge_prefix || '';
    let databaseJobTitle: string = client?.person_in_charge_job_title || '';

    useEffect(() => {
        setRepairAddress1(address1);
        setRepairAddress2(address2);
        setContactTel(tel);
        setContactEmail(email);
        setDistrict(databaseDistrict);
        setPersonInCharge(databasePersonInCharge);
        setPersonInChargePrefix(databasePersonInChargePrefix);
        setJobTitle(databaseJobTitle);
    }, [
        address1,
        address2,
        tel,
        email,
        databaseDistrict,
        databasePersonInCharge,
        databasePersonInChargePrefix,
        databaseJobTitle,
    ]);

    // const user_id = useSelector((state: RootState) => state.auth.user?.user_id);

    const [price, setPrice] = useStorageState('price', 0);

    const [selectedRepairCategoryId, setRepairCategoryId] = useStorageState<number | null>(
        'selectedRepairCategoryId',
        null
    );

    const [problemDesc, setProblemDesc] = useStorageState('problemDesc', '');

    const [selectedRepairItemIdList, setSelectedRepairItemIdList] = useStorageState<{
        [service_id: number]: boolean;
    }>('selectedRepairItemIdList', EmptyObject);

    const [applianceBrand, setApplianceBrand] = useStorageState<string>('applianceBrand', '');
    const [applianceModal, setApplianceModal] = useStorageState<string>('applianceModal', '');
    const [applianceLocation, setApplianceLocation] = useStorageState<string>(
        'applianceLocation',
        ''
    );
    const [district, setDistrict] = useStorageState<string>('district', '');
    const [repairAddress1, setRepairAddress1] = useStorageState<string>('repairAddress1', '');
    const [repairAddress2, setRepairAddress2] = useStorageState<string>('repairAddress2', '');
    const [personInCharge, setPersonInCharge] = useStorageState<string>('personInCharge', '');
    const [personInChargePrefix, setPersonInChargePrefix] = useStorageState<string>(
        'personInChargePrefix',
        ''
    );
    const [contactTel, setContactTel] = useStorageState<string>('contactTel', '');
    const [jobTitle, setJobTitle] = useStorageState<string>('jobTitle', '經理');
    const [contactEmail, setContactEmail] = useStorageState<string>('contactEmail', '');

    const [photoList, setPhotoList] = useStorageState<string[]>('photoList', []);

    async function submit() {
        const datas = {
            repair_category_id: selectedRepairCategoryId,
            repair_item_id_array: Object.entries(selectedRepairItemIdList)
                .filter(([key, value]) => value)
                .map(([key, value]) => +key),
            problem_desc: problemDesc,
            appliance_brand: applianceBrand,
            appliance_model: applianceModal,
            appliance_install_location_desc: applianceLocation,
            district_id: district,
            repair_address1: repairAddress1,
            repair_address2: repairAddress2,
            person_in_charge: personInCharge,
            person_in_charge_prefix: personInChargePrefix,
            contact_tel: contactTel,
            person_in_charge_job_title: jobTitle,
            contact_email: contactEmail,
        };

        const body = new FormData();

        for (let photo of photoList) {
            let blob = await dataURItoBlob(photo);
            let file = new File([blob], 'image', {
                lastModified: Date.now(),
                type: dataURItoMimeType(photo),
            });
            body.append('orderImage', file);
        }

        body.append('data', JSON.stringify(datas));

        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/order`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
            },
            body,
        });

        if (res.status === 200) {
            const data = await res.json();
            console.log(data);
            setPrice(0);
            setRepairCategoryId(null);
            setProblemDesc('');
            setSelectedRepairItemIdList(EmptyObject);
            setApplianceBrand('');
            setApplianceModal('');
            setApplianceLocation('');
            setDistrict(databaseDistrict);
            setRepairAddress1(address1);
            setRepairAddress2(address2);
            setPersonInCharge(databasePersonInCharge);
            setPersonInChargePrefix(databasePersonInChargePrefix);
            setContactTel(tel);
            setJobTitle(databaseJobTitle);
            setContactEmail(email);
            setPhotoList([]);
            history.push('/client/success');
        } else {
            const data = await res.json();
            console.log(data);
        }
    }

    return (
        <IonPage className="ClientPage">
            <IonHeader>
                <IonToolbar>
                    <IonButtons className="client-back-button" slot="start">
                        <IonBackButton defaultHref="/client/service" />
                    </IonButtons>

                    <IonTitle>
                        <div className="client-bar-logo">
                            <LongLogo />
                        </div>
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <div className="client-header-logo">• 預約維修 •</div>
            <IonContent className="client-content" ion-text-center ion-padding>
                <CircleHeader first="#C4C4C4" second={null} third="#C4C4C4" />

                <IonListHeader className="ion-no-padding item-header">維修地址*</IonListHeader>

                <IonItem className="orangeItem">
                    <IonSelect
                        slot="start"
                        value={district}
                        defaultValue={district}
                        placeholder="地區"
                        okText="確認"
                        cancelText="取消"
                        onIonChange={(e) => setDistrict(e.detail.value)}
                    >
                        <IonSelectOption value="1">中西區</IonSelectOption>
                        <IonSelectOption value="2">東區</IonSelectOption>
                        <IonSelectOption value="3">南區</IonSelectOption>
                        <IonSelectOption value="4">灣仔區</IonSelectOption>
                        <IonSelectOption value="5">九龍城區</IonSelectOption>
                        <IonSelectOption value="6">觀塘區</IonSelectOption>
                        <IonSelectOption value="7">深水埗區</IonSelectOption>
                        <IonSelectOption value="8">黃大仙區</IonSelectOption>
                        <IonSelectOption value="9">油尖旺區</IonSelectOption>
                        <IonSelectOption value="18">離島區</IonSelectOption>
                        <IonSelectOption value="10">葵青區</IonSelectOption>
                        <IonSelectOption value="17">北區</IonSelectOption>
                        <IonSelectOption value="11">西頁區</IonSelectOption>
                        <IonSelectOption value="12">沙田區</IonSelectOption>
                        <IonSelectOption value="13">大埔區</IonSelectOption>
                        <IonSelectOption value="14">荃灣區</IonSelectOption>
                        <IonSelectOption value="16">屯門區</IonSelectOption>
                        <IonSelectOption value="15">元朗區</IonSelectOption>
                    </IonSelect>
                    <IonInput
                        value={repairAddress1}
                        placeholder="街道/門牌號碼/樓宇名稱"
                        defaultValue={repairAddress1}
                        onIonChange={(e) => setRepairAddress1(e.detail.value || '')}
                    ></IonInput>
                </IonItem>

                <IonItem className="orangeItem">
                    <IonInput
                        value={repairAddress2}
                        placeholder="座數/樓層/單位號碼"
                        defaultValue={repairAddress2}
                        onIonChange={(e) => setRepairAddress2(e.detail.value || '')}
                    ></IonInput>
                </IonItem>

                <IonListHeader className="ion-no-padding item-header">負責人*</IonListHeader>

                <IonItem className="orangeItem">
                    <IonInput
                        value={personInCharge}
                        slot="start"
                        placeholder="稱呼"
                        defaultValue={personInCharge}
                        onIonChange={(e) => setPersonInCharge(e.detail.value || '')}
                    ></IonInput>
                    <IonSelect
                        slot="end"
                        value={personInChargePrefix}
                        placeholder="稱謂"
                        defaultValue={personInChargePrefix}
                        okText="確認"
                        cancelText="取消"
                        onIonChange={(e) => setPersonInChargePrefix(e.detail.value)}
                    >
                        <IonSelectOption value="小姐">小姐</IonSelectOption>
                        <IonSelectOption value="女士">女士</IonSelectOption>
                        <IonSelectOption value="太太">太太</IonSelectOption>
                        <IonSelectOption value="先生">先生</IonSelectOption>
                    </IonSelect>
                </IonItem>

                <IonItem className="orangeItem">
                    <IonInput
                        value={jobTitle}
                        slot="start"
                        placeholder="請輸入職位"
                        defaultValue={jobTitle}
                        onIonChange={(e) => setJobTitle(e.detail.value || '')}
                    ></IonInput>
                </IonItem>

                <IonListHeader className="ion-no-padding item-header">聯絡電話*</IonListHeader>

                <IonItem className="orangeItem">
                    <IonInput
                        value={contactTel}
                        type="text"
                        maxlength={8}
                        minlength={8}
                        placeholder="輪入電話號碼"
                        defaultValue={contactTel}
                        onIonChange={(e) => setContactTel(e.detail.value || '')}
                    ></IonInput>
                </IonItem>

                <IonListHeader className="ion-no-padding item-header">電郵*</IonListHeader>
                <IonItem className="orangeItem">
                    <IonInput
                        value={contactEmail}
                        type="email"
                        placeholder="輪入電郵地址"
                        defaultValue={contactEmail}
                        onIonChange={(e) => setContactEmail(e.detail.value || '')}
                    ></IonInput>
                </IonItem>
            </IonContent>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <IonButton
                    className="formButton"
                    routerDirection="back"
                    routerLink="/client/booking"
                >
                    上一步
                </IonButton>
                <IonButton expand="block" className="formButton" onClick={submit}>
                    確認預約
                </IonButton>
            </div>
        </IonPage>
    );
}

export default ClientFeePage;
