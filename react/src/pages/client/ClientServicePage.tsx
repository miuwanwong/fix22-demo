import { IonHeader, IonToolbar, IonButton, IonTitle, IonPage, IonContent, IonText } from '@ionic/react';


import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { logoutThunk, RootState } from '../../redux';
import { useHistory } from 'react-router-dom';
import { SquareLogo } from '../../components/Logo';
import './Client.scss'
import { setInitialStateAction } from '../../redux/data/action';

function ClientServicePage() {
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const history = useHistory();
    const dispatch = useDispatch();



    useEffect(() => {
        if (!isAuthenticated) {
            history.push('/client/login');
        }
    }, [isAuthenticated, history]);

    const logout = () => {
        dispatch(logoutThunk());

        dispatch(setInitialStateAction())
        return history.push('/client/login');
    };

    return (
        <IonPage className="ClientPage">

            <IonContent class="client-content ion-text-center"  >
                <div style={{ marginBottom: '10px' }}><SquareLogo /></div>
                <div className="client-title ion-text-center" >
                    <p color="primary" className="client-login-title">請選擇服務</p>
                </div>

                <div >


                    <IonButton
                        routerDirection="forward"
                        routerLink="/client/booking"
                        color="primary"
                        expand="block"
                        className="client-service-button"
                    >
                        • 預約維修 •
                    </IonButton>
                    <IonButton
                        routerLink="/client/status"
                        expand="block"
                        className="client-service-button"
                        color="secondary">
                        <IonText color="primary">• 訂單查詢 •</IonText>
                    </IonButton>
                    <IonButton
                        routerLink="/client/settings"
                        className="client-service-button"
                        expand="block"
                        color="secondary">
                        <IonText color="primary">• 帳戶設定 •</IonText>
                    </IonButton>
                </div>
                <div>
                    <button className="client-logoutbtn" onClick={logout} >
                        <IonText color="secondary">登出</IonText>
                    </button>
                </div>

            </IonContent>
        </IonPage>
    );
}

export default ClientServicePage;
