import { IonHeader, IonToolbar, IonTitle, IonPage, IonContent, IonIcon } from '@ionic/react';
import StatusList from './StatusList';
import './Client.scss';
import { LongLogo } from '../../components/Logo';
import { arrowBack } from 'ionicons/icons';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';
import { getAllOrderListThunk } from '../../redux/data/thunk';

function ClientStatusPage() {
    const history = useHistory();
    const dispatch = useDispatch();

    const backToServicePage = () => {
        history.push('/client/service');
    };

    const refresh = () => {
        dispatch(getAllOrderListThunk());
    };

    return (
        <IonPage className="ClientPage">
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        <div className="client-bar-logo" onClick={refresh}>
                            <LongLogo />
                        </div>
                        <div className="client-back-button" onClick={backToServicePage}>
                            <IonIcon icon={arrowBack}></IonIcon>
                        </div>
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <div className="client-header-logo">• 訂單查詢 •</div>
            <IonContent className="client-content" ion-no-padding>
                <StatusList />
            </IonContent>
        </IonPage>
    );
}

export default ClientStatusPage;
