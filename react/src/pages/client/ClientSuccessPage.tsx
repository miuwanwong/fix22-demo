import {
    IonButton,
    IonPage,
    IonHeader,
    IonContent,
    IonToolbar,
    IonTitle,
    IonText,
    IonIcon,
} from '@ionic/react';
import { useHistory } from 'react-router-dom';
import { LongLogo } from '../../components/Logo';
import CircleHeader from './CircleHeader';
import styles from './Client.module.scss';
import { arrowBack } from 'ionicons/icons';

function ClientSuccessBooking() {
    const history = useHistory();

    const backToServicePage = () => {
        history.push('/client/service');
    };

    return (
        <IonPage className="ClientPage">
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        <div className="client-bar-logo">
                            <LongLogo />
                        </div>
                        <div className="client-back-button" onClick={backToServicePage}>
                            <IonIcon icon={arrowBack}></IonIcon>
                        </div>
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <div className="client-header-logo">• 預約維修 •</div>
            <IonContent className="client-content" ion-text-center ion-padding>
                <CircleHeader first="#C4C4C4" second="#C4C4C4" third={null} />
                <div className={styles.div}>
                    <div className={styles.successFormat}>
                        <h1 className="client-token">
                            <IonText color="primary">預約程序完成</IonText>
                        </h1>
                        <div>
                            <IonText className="client-token" color="primary">
                                客戶服務員將以電郵通知
                            </IonText>
                        </div>
                        <div>
                            <IonText className="client-token" color="primary">
                                閣下最快的維修時段
                            </IonText>
                        </div>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                        <IonButton className="orangeButton" routerLink="/client/service">
                            重新選擇服務
                        </IonButton>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    );
}

export default ClientSuccessBooking;
