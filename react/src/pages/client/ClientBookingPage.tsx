import { useEffect, useState } from 'react';
import {
    IonButtons,
    IonListHeader,
    IonInput,
    IonCheckbox,
    IonButton,
    IonPage,
    IonHeader,
    IonContent,
    IonToolbar,
    IonTitle,
    IonItem,
    IonLabel,
    IonSelect,
    IonSelectOption,
    IonBackButton,
    IonText,
    IonSpinner,
    IonAlert,
    IonModal,
} from '@ionic/react';

import CircleHeader from './CircleHeader';
import { useStorageState } from '../../hooks/useStorageState';
import { EmptyObject } from '../../constants';
import { selectImage, filesToBase64Strings } from '@beenotung/tslib/file';
import { useDispatch } from 'react-redux';
import { compressImageToBase64, toImage } from '@beenotung/tslib/image';
import { useHistory } from 'react-router';
import './Client.scss';
import { LongLogo } from '../../components/Logo';
import { ModalState } from '../cifu/CifuConfirmedPage';

type RepairType = {
    repair_category_id: number;
    repair_category_name: string;
    repair_category_desc: string;
    service_list: ServiceType[];
};
type ServiceType = {
    repair_item_id: number;
    repair_item_name: string;
    repair_fee: number;
    need_remarks: boolean;
};

function ClientBookingPage() {
    const [repairList, setRepairList] = useState<RepairType[] | null>(null);
    const [price, setPrice] = useStorageState<number>('price', 0);
    const [selectedRepairCategoryId, setRepairCategoryId] = useStorageState<number | null>(
        'selectedRepairCategoryId',
        null
    );
    const [applianceBrand, setApplianceBrand] = useStorageState<string>('applianceBrand', '');
    const [applianceModal, setApplianceModal] = useStorageState<string>('applianceModal', '');
    const [applianceLocation, setApplianceLocation] = useStorageState<string>(
        'applianceLocation',
        ''
    );
    const history = useHistory();
    const dispatch = useDispatch()
    const [showAlert, setShowAlert] = useState<boolean>(false)
    const [photoList, setPhotoList] = useStorageState<string[]>('photoList', []);

    useEffect(() => {
        const token = localStorage.getItem('token');
        const url = `${process.env.REACT_APP_API_SERVER}/appliance/typesproblems`;
        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
        })
            .then((res) => res.json())
            .then((data) => setRepairList(data.repairList));

    }, []);

    const [selectedRepairItemIdList, setSelectedRepairItemIdList] = useStorageState<{
        [service_id: number]: boolean;
    }>('selectedRepairItemIdList', EmptyObject);

    const [problemDesc, setProblemDesc] = useStorageState('problemDesc', '');

    const [loading, setLoading] = useState<boolean>(false);
    const [modalPhoto, setModalPhoto] = useState<ModalState>({ photo: '', isOpen: false });
    const serviceList = repairList
        ? repairList.find((repair) => repair.repair_category_id === selectedRepairCategoryId)
            ?.service_list
        : '';

    useEffect(() => {
        let sum = 0;

        if (serviceList != '') {
            serviceList?.forEach((service) => {
                if (selectedRepairItemIdList[service.repair_item_id]) {
                    sum += service.repair_fee;
                }
            });
            setPrice(sum);
        }
    }, [selectedRepairItemIdList, serviceList, setPrice]);

    useEffect(()=>{
        if(selectedRepairCategoryId != null || ""){
            setRepairCategoryId(selectedRepairCategoryId)
        }
    },[selectedRepairCategoryId])

    async function select() {
        setPhotoList([]);

        let files = await selectImage({ multiple: true });
        let photoList = await filesToBase64Strings(files);

        photoList = photoList.slice(0, 3)

        if (photoList.length > 0) {
            setLoading(true);
        }

    
        for (let i = 0; i < photoList.length; i++) {
            let image = await toImage(photoList[i]);
            photoList[i] = compressImageToBase64({ image, maximumLength: 300 * 1000 });
        }
        setPhotoList(photoList)
        setLoading(false);
    }



    function checkBeforeNext() {
        if (selectedRepairCategoryId == null || selectedRepairItemIdList == EmptyObject) {
            setShowAlert(true)
            return
        }



        history.push('/client/fee');
    }

    return (
        <IonPage className="ClientPage">
            <IonHeader>
                <IonToolbar>
                    <IonButtons className="client-back-button" slot="start">
                        <IonBackButton defaultHref="/client/service" />
                    </IonButtons>

                    <IonTitle>
                        <div className="client-bar-logo">
                            <LongLogo />
                        </div>
                    </IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                cssClass='my-custom-class'
                header={''}
                subHeader={''}
                message={'請選擇維修範圍或項目'}
                buttons={['確定']}
            />

            <IonModal animated isOpen={modalPhoto.isOpen}>
                <IonContent color="tertiary">
                    <div className="client-header-logo">• 圖片 •</div>
                    <div className="modal-div">
                        <img style={{ width: '80%' }} src={modalPhoto.photo}></img>
                    </div>
                </IonContent>
                <IonButton
                    className="modalBtn"
                    onClick={() => setModalPhoto({ photo: '', isOpen: false })}
                >
                    關閉
                </IonButton>
            </IonModal>
            <div className="client-header-logo">• 預約維修 •</div>
            <IonContent className="client-content" ion-text-center ion-padding>


                <CircleHeader first={null} second="#C4C4C4" third="#C4C4C4" />

                <IonItem>
                    <IonLabel
                        className="checkbox-label"

                    >
                        維修範疇*
                    </IonLabel>
                    <IonSelect
                        className="checkbox-label"
                        value={selectedRepairCategoryId}
                        placeholder={'請選擇'}
                        defaultValue={selectedRepairCategoryId || ''}
                        okText="確認"
                        cancelText="取消"
                        onIonChange={(e) => {
                            setRepairCategoryId(e.detail.value);
                            setSelectedRepairItemIdList(EmptyObject);
                            setProblemDesc('');
                        }}
                    >
                        {repairList !== null ? (
                            repairList.map((repair) => (
                                <IonSelectOption
                                    className="checkbox-label"
                                    key={'repair-category-' + repair.repair_category_id}
                                    value={repair.repair_category_id}
                                >
                                    {repair.repair_category_name}
                                </IonSelectOption>
                            ))
                        ) : (
                            <div></div>
                        )}
                    </IonSelect>
                </IonItem>

                {serviceList && (
                    <>

                        {serviceList !== null
                            ? serviceList.map((service) => (
                                <>
                                    <IonItem
                                        className="checkBox-fontsize"
                                        key={'repair-item-' + service.repair_item_id}
                                    >
                                        <IonLabel>
                                            {service.repair_item_name}{' '}
                                            {service.repair_fee == null ? (
                                                <></>
                                            ) : (
                                                `($${service.repair_fee.toLocaleString()})`
                                            )}
                                        </IonLabel>
                                        <IonCheckbox
                                            slot="start"
                                            checked={
                                                !!selectedRepairItemIdList[service.repair_item_id]
                                            }
                                            onIonChange={(e) => {
                                                setSelectedRepairItemIdList({
                                                    ...selectedRepairItemIdList,
                                                    [service.repair_item_id]: e.detail.checked,
                                                });
                                            }}
                                        />
                                    </IonItem>
                                </>
                            ))
                            : ''}

                        <IonItem className="orangeItem">
                            <IonLabel className="signup-label" position="stacked">
                                <strong>請簡單描述電器出現的問題</strong>
                            </IonLabel>
                            <IonInput
                                value={problemDesc}
                                onIonChange={(e) => setProblemDesc(e.detail.value || '')}
                            ></IonInput>
                        </IonItem>
                    </>
                )}

                <IonListHeader className="ion-no-padding item-header">
                    請上載照片以顯示電器的位置或問題
                </IonListHeader>

                <div style={{ display: 'flex', marginTop: '20px' }}>
                    <IonButton color="secondary" onClick={select}>
                        <IonText color="light">選擇圖片</IonText>
                    </IonButton>

                    {photoList.length == 0 && loading == false && (
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div className="emptyPhotoDiv"></div>
                            <div className="emptyPhotoDiv"></div>
                            <div className="emptyPhotoDiv"></div>
                        </div>
                    )}
                    {loading && (
                        <div>
                            <IonSpinner name="dots" />
                            <IonSpinner name="dots" /> <IonSpinner name="bubbles" />
                        </div>
                    )}

                    {photoList.length > 0 &&
                        photoList.slice(0, 3).map((photo) => (
                            <button
                                key={'photo-' + photo}
                                className="openModalBtn"
                                onClick={() => setModalPhoto({ photo, isOpen: true })}
                            >
                                <div
                                    style={{
                                        backgroundImage: `url(${photo})`,
                                        width: '50px',
                                        height: '50px',
                                        backgroundSize: 'cover',
                                        backgroundPosition: 'center',
                                        paddingLeft: '20px',
                                    }}
                                ></div>
                            </button>
                        ))}
                </div>
                <IonItem className="orangeItem">
                    <IonLabel className="signup-label" color={'black'} position="stacked">
                        <strong>電器牌子</strong>
                    </IonLabel>
                    <IonInput
                        required
                        value={applianceBrand}
                        onIonChange={(e) => setApplianceBrand(e.detail.value || '')}
                    ></IonInput>
                </IonItem>

                <IonItem className="orangeItem">
                    <IonLabel className="signup-label" color={'black'} position="stacked">
                        <strong>電器型號</strong>
                    </IonLabel>
                    <IonInput
                        value={applianceModal}
                        onIonChange={(e) => setApplianceModal(e.detail.value || '')}
                    ></IonInput>
                </IonItem>

                <IonItem className="orangeItem">
                    <IonLabel className="signup-label" color={'black'} position="stacked">
                        <strong>安裝位置</strong>
                    </IonLabel>
                    <IonInput
                        value={applianceLocation}
                        placeholder="例子:近大門位置"
                        onIonChange={(e) => setApplianceLocation(e.detail.value || '')}
                    ></IonInput>
                </IonItem>
            </IonContent>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <IonButton expand="block" className="formButton" onClick={checkBeforeNext}>
                    下一步
                </IonButton>
            </div>
        </IonPage>
    );
}

export default ClientBookingPage;

