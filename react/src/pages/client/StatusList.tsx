import React, { useEffect } from 'react';
import {
    IonBadge,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    useIonToast,
} from '@ionic/react';
import styles from '../../pages/client/Client.module.scss';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { socket } from '../../hooks/socketio';
import { getAllOrderListThunk } from '../../redux/data/thunk';
import { AssignmentState, OrderState } from '../../redux/data/state';
import { statusReference } from '../../data/status';
import './Client.scss';

type OrderItem = {
    order: OrderState;
    assignment: AssignmentState[];
};

function StatusList() {
    const dispatch = useDispatch();
    const [presentToast, dismissToast] = useIonToast();


    useEffect(() => {
        if (orderList.length !== 0) return;
        dispatch(getAllOrderListThunk());
    }, [dispatch]);

    useEffect(() => {
        const onNewAssignment = (data: { order_id: number }) => {
            dispatch(getAllOrderListThunk());
            presentToast({
                message: `訂單#${data.order_id}已安排維修日期`,
                color: 'danger',
                buttons: [
                    {
                        text: '確認',
                        role: 'cancel',
                    },
                ],
            });
        };

        const onEditAssignment = (data: { order_id: number }) => {
            dispatch(getAllOrderListThunk());
            presentToast({
                message: `訂單#${data.order_id}的維修安排有更改`,
                color: 'danger',
                buttons: [
                    {
                        text: '確認',
                        role: 'cancel',
                    },
                ],
            });
        };

        const onDeleteAssignment = (data: { order_id: number }) => {
            dispatch(getAllOrderListThunk());
            presentToast({
                message: `訂單#${data.order_id}的維修安排已被取消`,
                color: 'danger',
                buttons: [
                    {
                        text: '確認',
                        role: 'cancel',
                    },
                ],
            });
        };

        const onQuotationSent = (data: { order_id: number }) => {
            dispatch(getAllOrderListThunk());
            presentToast({
                message: `訂單#${data.order_id}已發出報價單，請付款`,
                color: 'danger',
                buttons: [
                    {
                        text: '確認',
                        role: 'cancel',
                    },
                ],
            });
        };

        const onOrderCancelled = (data: { order_id: number }) => {
            dispatch(getAllOrderListThunk());
            presentToast({
                message: `訂單#${data.order_id}已被取消`,
                color: 'danger',
                buttons: [
                    {
                        text: '確認',
                        role: 'cancel',
                    },
                ],
            });
        };

        socket.on('newAssignment', onNewAssignment);
        socket.on('editAssignment', onEditAssignment);
        socket.on('deleteAssignment', onDeleteAssignment);
        socket.on('quotationSent', onQuotationSent);
        socket.on('orderCancelled', onOrderCancelled);

        return () => {
            socket.off('newAssignment', onNewAssignment);
            socket.off('editAssignment', onEditAssignment);
            socket.off('quotationSent', onQuotationSent);
            socket.off('deleteAssignment', onDeleteAssignment);
            socket.off('orderCancelled', onOrderCancelled);
        };
    }, [presentToast, dispatch]);

    const orderList: OrderItem[] = useSelector((state: RootState): OrderItem[] => {
        const orderList: OrderItem[] = [];

        Object.values(state.data.order_list).forEach((order) => {
            const assignment = Object.values(state.data.assignment_list)
                .sort((a, b) => {
                    return new Date(b.created_at!).getTime() - new Date(a.created_at!).getTime();
                })
                .filter((assignment) => {
                    return assignment.order_id == order.order_id;
                });

            if (!assignment) return;

            orderList.push({
                order,
                assignment,
            });
        });

        return orderList;
    });

    orderList.sort((a, b) => {
        return b.order.order_id - a.order.order_id;
    });

    return (
        <>
            {!orderList && <div>未有訂單資料</div>}
            {orderList &&
                orderList.map((item) => (
                    <IonCard key={item.order.order_id}>
                        <IonCardHeader>
                            <IonCardTitle>
                                <h6>
                                    <strong>
                                        <span>#{item.order.order_id}</span> -{' '}
                                        <span>{item.order.repair_category_name}</span>
                                    </strong>
                                </h6>

                                {item.assignment.length > 0 &&
                                item.assignment![0].mark_complete_by_client != null ? (
                                    <IonBadge className={styles.finished}>已完工</IonBadge>
                                ) : item.order.order_status_id ===
                                  statusReference.orderStatusCancelled ? (
                                    <IonBadge className={styles.finished}>已取消</IonBadge>
                                ) : item.order.order_status_id ===
                                  statusReference.orderStatusAssigned ? (
                                    <Link to={`/client/finished/${item.order.order_id}`}>
                                        <IonBadge className={styles.confirmFinished}>
                                            請按此確認完工
                                        </IonBadge>
                                    </Link>
                                ) : item.order.payment_status_id ===
                                  statusReference.paymentStatusPendingQuotation ? (
                                    <IonBadge className={styles.processing}>正在處理</IonBadge>
                                ) : item.order.payment_status_id ===
                                  statusReference.paymentStatusPendingPayment ? (
                                    <Link to={`/client/upload/${item.order.order_id}`}>
                                        <IonBadge className={styles.upload}>
                                            請按此上載付款證明
                                        </IonBadge>
                                    </Link>
                                ) : item.order.payment_status_id ===
                                  statusReference.paymentStatusPaymentImageSubmitted ? (
                                    <IonBadge color="success">已上載付款證明</IonBadge>
                                ) : item.order.payment_status_id ===
                                  statusReference.paymentStatusPaymentConfirmed ? (
                                    <IonBadge color="success">已付款</IonBadge>
                                ) : (
                                    ''
                                )}
                            </IonCardTitle>
                        </IonCardHeader>

                        <IonCardContent>
                            {(() => {
                                if (item.assignment && item.assignment.length !== 0) {
                                    item.assignment.sort(function (a, b) {
                                        return (
                                            new Date(b.repair_date).getTime() -
                                            new Date(a.repair_date).getTime()
                                        );
                                    });
                                    let date = item.assignment[0].repair_date
                                        .toString()
                                        .substring(0, 10);

                                    return (
                                        <>
                                            <span>維修時間: {date}</span>{' '}
                                            <span>
                                                {item.assignment[0].repair_time === 'am'
                                                    ? '上午 9:00至12:00'
                                                    : '下午 2:00至6:00'}
                                            </span>
                                        </>
                                    );
                                } else {
                                    return <span>維修時間: 待定</span>;
                                }
                            })()}
                            <p>{item.order.repair_address1}</p>
                            <p>{item.order.repair_address2 ? item.order.repair_address2 : ''}</p>
                            <p>{}</p>
                        </IonCardContent>
                    </IonCard>
                ))}

        </>
    );
}

export default StatusList;
