import { Redirect, Route, useHistory } from 'react-router-dom';
import { IonApp, IonLoading, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
// import AppUrlListener from './components/AppUrlListener';
import { Deeplinks } from '@ionic-native/deeplinks';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

import './App.scss';
import CifuRepairPage from './pages/cifu/CifuRepairPage';
import CifuPaymentPage from './pages/cifu/CifuPaymentPage';
import CifuLoginPage from './pages/cifu/CifuLoginPage';
import CifuRepairInfoPage from './pages/cifu/CifuRepairInfoPage';
import CifuConfirmedPage from './pages/cifu/CifuConfirmedPage';

import ClientServicePage from './pages/client/ClientServicePage';
import ClientBookingPage from './pages/client/ClientBookingPage';
import ClientFeePage from './pages/client/ClientFeePage';
import ClientSuccessPage from './pages/client/ClientSuccessPage';
import ClientStatusPage from './pages/client/ClientStatusPage';
import ClientUploadPage from './pages/client/ClientUploadPage';
import ClientFinishedPage from './pages/client/ClientFinishedPage';
import ClientSignUpPage from './pages/client/ClientSignUpPage';

import AdminHomePage from './admin/AdminHomePage';
import ClientLoginPage from './pages/client/ClientLoginPage';
import NoMatch from './components/NoMatch';
import AdminLoginPage from './admin/AdminLoginPage';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { checkUserStatus, RootState } from './redux';
import CifuRoute from './components/cifu/CifuRoute';
import { ClientRoute } from './components';
import AdminRoute from './admin/AdminRoute';
import ClientSettingsPage from './pages/client/ClientSettingsPage';
import AdminSettingsPage from './admin/AdminSettingsPage';
import CifuSettingsPage from './pages/cifu/CifuSettingsPage';
import CifuSignupPage from './pages/cifu/CifuSignupPage';
import ClientAccountPage from './admin/ClientAccountPage';
import ClientAccountInfoPage from './admin/ClientAccountInfoPage';
import CifuAccountPage from './admin/CifuAccountPage';
import AdminOrderPage from './admin/AdminOrderPage';

function App() {
    const dispatch = useDispatch();

    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const user_type_id = useSelector((state: RootState) => state.auth.user?.user_type_id);
    const isLoading = useSelector((state: RootState) => state.auth.isLoading);

    if (
        !process.env.REACT_APP_USERS_CIFU ||
        !process.env.REACT_APP_USERS_CLIENT ||
        !process.env.REACT_APP_USERS_ADMIN
    ) {
        throw new Error('missing env');
    }

    const isCifu = user_type_id === parseInt(process.env.REACT_APP_USERS_CIFU);
    const isClient = user_type_id === parseInt(process.env.REACT_APP_USERS_CLIENT);
    const isAdmin = user_type_id === parseInt(process.env.REACT_APP_USERS_ADMIN);

    useEffect(() => {
        dispatch(checkUserStatus());
    }, [dispatch]);

    if (isAuthenticated === null || isLoading) {
        return (
            <>
                <IonLoading isOpen={true} message={'正在驗證您的身份...'} />
            </>
        );
    }
    if (isClient) {
        document.body.classList.add('client');
    } else {
        document.body.classList.remove('client');
    }

    if (isCifu) {
        document.body.classList.add('cifu');
    } else {
        document.body.classList.remove('cifu');
    }

    const DeepLinkMonitor = () => {
        const history = useHistory();
        useEffect(() => {
            let sub = Deeplinks.route({}).subscribe(
                (match: any) => {
                    console.log('match:', match);
                },
                (nomatch: any) => {
                    if (!nomatch || !nomatch.$link) return;

                    const { path, queryString } = nomatch.$link;
                    if (path !== '/client/login') return;
                    const params = new URLSearchParams(queryString);
                    const passcode = params.get('passcode');
                    if (!passcode) return;
                    history.push('/client/login?' + queryString);
                }
            );
            return () => {
                sub.unsubscribe();
            };
        }, [history]);
        return <></>;
    };

    return (
        <IonApp>
            <IonReactRouter>
                {/* <AppUrlListener></AppUrlListener> */}
                <DeepLinkMonitor />
                <IonRouterOutlet>
                    {/* This public route is to set App Launch Page:
                    if isCifu & already login before --> open cifu repair page
                    if isClient & already login before --> open client service page
                    if fresh role, not login before --> open client service page 
                     */}
                    <Route exact path="/">
                        {isCifu ? (
                            <Redirect to="/cifu/repair" />
                        ) : isClient ? (
                            <Redirect to="/client/service" />
                        ) : (
                            <ClientLoginPage />
                        )}
                    </Route>

                    <Route exact path="/cifu/login">
                        {isCifu ? <Redirect to="/cifu/repair" /> : <CifuLoginPage />}
                    </Route>
                    <Route exact path="/cifu/signup">
                        {isCifu ? <Redirect to="/cifu/repair" /> : <CifuSignupPage />}
                    </Route>
                    <CifuRoute exact path="/cifu/repair" component={CifuRepairPage} />
                    <CifuRoute exact path="/cifu/repairinfo" component={CifuRepairInfoPage} />
                    <CifuRoute exact path="/cifu/confirmed" component={CifuConfirmedPage} />
                    <CifuRoute exact path="/cifu/payment" component={CifuPaymentPage} />
                    <CifuRoute exact path="/cifu/settings" component={CifuSettingsPage} />
                    <CifuRoute
                        exact
                        path="/cifu/repairinfo/:assignmentId"
                        component={CifuRepairInfoPage}
                    />
                    <CifuRoute
                        exact
                        path="/cifu/confirmed/:assignmentId"
                        component={CifuConfirmedPage}
                    />

                    <Route exact path="/client/login">
                        {isClient ? <Redirect to="/client/service" /> : <ClientLoginPage />}
                    </Route>
                    <Route exact path="/client/signup">
                        {isClient ? <Redirect to="/client/service" /> : <ClientSignUpPage />}
                    </Route>
                    <ClientRoute exact path="/client/service" component={ClientServicePage} />
                    <ClientRoute exact path="/client/booking" component={ClientBookingPage} />
                    <ClientRoute exact path="/client/fee" component={ClientFeePage} />
                    <ClientRoute exact path="/client/success" component={ClientSuccessPage} />
                    <ClientRoute exact path="/client/status" component={ClientStatusPage} />
                    <ClientRoute
                        exact
                        path="/client/upload/:orderId"
                        component={ClientUploadPage}
                    />
                    <ClientRoute
                        exact
                        path="/client/finished/:assignmentId"
                        component={ClientFinishedPage}
                    />
                    <ClientRoute exact path="/client/settings" component={ClientSettingsPage} />

                    <Route exact path="/admin/login">
                        {isAdmin ? <Redirect to="/admin/home" /> : <AdminLoginPage />}
                    </Route>
                    <AdminRoute exact path="/admin/home" component={AdminHomePage} />
                    <AdminRoute exact path="/admin/settings" component={AdminSettingsPage} />
                    <AdminRoute exact path="/admin/cifu-accounts" component={CifuAccountPage} />
                    <AdminRoute exact path="/admin/client-accounts" component={ClientAccountPage} />
                    <AdminRoute exact path="/admin/orders" component={AdminOrderPage} />
                    <AdminRoute
                        exact
                        path="/admin/client-accounts/:client_id"
                        component={ClientAccountInfoPage}
                    />
                    <Route component={NoMatch} />
                </IonRouterOutlet>
            </IonReactRouter>
        </IonApp>
    );
}

export default App;
