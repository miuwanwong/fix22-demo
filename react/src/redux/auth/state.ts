import { JWTPayload } from 'shared';

export type AuthState = {
  isAuthenticated: boolean | null;
  user: JWTPayload | null;
  // token: string;
  errorMessage: string | null;
  isLoading: boolean;
};
