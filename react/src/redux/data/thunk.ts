import { RootThunkDispatch } from '../thunk';
//import { ThunkDispatch } from 'redux-thunk'
//import { RootState } from './state'
import {
  updateOrderStatusAction,
  updatePaymentStatusAction,
  cifuChangeCompletionDateAction,
  clientChangeCompletionDateAction,
  setState,
  deleteAssignmentAction,
  createAssignmentAction,
  editAssignmentAction,
} from './action';
import { dataURItoMimeType, dataURItoBlob } from '@beenotung/tslib/image';
import { statusReference } from '../../data/status';

import { History } from 'history';
import { TimezoneDate } from 'timezone-date.ts';

//export type RootDispatch = ThunkDispatch<RootState, null, RootAction>

//for admin and client
export function getAllOrderListThunk() {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const token = localStorage.getItem('token');

      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/orders`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      const json = await res.json();
      if (json.status === 500) {
        console.error('cannot fetch orders');
        return;
      }
      const partialState = json.orders;

      console.log(partialState);

      dispatch(setState(partialState));
    };
  } catch (err) {
    console.error(err);
  }
}

// for admin and cifu
export function getAllAssignmentListThunk() {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const token = localStorage.getItem('token');
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/assignments`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const json = await res.json();
      if (json.status === 500) {
        console.error('cannot get assignments');
        return;
      }
      const partialState = json.assignmentList;
      dispatch(setState(partialState));
    };
  } catch (err) {
    console.error(err);
  }
}

//for client
export function uploadPaymentImageThunk(history: History, photoList: string[], orderId: number) {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const body = new FormData();
      for (let photo of photoList) {
        let blob = await dataURItoBlob(photo);
        let file = new File([blob], 'image', {
          lastModified: Date.now(),
          type: dataURItoMimeType(photo),
        });
        body.append('paymentImage', file);
      }

      const token = localStorage.getItem('token');

      const res = await fetch(
        `${process.env.REACT_APP_API_SERVER}/order/${orderId}/payment-images`,
        {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${token}`,
          },
          body,
        }
      );

      if (res.status === 200) {
        const data = await res.json();

        dispatch(
          updatePaymentStatusAction(orderId, statusReference.paymentStatusPaymentImageSubmitted)
        );
        history.push('/client/status');
      } else {
        const data = await res.json();
        console.error(data);
      }
    };
  } catch (err) {
    console.error(err);
  }
}

//for client
export function clientChangeCompletionDateThunk(
  history: History,
  assignmentId: number,
  orderId: number
) {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const token = localStorage.getItem('token');

      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/order/order-status`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          orderId,
          orderStatusId: statusReference.orderStatusCompleted,
          assignmentId,
        }),
      });

      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        dispatch(updateOrderStatusAction(orderId, statusReference.orderStatusCompleted));
        dispatch(clientChangeCompletionDateAction(assignmentId));

        history.push('/client/status');
      } else {
        const data = await res.json();
        console.error(data);
      }
    };
  } catch (err) {
    console.error(err);
  }
}

//for cifu
export function cifuChangeCompletionDateThunk(
  history: History,
  orderId: number,
  assignmentId: number,
  photoList: string[]
) {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const body = new FormData();
      for (let photo of photoList) {
        let blob = await dataURItoBlob(photo);
        let file = new File([blob], 'image', {
          lastModified: Date.now(),
          type: dataURItoMimeType(photo),
        });
        body.append('completionImage', file);
      }

      body.append('orderId', `${orderId}`);
      const token = localStorage.getItem('token');
      const res = await fetch(
        `${process.env.REACT_APP_API_SERVER}/assignment/${assignmentId}/completion`,
        {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${token}`,
          },
          body,
        }
      );

      if (res.status === 200) {
        //const data = await res.json()

        dispatch(cifuChangeCompletionDateAction(assignmentId));
        history.push('/cifu/repair');
      } else {
        const data = await res.json();
        console.error(data);
      }
    };
  } catch (err) {
    console.error(err);
  }
}

export function getAllAccountsListThunk(client: string) {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const token = localStorage.getItem('token');

      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/accounts/${client}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      const json = await res.json();

      if (json.status === 500) {
        console.error('cannot get accounts list');
        return;
      }
      const partialState = json.accounts;
      // console.log('json', json.accounts);
      // console.log('partialState', partialState);
      dispatch(setState(partialState));
    };
  } catch (err) {
    console.error(err);
  }
}

export function updatePaymentStatusThunk(orderId: number, paymentStatusId: number) {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const token = localStorage.getItem('token');
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/order/payment-status`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ orderId, paymentStatusId }),
      });

      const data = await res.json();
      if (res.status === 200) {
        //const data = await res.json()
        dispatch(updatePaymentStatusAction(orderId, paymentStatusId));
      } else {
        const data = await res.json();
        console.error(data);
      }
    };
  } catch (err) {
    console.error(err);
  }
}

export function deleteAssignmentThunk(assignment_id: number, order_id: number) {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const token = localStorage.getItem('token');
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/assignment`, {
        method: 'DELETE',
        headers: {
          'Content-type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ assignment_id, order_id }),
      });

      if (res.status === 200) {
        const orderStatusUpdated = await res.json();

        if (orderStatusUpdated == true) {
          dispatch(updateOrderStatusAction(order_id, statusReference.orderStatusPendingAssignment));
        }
        dispatch(deleteAssignmentAction(assignment_id));
      } else {
        const data = await res.json();
        console.error(data);
      }
      //      dispatch(changeOrderStatusAction(order_id, 1, 'pending assignment'));
      //  }
      //   dispatch(
      ///    updateAssignmentListOnOrdersAction(
      //   {
      //     assignment_id,
      //     order_id,
      //     cifu_id: 0,
      //     repair_date: new Date(),
      //     repair_time: 'am',
      //     remarks: '',
      //   },
      //   'delete'
      // )
      //;
    };
  } catch (err) {
    console.error(err);
  }
}

export function updateOrderStatusThunk(orderId: number, orderStatusId: number) {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const token = localStorage.getItem('token');
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/order/order-status`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ orderId, orderStatusId }),
      });

      const data = await res.json();
      if (res.status === 200) {
        //const data = await res.json()
        //console.log(data);

        dispatch(updateOrderStatusAction(orderId, orderStatusId));
      } else {
        const data = await res.json();
        console.error(data);
      }
    };
  } catch (err) {
    console.error(err);
  }
}

export function createAssignmentThunk(
  order_id: number,
  cifu_id: number,
  repair_date: string,
  repair_time: string,
  remarks: string,
  total_amount_to_cifu: number
) {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const token = localStorage.getItem('token');
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/assignment`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          order_id,
          cifu_id,
          repair_date,
          repair_time,
          remarks,
          total_amount_to_cifu,
        }),
      });

      if (res.status === 200) {
        const data = await res.json();

        dispatch(createAssignmentAction(data.assignment));
        dispatch(updateOrderStatusAction(order_id, statusReference.orderStatusAssigned));
      } else {
        const data = await res.json();
        console.error(data);
      }
    };
  } catch (err) {
    console.error(err);
  }
}

export function editAssignmentThunk(
  assignment_id: number,
  order_id: number,
  cifu_id: number,
  repair_date: string,
  repair_time: string,
  remarks: string,
  total_amount_to_cifu: number
) {
  try {
    return async (dispatch: RootThunkDispatch) => {
      const token = localStorage.getItem('token');
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/assignment`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          assignment_id,
          order_id,
          cifu_id,
          repair_date,
          repair_time,
          remarks,
          total_amount_to_cifu,
        }),
      });

      if (res.status === 200) {
        const data = await res.json();
        dispatch(editAssignmentAction(data.assignment));
      } else {
        const data = await res.json();
        console.error(data);
      }
    };
  } catch (err) {
    console.error(err);
  }
}
