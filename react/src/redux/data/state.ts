export type OrderState = {
  order_id: number;
  user_id: number;
  repair_category_id: number;
  repair_category_name: string;
  problem_desc: string;
  appliance_brand: string;
  appliance_model: string;
  appliance_install_location_desc: string;
  repair_address1: string;
  repair_address2: string;
  district_id: number | string;
  district_name?: string;
  person_in_charge: string;
  person_in_charge_prefix: string;
  person_in_charge_job_title: string;
  contact_tel: string;
  contact_email: string;
  order_status_id: number;
  order_status?: string;
  payment_status_id: number;
  payment_status?: string;
  company_name: string;
  created_at?: Date;
  updated_at?: Date;
};

export type AssignmentState = {
  assignment_id?: number;
  order_id: number;
  cifu_id: number;
  repair_date: Date | string;
  repair_time: string;
  remarks: string;
  mark_complete_by_cifu?: Date;
  mark_complete_by_client?: Date;
  total_amount_to_cifu: number;
  cifu_payment_status?: boolean;
  created_at?: Date;
};

export type CompletionImageState = {
  assignment_id: number;
  image_file_name: string;
  uploaded_by: number;
};

export type OrderImageState = {
  order_id: number;
  image_file_name: string;
};

export type PaymentImageState = {
  order_id: number;
  image_file_name: string;
};

export type ClientState = {
  user_id: number;
  company_name: string;
  company_tel: string;
  company_address1: string;
  company_address2: string | null;
  district_id: number;
  district_name: string;
  person_in_charge: string;
  person_in_charge_prefix: string;
  person_in_charge_job_title: string;
  email: string;
  email_verified: boolean;
};

export type SelectedRepairItemList = {
  order_id: number;
  repair_item_id: number;
  repair_category_id: number;
  repair_item_name: string;
  repair_fee: number;
};

export type DataState = {
  order_list: Record<number, OrderState>;
  order_images_list: Record<number, OrderImageState>;

  payment_images_list: Record<number, PaymentImageState>;

  assignment_list: Record<number, AssignmentState>;

  completion_images_list: Record<number, CompletionImageState>;

  selected_repair_items_list: Record<number, SelectedRepairItemList>;

  client_list: Record<number, ClientState>;

  cifu_list: Record<
    number,
    {
      user_id: number;
      name: string;
      tel: string;
      email: string;
    }
  >;
};
