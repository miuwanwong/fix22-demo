import { DataState } from './state';
import { DataAction } from './action';

const initialState: DataState = {
  order_list: {},
  order_images_list: {},
  payment_images_list: {},
  assignment_list: {},
  completion_images_list: {},
  selected_repair_items_list: {},
  client_list: {},
  cifu_list: {},
};

export const dataReducer = (state: DataState = initialState, action: DataAction): DataState => {
  switch (action.type) {
    case '@@APP/setState': {
      const { partialState } = action;

      if (partialState.order_list) {
        state = {
          ...state,
          order_list: {
            ...state.order_list,
            ...partialState.order_list,
          },
        };
      }

      if (partialState.order_images_list) {
        state = {
          ...state,
          order_images_list: {
            ...state.order_images_list,
            ...partialState.order_images_list,
          },
        };
      }

      if (partialState.payment_images_list) {
        state = {
          ...state,
          payment_images_list: {
            ...state.payment_images_list,
            ...partialState.payment_images_list,
          },
        };
      }

      if (partialState.assignment_list) {
        state = {
          ...state,
          assignment_list: {
            ...state.assignment_list,
            ...partialState.assignment_list,
          },
        };
      }

      if (partialState.completion_images_list) {
        state = {
          ...state,
          completion_images_list: {
            ...state.completion_images_list,
            ...partialState.completion_images_list,
          },
        };
      }

      if (partialState.selected_repair_items_list) {
        state = {
          ...state,
          selected_repair_items_list: {
            ...state.selected_repair_items_list,
            ...partialState.selected_repair_items_list,
          },
        };
      }

      if (partialState.client_list) {
        state = {
          ...state,
          client_list: {
            ...state.client_list,
            ...partialState.client_list,
          },
        };
      }

      if (partialState.cifu_list) {
        state = {
          ...state,
          cifu_list: {
            ...state.cifu_list,
            ...partialState.cifu_list,
          },
        };
      }

      return state;
    }

    case '@@APP/cifuChangeCompletionDate': {
      let assignment = state.assignment_list[action.assignmentId];

      if (!assignment) return state;

      assignment = {
        ...assignment,
        mark_complete_by_cifu: new Date(),
      };
      let assignment_list = { ...state.assignment_list };
      assignment_list[action.assignmentId] = assignment;

      return { ...state, assignment_list };
    }

    case '@@APP/clientChangeCompletionDate': {
      let assignment = state.assignment_list[action.assignmentId];

      if (!assignment) return state;

      assignment = {
        ...assignment,
        mark_complete_by_client: new Date(),
      };
      let assignment_list = { ...state.assignment_list };
      assignment_list[action.assignmentId] = assignment;

      return { ...state, assignment_list };
    }

    case '@@APP/updatePaymentStatus': {
      return {
        ...state,
        order_list: {
          ...state.order_list,
          [action.orderId]: {
            ...state.order_list[action.orderId],
            payment_status_id: action.paymentStatusId,
          },
        },
      };
    }

    case '@@APP/updateOrderStatus': {
      let order = state.order_list[action.orderId];

      if (!order) return state;

      order = {
        ...order,
        order_status_id: action.orderStatusId,
      };

      let order_list = { ...state.order_list };
      order_list[action.orderId] = order;

      return { ...state, order_list };
    }

    case '@@APP/deleteAssignment': {
      let assignment_list = Object.values(state.assignment_list);

      assignment_list = assignment_list.filter(
        (item) => item.assignment_id !== action.assignmentId
      );

      return { ...state, assignment_list };
    }

    case '@@APP/createAssignment': {
      let assignment_list = Object.values(state.assignment_list);

      assignment_list.push(action.assignment);

      assignment_list = { ...assignment_list };

      return { ...state, assignment_list };
    }

    case '@@APP/editAssignment': {
      let assignment_list = { ...state.assignment_list };

      assignment_list[action.assignment.assignment_id!] = action.assignment;

      return { ...state, assignment_list };
    }

    default:
      return state;
  }
};
