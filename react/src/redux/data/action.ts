import { AssignmentState, DataState } from './state';

export function setState(partialState: Partial<DataState>) {
  return {
    type: '@@APP/setState' as const,
    partialState,
  };
}

export function cifuChangeCompletionDateAction(assignmentId: number) {
  return {
    type: '@@APP/cifuChangeCompletionDate' as const,
    assignmentId,
  };
}

export function updatePaymentStatusAction(orderId:number, paymentStatusId:number){

  return{
    type: '@@APP/updatePaymentStatus' as const,
    orderId,
    paymentStatusId,
  };
}

export function updateOrderStatusAction(orderId:number, orderStatusId:number){
  return{
    type: '@@APP/updateOrderStatus' as const,
    orderId,
    orderStatusId,
  };
}

export function clientChangeCompletionDateAction(assignmentId: number) {
  return {
    type: '@@APP/clientChangeCompletionDate' as const,
    assignmentId,
  };
}

export function deleteAssignmentAction(assignmentId: number) {
  return {
    type: '@@APP/deleteAssignment' as const,
    assignmentId,
  };
}

export function createAssignmentAction(assignment: AssignmentState) {
  return {
    type: '@@APP/createAssignment' as const,
    assignment,
  };
}

export function editAssignmentAction(assignment: AssignmentState) {
  return {
    type: '@@APP/editAssignment' as const,
    assignment,
  };
}

export function setInitialStateAction(){
 return{
   type: '@APP/setInitialState' as const
 }
}

export type DataAction =
  | ReturnType<typeof setState>
  | ReturnType<typeof cifuChangeCompletionDateAction>
  | ReturnType<typeof updatePaymentStatusAction>
  | ReturnType<typeof updateOrderStatusAction>
  | ReturnType<typeof clientChangeCompletionDateAction>
  | ReturnType<typeof createAssignmentAction>
  | ReturnType<typeof deleteAssignmentAction>
  | ReturnType<typeof editAssignmentAction>
  | ReturnType<typeof setInitialStateAction>;
