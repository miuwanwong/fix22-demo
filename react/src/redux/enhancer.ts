import { applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import { history } from './history';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any;
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// const rootEnhancer =
//   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

export const rootEnhancer = composeEnhancer(
  // applyMiddleware(logger),
  applyMiddleware(routerMiddleware(history)),
  applyMiddleware(thunk)
);
