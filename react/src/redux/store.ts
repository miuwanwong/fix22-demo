import { createStore } from 'redux';
import { rootReducer } from './reducer';
import { rootEnhancer } from './enhancer';
// import { composeWithDevTools } from 'redux-devtools-extension';

// let rootEnhancer = composeWithDevTools(
//   applyMiddleware(thunk),
//   applyMiddleware(routerMiddleware(history))
// );

export let store = createStore(rootReducer, rootEnhancer);
