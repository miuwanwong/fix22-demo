import { RouterState } from 'connected-react-router';
import { AuthState } from './auth/state';
import { DataState } from './data/state';
//import { FileUploadState } from './fileupload/state';
//import { AssignmentState, OrderState } from './orders/state';

export type RootState = {
  router: RouterState;
  auth: AuthState;
  data: DataState
 //order: OrderState[];
 //assignment: AssignmentState[];
 //fileUpload: FileUploadState
};

/*
export type RootState = {

  auth: {
    token: string
    user_id: number
    role: 'admin' | 'cifu' | 'client'
  } | null
  order_list: Record<
    number,
    {
      order_id: number
      user_id: number
      repair_category_id: number
      problem_desc: string
      appliance_brand: string
      appliance_model: string
      appliance_install_location_desc: string
      repair_address1: string
      repair_address2: string
      district_id: string
      person_in_charge: string
      person_in_charge_prefix: string
      person_in_charge_job_title: string
      contact_tel: string
      contact_email: string
      order_status_id: number
      payment_status_id: number
    }
  >
  order_images_list: Record<
    number,
    {
      order_id: number
      image_file_name: string
    }
  >

  payment_images_list: Record<
    number,
    {
      order_id: number,
      image_file_name: string
    }
  >

  assignment_list: Record<
    number,
    {
      assignment_id: number
      order_id: number
      cifu_id: number
      repair_date: string | Date
      repair_time: string
      remarks: string
      mark_complete_by_cifu: Date
      mark_complete_by_client: Date
      total_amount_to_cifu: number
      cifu_payment_status: boolean
    }
  >
  completion_images_list: Record<
    number,
    {
      assignment_id: number
      image_file_name: string
      uploaded_by: number
    }
  >
  /*
  user_list: Record<
    number,
    {
      user_id: number
      username: string
      role: 'admin' | 'cifu' | 'client'
    }
  >
}
*/