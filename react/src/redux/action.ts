import { CallHistoryMethodAction } from 'connected-react-router';
//import { AssignmentAction } from './assignments/action';
import { AuthAction } from './auth/action';
import { DataAction } from './data/action';

//import { FileUploadAction } from './fileupload/action';
//import { OrderAction } from './orders/action';

export type RootAction =
  | CallHistoryMethodAction
  | AuthAction
  | DataAction
 // | FileUploadAction
 // | OrderAction
 // | AssignmentAction;
