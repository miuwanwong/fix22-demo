import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux';
import { history } from './history';
import { authReducer } from './auth/reducer';
import {dataReducer} from './data/reducer'


export let rootReducer = combineReducers({
  router: connectRouter(history),
  auth: authReducer,
  data: dataReducer
});


